<?php
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/views/pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'lineas', 'action' => 'relacionsin'));
        Router::connect('/login', array('controller' => 'usuarios', 'action' => 'selecciona'));
        Router::connect('/logout', array('controller' => 'usuarios', 'action' => 'logout'));
        Router::connect('/control', array('controller' => 'lineas', 'action' => 'control'));
        Router::connect('/relazioa', array('controller' => 'lineas', 'action' => 'relazioa'));
        Router::connect('/addhoras', array('controller' => 'lineas', 'action' => 'addhoras'));
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
