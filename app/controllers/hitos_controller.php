<?php
class HitosController extends AppController {

	var $name = 'Hitos';
        var $components = array('RequestHandler');

        function beforeFilter(){
		$this->checkSession();
	}

	function finalizar ($id=null) {
		if ($id!=null) {
			
			$this->Hito->id = $id;
			$this->Hito->saveField('finalizado',1);
			
			$this->redirect($this->referer());
			
		}
	}
        
	function index() {
		$this->Hito->recursive = 0;
		$this->set('hitos', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid hito', true));
			$this->redirect(array('action' => 'index'));
		}
                $this->Hito->recursive = 2;
		$this->set('hito', $this->Hito->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Hito->create();
                        var_dump($this->data);
			if ($this->Hito->save($this->data)) {
				$this->Session->setFlash(__('The hito has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The hito could not be saved. Please, try again.', true));
			}
		}
		$usuarios = $this->Hito->Usuario->find('list');
		$proyectos = $this->Hito->Proyecto->find('list');
		$this->set(compact('usuarios', 'proyectos'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid hito', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Hito->save($this->data)) {
				$this->Session->setFlash(__('The hito has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The hito could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Hito->read(null, $id);
		}
		$usuarios = $this->Hito->Usuario->find('list');
		$proyectos = $this->Hito->Proyecto->find('list');
		$this->set(compact('usuarios', 'proyectos'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for hito', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Hito->delete($id)) {
			$this->Session->setFlash(__('Hito deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Hito was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}

    function muestra() {
            
        }
 
    function addajax() {

            if (!empty($this->data)) {
                if ($this->Hito->save($this->data)) {
                    //$this->Session->setFlash(__('Hito guardado', true));
                    $this->redirect(array('controller'=>'proyectos','action'=>'view',$this->data['Hito']['proyecto_id']));
                }
                }
                //Lo que se hace en esta parte es recargar la vista index, para que se muestre el post
                //que acabamos de agregar, junto con los que ya teníamos. (Sin este código la magia no
                //existiría).
//                $this->set('hitos', $this->Hito->find('all'));
//                $this->render('muestra');

                //Esta función elimina un post y recarga la página, exactamente como en el método ‘add’.

            

        }
}
?>