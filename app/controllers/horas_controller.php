<?php
class HorasController extends AppController {

	var $name = 'Horas';

        function beforeFilter(){
		$this->checkSession();
	}

	function index() {
		$this->Hora->recursive = 0;
		$this->set('Horas', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Hora', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->Hora->recursive = 2;
		$this->set('Hora', $this->Hora->read(null, $id));
	}
	
	function add2() {
		if (!empty($this->data)) {
			if ($this->Hora->saveAll($this->data['Hora'])) {
				$this->Session->setFlash(__('The Hora has been saved', true));
                $this->redirect(array('controller'=>'Horas','action'=>'add2'));
			} else {
				$this->Session->setFlash(__('The Hora could not be saved. Please, try again.', true));
			}
		}
		$usuarios = $this->Hora->Usuario->find('list');
		$familias = $this->Hora->Familia->find('list');
		$horas 	  = $this->Hora->Find('all');
		$this->set(compact('familias', 'usuarios','horas'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Hora', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Hora->save($this->data)) {
                $this->redirect(array('controller'=>'proyectos','action'=>'view',$this->data['Hora']['proyecto_id']));
			} else {
				$this->Session->setFlash(__('The Hora could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Hora->read(null, $id);
		}
		$proyectos = $this->Hora->Proyecto->find('list');
		$hitos = $this->Hora->Hito->find('list');
		$usuarios = $this->Hora->Usuario->find('list');
		$this->set(compact('proyectos', 'hitos', 'usuarios'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Hora', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Hora->delete($id)) {
			$this->Session->setFlash(__('Hora deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Hora was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>