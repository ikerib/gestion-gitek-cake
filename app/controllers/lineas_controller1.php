<?php
set_time_limit(400);

ob_start();     // buffer output
class LineasController extends AppController {

	var $name = 'Lineas';
        var $components = array('RequestHandler');
        var $helpers = array('Cache','Excel');

    function beforeFilter(){
			$this->checkSession();
		}

	function aexceljosebe ($datos = null) {
		$ep = $this->Session->Read("ExcelJosebe");
		$this->layout = 'ajax';
		$i=-1;
		$kk=array();
		//var_dump($ep);
		foreach ($ep as $p) {
			$i=$i+1;
			
			$kk[$i]['Coordinador'] = $p['Usuario']['name'];
			$kk[$i]['Familia'] = $p['Proyecto']['familia_id'];
			$kk[$i]['Proyecto'] = $p['Proyecto']['name'];
			
			$kk[$i]['Mes'] = $p[0]['Mes'];
			$kk[$i]['Horas'] = $p[0]['Horas'];
			$kk[$i]['Urtea'] = $p[0]['Urtea'];
		}

		$this->Session->delete('ExcelJosebe');
		$this->set('data', $kk);
	}

	function aexcel($datos = null) {
			$ep = $this->Session->Read("ExcelProyecto");
			$this->layout = 'ajax';
			$i=-1;
			$kk=array();
            $urtea = "2012";
            if (!empty ($this->data['Linea']['year'])) {
                $urtea = $this->data['Linea']['year'];
            }
			foreach ($ep as $p) {
                $i=$i+1;
                $kk[$i]['year'] = $p['year'];
                $kk[$i]['coordinador_id'] = $p['coordinador_id'];
                $kk[$i]['coordinador_nombre'] = $p['coordinador_nombre'];
                $kk[$i]['proyecto_id'] = $p['proyecto_id'];
                $kk[$i]['proyecto_nombre'] = $p['proyecto_nombre'];
                $kk[$i]['categoria_id'] = $p['categoria_id'];
                $kk[$i]['categoria_nombre'] = $p['categoria_nombre'];
                $kk[$i]['familia_id'] = $p['familia_id'];
                $kk[$i]['finalizado'] = $p['finalizado'];
                $kk[$i]['total_prev'] = $p['total_prev']-$p['orduk'];
                $kk[$i]['total_horas'] = $p['1']+$p['2']+$p['3']+$p['4']+$p['5']+$p['6']+$p['7']+$p['8']+$p['9']+$p['10']+$p['11']+$p['12'];
                $kk[$i]['desv'] = $kk[$i]['total_horas']*100/$p['total_prev'];
                $kk[$i]['1'] = $p['1'];
                $kk[$i]['2'] = $p['2'];
                $kk[$i]['3'] = $p['3'];
                $kk[$i]['4'] = $p['4'];
                $kk[$i]['5'] = $p['5'];
                $kk[$i]['6'] = $p['6'];
                $kk[$i]['7'] = $p['7'];
                $kk[$i]['8'] = $p['8'];
                $kk[$i]['9'] = $p['9'];
                $kk[$i]['10'] = $p['10'];
                $kk[$i]['11'] = $p['11'];
                $kk[$i]['12'] = $p['12'];   				
			}
			$this->Session->delete('ExcelProyecto');
			$this->set('data', $kk);
    }
 
	  function aexcelhitos($datos = null) {
				$ep = $this->Session->Read("ExcelHito");
				$this->layout = 'ajax';
				$i=-1;
				$kk=array();
				foreach ($ep as $p) {
					$i=$i+1;

					$kk[$i]['coordinador_id'] = $p['coordinador_id'];
					$kk[$i]['coordinador_nombre'] = $p['coordinador_nombre'];
					$kk[$i]['proyecto_id'] = $p['proyecto_id'];
					$kk[$i]['proyecto_nombre'] = $p['proyecto_nombre'];
					$kk[$i]['hito_id'] = $p['hito_id'];
					$kk[$i]['hito_nombre'] = $p['hito_nombre'];					
					$kk[$i]['categoria_id'] = $p['categoria_id'];
					$kk[$i]['categoria_nombre'] = $p['categoria_nombre'];
				//	$kk[$i]['familia_id'] = $p['familia_id'];
					$kk[$i]['finalizado'] = $p['finalizado'];
					$kk[$i]['total_prev'] = $p['total_prev'];
					$kk[$i]['total_horas'] = $p['1']+$p['2']+$p['3']+$p['4']+$p['5']+$p['6']+$p['7']+$p['8']+$p['9']+$p['10']+$p['11']+$p['12'];
					$kk[$i]['desv'] = $kk[$i]['total_horas']*100/$p['total_prev'];
					$kk[$i]['1'] = $p['1'];
					$kk[$i]['2'] = $p['2'];
					$kk[$i]['3'] = $p['3'];
					$kk[$i]['4'] = $p['4'];
					$kk[$i]['5'] = $p['5'];
					$kk[$i]['6'] = $p['6'];
					$kk[$i]['7'] = $p['7'];
					$kk[$i]['8'] = $p['8'];
					$kk[$i]['9'] = $p['9'];
					$kk[$i]['10'] = $p['10'];
					$kk[$i]['11'] = $p['11'];
					$kk[$i]['12'] = $p['12'];

				}
				$this->Session->delete('ExcelHito');
				$this->set('data', $kk);


    }
	
		function aexceltareas($datos = null) {
					$ep = $this->Session->Read("ExcelTarea");
					$this->layout = 'ajax';
					$i=-1;
					$kk=array();
					foreach ($ep as $p) {
						$i=$i+1;

						$kk[$i]['coordinador_id'] = $p['coordinador_id'];
						$kk[$i]['coordinador_nombre'] = $p['coordinador_nombre'];
						$kk[$i]['proyecto_id'] = $p['proyecto_id'];
						$kk[$i]['proyecto_nombre'] = $p['proyecto_nombre'];
						$kk[$i]['hito_id'] = $p['hito_id'];
						$kk[$i]['hito_nombre'] = $p['hito_nombre'];
						$kk[$i]['tarea_id'] = $p['tarea_id'];
						$kk[$i]['tarea_nombre'] = $p['tarea_nombre'];											
						$kk[$i]['categoria_id'] = $p['categoria_id'];
						$kk[$i]['categoria_nombre'] = $p['categoria_nombre'];
				//		$kk[$i]['familia_id'] = $p['familia_id'];
						$kk[$i]['finalizado'] = $p['finalizado'];
						$kk[$i]['total_prev'] = $p['total_prev'];
						$kk[$i]['total_horas'] = $p['1']+$p['2']+$p['3']+$p['4']+$p['5']+$p['6']+$p['7']+$p['8']+$p['9']+$p['10']+$p['11']+$p['12'];
						if ( ($p['total_prev']!=0) && isset($p['total_prev'])) {
							$kk[$i]['desv'] = $kk[$i]['total_horas']*100/$p['total_prev'];
						} else {
							$kk[$i]['desv'] = "-";
						}
						$kk[$i]['1'] = $p['1'];
						$kk[$i]['2'] = $p['2'];
						$kk[$i]['3'] = $p['3'];
						$kk[$i]['4'] = $p['4'];
						$kk[$i]['5'] = $p['5'];
						$kk[$i]['6'] = $p['6'];
						$kk[$i]['7'] = $p['7'];
						$kk[$i]['8'] = $p['8'];
						$kk[$i]['9'] = $p['9'];
						$kk[$i]['10'] = $p['10'];
						$kk[$i]['11'] = $p['11'];
						$kk[$i]['12'] = $p['12'];

					}
					$this->Session->delete('ExcelTarea');
					$this->set('data', $kk);
		}

		function proy_total( $id = null ) {
		if ( $id != null ) {
			$total = $this->Linea->query(
				"SELECT SUM(total_horas) as `Totala`
					FROM `lineas` as `Linea`
					WHERE `Linea`.`proyecto_id` = " . $id
					);
			if ( $total['0']['0']['Totala'] != null ) {
				return $total['0']['0']['Totala'];
			} else {
				return 0;
			}
		}		
		return 0;
    }

		function hito_total( $id = null ) {
		if ( $id != null ) {
			$total = $this->Linea->query(
				"SELECT SUM(total_horas) as `Totala`
					FROM `lineas` as `Linea`
					WHERE `Linea`.`hito_id` = " . $id
					);
			if ( $total['0']['0']['Totala'] != null ) {
				return $total['0']['0']['Totala'];
			} else {
				return 0;
			}			
		}		
		return 0;
    }

    function bilatucategoria ($id=null) {
            
            if ($id!=null) {
                $this->loadModel('Categoria');
                $cat = $this->Categoria->findById($id);
                return $cat['Categoria']['name'];
            }
    }

    function bilatufamilia ($id=null) {
            
            if ($id!=null) {
                $this->loadModel('Familia');
                $cat = $this->Familia->findById($id);
                return $cat['Familia']['name'];
            }
    }

	function bilatuusuario ($id=null) {
            
		if ($id!=null) {
			$this->loadModel('Usuario');
			$cat = $this->Usuario->findById($id);
			return $cat['Usuario']['name'];
		}
	}

	function horasyear($urtea,$proy) {

		$batu = $this->Linea->query(
				"select sum(total_horas) as batuketa
					from lineas
					where proyecto_id = " . $proy . " and YEAR(fecha)=" . $urtea
			);

		//var_dump($batu);
		return $batu[0][0]['batuketa'];
	}

    function relazioa() {
        
        
        // *****************************************************************************************************************
        // ***************************************************  PROYECTOS   ************************************************
        // *****************************************************************************************************************
        
        
        $urtea = "2012";
        if (!empty ($this->data['Linea']['year'])) {
        	$urtea = $this->data['Linea']['year'];
        }

        $conurtea=array('YEAR(Linea.fecha)' => $urtea);
           
        
        $con1[0] = "";

        $con3[0] = array('YEAR(Linea.fecha)' => $urtea);
        $con3[1] = array('89 = 89');
        $i=1;
        $k=2;



        //condiciones
        if (!empty($this->data)) {
							
            if (!empty ($this->data['Linea']['categoria_id'])) {
                $con1[$i]=array('Proyecto.categoria_id' => $this->data['Linea']['categoria_id']);
                $i+=1;
            }

            // Coordinador
            if (!empty ($this->data['Linea']['usuario_id'])) {
                $con1[$i]=array(
                                'OR'   =>  array(
                                     'Hito.usuario_id'=>$this->data['Linea']['usuario_id'],
                                     'Proyecto.usuario_id'  =>  $this->data['Linea']['usuario_id'],
                                     ));
                $i+=1;
            }

            //proyecto
            if (!empty($this->data['Linea']['proyecto_id'])) {
                $con1[$i]=array('Proyecto.id' => $this->data['Linea']['proyecto_id']);
                $i+=1;
            }

            // Situación
            if ((!empty ($this->data['Linea']['situacion']) && ($this->data['Linea']['situacion']!=-1) )) {
                $sit = $this->data['Linea']['situacion'];
                $con1[$i]=array('(Proyecto.total_horas * 100 / Proyecto.total_prev) >'.$sit);
                $i+=1;
            }

        } else {
            
            if (!empty ( $this->params['url']['q'] )) {
								$con1[$i] = array('OR' => array(
										'Proyecto.usuario_id'  =>  $this->Session->read('Usuario_id'),
										'Hito.usuario_id'  =>  $this->Session->read('Usuario_id')
											));
                                
            }
        }
        $this->set('miset',$con1);

        //desde - hasta
        if (((!empty ($this->data['Linea']['hasta'])) && (!empty($this->data['Linea']['desde']))
                && ($this->data['Linea']['hasta']!="") && ($this->data['Linea']['desde']!=""))) {
            $con3[$k]=array('Linea.fecha >=' => $this->data['Linea']['desde'],'Linea.fecha <=' => $this->data['Linea']['hasta']);
            $k+=1;
        }
			
		//tecnico_id
		//$this->Session->write("Tecnico",$this->data['Linea']['tecnico_id']);
		if ( (!empty ($this->data['Linea']['tecnico_id'])) && ( $this->data['Linea']['tecnico_id']!=""  )) {
            $con3[$k]=array('Linea.usuario_id' => $this->data['Linea']['tecnico_id']);
            $k+=1;

			// Si se selecciona tecnico_id hay que mostrar solo los proyectos del mismo
			$micon = array('OR' => array(
				'Linea.usuario_id'=>$this->data['Linea']['tecnico_id'],
				'Hito.usuario_id'=>$this->data['Linea']['tecnico_id']
				));

			$this->loadModel('Proyecto');

			$proytech = $this->Proyecto->query(
				"SELECT Proyecto.id, Proyecto.usuario_id, Proyecto.name, Proyecto.categoria_id, Proyecto.familia_id, 
					Proyecto.finalizado, Proyecto.total_prev, Proyecto.total_horas, Proyecto.created
							from proyectos as Proyecto
							left join tareas on tareas.proyecto_id = Proyecto.id
							left join hitos on hitos.proyecto_id = Proyecto.id
							where Proyecto.Usuario_id = " .$this->data['Linea']['tecnico_id'] . " or
							tareas.usuario_id = " . $this->data['Linea']['tecnico_id'] . " or
							hitos.usuario_id = " . $this->data['Linea']['tecnico_id'] . "
							group by Proyecto.id
							order by Proyecto.familia_id DESC, Proyecto.name"
			);

			$this->set("proytech",$proytech);
		}

        if ($con1!="") 
				{
        	$this->loadModel('Proyecto');
					$ikerproy = $this->Proyecto->Hito->find('all',array(
                                                        'group'=>'Proyecto.id',
														'order' => 'Proyecto.familia_id DESC,Proyecto.name',
                                                       // 'recursive'=>'-1'
															));
					$this->set("ikerproy",$ikerproy);
      	} else {
                $this->loadModel('Proyecto');
                $ikerproy = $this->Proyecto->find('all',array(
					// $ikerproy = $this->Proyecto->Hito->find('all',array(
                    // 
                        'conditions'=>$conurtea,
					 										'group'=>'Proyecto.id',	
					 										'order' => 'Proyecto.familia_id DESC,Proyecto.name',
					// 										'recursive'=>'-1'
															));
        }

				$this->Session->write("con3",$con3);
        //SUMA DE HORAS
        $this->loadModel('Linea');
        if ($con3=="") {
        	//Iker 16/01/2012
       		$proiektu =$this->Linea->find('all',array(
          //    'conditions' =>$con3,
          			'conditions' => $conurtea,
                    'order'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','MONTH(Linea.fecha)','YEAR(Linea.fecha)'),
                    'group'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','MONTH(Linea.fecha)','YEAR(Linea.fecha)'),
                    'fields'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','MONTH(Linea.fecha) as Mes',
                    'YEAR(Linea.fecha) as Urtea','Proyecto.total_prev','Proyecto.total_horas','SUM(Linea.total_horas) as Horas'),
                    'recursive'=>'1'
                ));
        } else {
        	//Iker 16/01/2012
        	// $con3[$k]=array('Linea.usuario_id' => $this->data['Linea']['tecnico_id']);
        	$con3[$k]=array('YEAR(Linea.fecha)' => $urtea);
                $proiektu =$this->Linea->find('all',array(
                    'conditions' =>$con3,
                    // 'conditions' => $conurtea,
                    'order'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','MONTH(Linea.fecha)','YEAR(Linea.fecha)'),
                    'group'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','MONTH(Linea.fecha)','YEAR(Linea.fecha)'),
                    'fields'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','MONTH(Linea.fecha) as Mes',
                    'YEAR(Linea.fecha) as Urtea', 'Proyecto.total_prev','Proyecto.total_horas','SUM(Linea.total_horas) as Horas'),
                    'recursive'=>'1'
                ));
        }

		//ezabatu
		$this->set("proiektu",$proiektu);
        // ARRAY Proiektuak
        $proiektuak=array();
        $cont=0;
        $lehena=0;
		if (empty ($this->data['Linea']['tecnico_id'])) 
		{
			foreach ( $ikerproy as $d ) 
			{
				$urtia = date("Y",strtotime($d['Proyecto']['created']));
	        	$proiektuak[$cont]=array(
	        		'year'				 => $urtia,
	        		'orduk'				 => $this->requestAction('/lineas/horasyear/'.$urtia."/".$d['Proyecto']['id']),
					'coordinador_id'     =>$d['Proyecto']['usuario_id'],
					'coordinador_nombre' =>$this->requestAction('/lineas/bilatuusuario/'.$d['Proyecto']['usuario_id']),
					'proyecto_id'        =>$d['Proyecto']['id'],
					'proyecto_nombre'    =>$d['Proyecto']['name'],
					'categoria_id'       =>$d['Proyecto']['categoria_id'],
					'categoria_nombre'   =>$this->requestAction('/lineas/bilatucategoria/'.$d['Proyecto']['categoria_id']),
					'familia_id'         =>$d['Proyecto']['familia_id'],
					'familia_nombre'     => $this->requestAction('/lineas/bilatufamilia/'.$d['Proyecto']['familia_id']),
					'finalizado'         =>$d['Proyecto']['finalizado'],
					'usuario_id'         =>$d['Proyecto']['usuario_id'],
					'usuario_nombre'     => $this->requestAction('/lineas/bilatuusuario/'.$d['Proyecto']['usuario_id']),
					'total_prev'         =>$d['Proyecto']['total_prev'],
					'total_horas'        =>$d['Proyecto']['total_horas'],
					'desv'               =>'0',
					'1'                  =>'0',
					'2'                  =>'0',
					'3'                  =>'0',
					'4'                  =>'0',
					'5'                  =>'0',
					'6'                  =>'0',
					'7'                  =>'0',
					'8'                  =>'0',
					'9'                  =>'0',
					'10'                 =>'0',
					'11'                 =>'0',
					'12'                 =>'0',
               );

               //foreach ($data as $d) {
          	foreach ($proiektu as $de) {
            	//if ( $de['Proyecto']['Proyecto']['id']==$d['Proyecto']['id'] ) {
              if (isset($de['Linea']['proyecto_id']) && isset($d['Proyecto']['id']))
              {
              	if ( $de['Linea']['proyecto_id']==$d['Proyecto']['id'] ) {

                	$proiektuak[$cont][$de['0']['Mes']]=$de['0']['Horas'];
                }
              }

            }

            //calculo del desvio
            if ( $proiektuak[$cont]['year'] == $urtea ) {
                if ( ($proiektuak[$cont]['total_prev']!=null) && ($proiektuak[$cont]['total_prev']!=0) ){
                	$proiektuak[$cont]['desv']=$proiektuak[$cont]['total_horas'] * 100 / $proiektuak[$cont]['total_prev'];
                } else {
                	$proiektuak[$cont]['desv']="---";
                }
            }
            $proiektuak[$cont]['ikerurtea'] = $urtea;
            $cont+=1;
        }	
			} 
			else 
			{
				foreach ( $proytech as $d ) 
				{
					$urtia = date("Y",strtotime($d['Proyecto']['created']));
	      			$proiektuak[$cont]=array(
	      				'year'				 => $urtia,
	        			'orduk'				 => $this->requestAction('/lineas/horasyear/'.$urtia."/".$d['Proyecto']['id']),
	        			'coordinador_id'=>$d['Proyecto']['usuario_id'],
	          			'coordinador_nombre'=>$this->requestAction('/lineas/bilatuusuario/'.$d['Proyecto']['usuario_id']),
	          			'proyecto_id'=>$d['Proyecto']['id'],
	          			'proyecto_nombre'=>$d['Proyecto']['name'],
	          			'categoria_id'=>$d['Proyecto']['categoria_id'],
	          			'categoria_nombre'=>$this->requestAction('/lineas/bilatucategoria/'.$d['Proyecto']['categoria_id']),
						'familia_id'=>$d['Proyecto']['familia_id'],
						'familia_nombre' => $this->requestAction('/lineas/bilatufamilia/'.$d['Proyecto']['familia_id']),
						'finalizado'=>$d['Proyecto']['finalizado'],
	          			'usuario_id'=>$d['Proyecto']['usuario_id'],
						'usuario_nombre' => $this->requestAction('/lineas/bilatuusuario/'.$d['Proyecto']['usuario_id']),
						'total_prev'=>$d['Proyecto']['total_prev'],
						'total_horas'=>$d['Proyecto']['total_horas'],
						'desv'=>'0',
						'1'=>'0',
						'2'=>'0',
						'3'=>'0',
						'4'=>'0',
						'5'=>'0',
						'6'=>'0',
						'7'=>'0',
						'8'=>'0',
						'9'=>'0',
						'10'=>'0',
						'11'=>'0',
						'12'=>'0',
	          		);
					foreach ($proiektu as $de) {
	          			//if ( $de['Proyecto']['Proyecto']['id']==$d['Proyecto']['id'] ) {
	          			if (isset($de['Linea']['proyecto_id']) && isset($d['Proyecto']['id'])) {
	          				if ( $de['Linea']['proyecto_id']==$d['Proyecto']['id'] ) {
	            				$proiektuak[$cont][$de['0']['Mes']]=$de['0']['Horas'];
	            			}
	          			}
          			}

        			//calculo del desvio
                    if ( $proiektuak[$cont]['year'] == $urtea ) {
              			if ( ($proiektuak[$cont]['total_prev']!=null) && ($proiektuak[$cont]['total_prev']!=0) ){
    	              		$proiektuak[$cont]['desv']=$proiektuak[$cont]['total_horas'] * 100 / $proiektuak[$cont]['total_prev'];
              			} else {
    	              		$proiektuak[$cont]['desv']="---";
              			}
                    } else {
                        $proiektuak[$cont]['desv']="ez";
                    }
                    $proiektuak[$cont]['ikerurtea2'] = $urtea;
          			$cont+=1;
	      		}
			}
		
			$this->set(compact('proiektuak'));


            // **********************************************************************************************************************************
            // ************************************************    HITOS      ***********************************************************
            // **********************************************************************************************************************************

      $con2 = "";
      $j=0;
      //condiciones
      if (!empty($this->data)) {
		// Técnico
      	if (!empty ($this->data['Linea']['tecnico_id'])) {
      		$con2[$j]=array('Hito.usuario_id' => $this->data['Linea']['tecnico_id']);
        	$j+=1;
      	}
    } 
			      
      $this->loadModel('Hito');
      if ($con2!="") {
      	$ikerhito = $this->Hito->find('all',array('conditions'=>$con2));
      } else {
      	$ikerhito = $this->Hito->find('all');
      }
      
			$this->set("ikerhito",$ikerhito);

      //SUMA DE HORAS
      $this->loadModel('Linea');
      if ($con2==""){
      	$hitoak=$this->Linea->find('all', array(
        	//'conditions' =>$conditions2,
			'conditions'=>$conurtea,
			'order'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','Linea.hito_id','MONTH(Linea.fecha)'),
			'group'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','Linea.hito_id','MONTH(Linea.fecha)'),
			'fields'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','Linea.hito_id','Linea.usuario_id','MONTH(Linea.fecha) as Mes',
			'Proyecto.total_prev','Proyecto.total_horas','SUM(Linea.total_horas) as Horas'),
			'recursive'=>'1'
		));
		} else {
      	$hitoak=$this->Linea->find('all', array(
        	'conditions' =>$con2,
			// 'conditions' =>$con3,
			// 'conditions' =>$conurtea,
			'order'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','Linea.hito_id','MONTH(Linea.fecha)'),
			'group'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','Linea.hito_id','MONTH(Linea.fecha)'),
			'fields'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','Linea.hito_id','Linea.usuario_id','MONTH(Linea.fecha) as Mes',
			'Proyecto.total_prev','Proyecto.total_horas','SUM(Linea.total_horas) as Horas'),
			'recursive'=>'1'
		));
	}

		// ARRAY HITUAK
		$hituak=array();
		$cont=0;
		$lehena=0;
		foreach ( $ikerhito as $d ) {
			if ( ($d['Hito']['horas_prev'] != 0) && ($d['Hito']['horas_prev']!=null) ) {
				$desvio =$d['Hito']['horas_real'] * 100 / $d['Hito']['horas_prev'];
			} else {
				$desvio = "---";
			}
			$hituak[$cont]=array(
                        'coordinador_id'=>$d['Hito']['usuario_id'],
                        'coordinador_nombre'=>$this->requestAction('/lineas/bilatuusuario/'.$d['Hito']['usuario_id']),
                        'proyecto_id'=>$d['Proyecto']['id'],
                        'proyecto_nombre'=>$d['Proyecto']['name'],
                        'hito_id'=>$d['Hito']['id'],
                        'hito_nombre'=>$d['Hito']['name'],
                        'categoria_id'=>$d['Proyecto']['categoria_id'],
                        'categoria_nombre'=>$this->requestAction('/lineas/bilatucategoria/'.$d['Proyecto']['categoria_id']),
													'finalizado'=>$d['Hito']['finalizado'],
                        'usuario_id'=>null,
                        'total_prev'=>$d['Hito']['horas_prev'],
                        'total_horas'=>$d['Hito']['horas_real'],
                        'desv'=>$desvio,
                        '1'=>'0',
                        '2'=>'0',
                        '3'=>'0',
                        '4'=>'0',
                        '5'=>'0',
                        '6'=>'0',
                        '7'=>'0',
                        '8'=>'0',
                        '9'=>'0',
                        '10'=>'0',
                        '11'=>'0',
                        '12'=>'0',
                );

                foreach ($hitoak as $de) {
                   if ( isset($de['Linea']['proyecto_id']) && (isset($d['Proyecto']['id']))
                     && isset($de['Linea']['hito_id'])      && isset($d['Hito']['id'])) {
                        if ( ($de['Linea']['proyecto_id']== $d['Proyecto']['id']) &&
                             ($de['Linea']['hito_id'] == $d['Hito']['id'] )
                            )
                            {
                                $hituak[$cont][$de['0']['Mes']]=$de['0']['Horas'];
                            }
                   }
               }


                //calculo del desvio
                if ( ($hituak[$cont]['total_prev']!=null) && ($hituak[$cont]['total_prev']!=0) ){
                    $hituak[$cont]['desv']=$hituak[$cont]['total_horas'] * 100 / $hituak[$cont]['total_prev'];
                } else {
                    $hituak[$cont]['desv']="---";
                }
               $cont+=1;
            }

            $this->set(compact('hituak'));

            // **********************************************************************************************************************************
            // ************************************************    TAREAS      ***********************************************************
            // **********************************************************************************************************************************

            $con4 = "";
            $l=0;
            //condiciones

            $con4[$l] = array('not'=>array('Tarea.id'=>null));
            $l+=1;
            if (!empty($this->data)) {
                if (!empty ($this->data['Linea']['tecnico_id'])) {
                    $con4[$l]=array('Hito.usuario_id' => $this->data['Linea']['tecnico_id']);
                    $l+=1;
                }
            } else {

            }

            $this->loadModel('Tarea');

            if ($con4!=""){
                $ikertarea = $this->Tarea->find('all');
            } else {
                $ikertarea = $this->Tarea->find('all',array('conditions'=>$con4));
            }

            
            $this->set("ikertarea",$ikertarea);

            //SUMA DE HORAS
            if ($con4=="") {
                $tareak1=$this->Linea->find('all', array(
    //                        'conditions' =>$conditions3,
    						'conditions'=>$conurtea,
                            'order'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','Linea.hito_id','Linea.tarea_id','MONTH(Linea.fecha)'),
                            'group'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','Linea.hito_id','Linea.tarea_id','MONTH(Linea.fecha)'),
                            'fields'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','Linea.hito_id','Linea.tarea_id','MONTH(Linea.fecha) as Mes',
                                             'Linea.usuario_id','Proyecto.total_prev','Proyecto.total_horas','SUM(Linea.total_horas) as Horas','Tarea.id','Tarea.name'),
                            'recursive'=>'1'
                        ));
            } else {
                $tareak1=$this->Linea->find('all', array(
                        'conditions' =>$con4,
						'conditions' =>$con3,
						'conditions' =>$conurtea,
                        'order'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','Linea.hito_id','Linea.tarea_id','MONTH(Linea.fecha)'),
                        'group'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','Linea.hito_id','Linea.tarea_id','MONTH(Linea.fecha)'),
                        'fields'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','Linea.hito_id','Linea.tarea_id','MONTH(Linea.fecha) as Mes',
                                         'Linea.usuario_id','Proyecto.total_prev','Proyecto.total_horas','SUM(Linea.total_horas) as Horas','Tarea.id','Tarea.name'),
                        'recursive'=>'1'
                    ));
            }
            //ezabatu
            $this->set("tareak1",$tareak1);

            // ARRAY TAREAK
            $aTareak=array();
            $cont=0;
            $lehena=0;
            foreach ( $ikertarea as $d ) {
                if ( ($d['Tarea']['horas_prev'] != 0) && ($d['Tarea']['horas_prev']!=null) ) {
                        $desvio =$d['Tarea']['horas_total'] * 100 / $d['Tarea']['horas_prev'];
                    } else {
                        $desvio = "---";
                }
                $aTareak[$cont]=array(
                        'coordinador_id'=>$d['Usuario']['id'],
                        'coordinador_nombre'=>$d['Usuario']['name'],
                        'proyecto_id'=>$d['Proyecto']['id'],
                        'proyecto_nombre'=>$d['Proyecto']['name'],
                        'hito_id'=>$d['Hito']['id'],
                        'hito_nombre'=>$d['Hito']['name'],
                        'categoria_id'=>$d['Proyecto']['categoria_id'],
                        'categoria_nombre'=>$this->requestAction('/lineas/bilatucategoria/'.$d['Proyecto']['categoria_id']),
												'finalizado'=>$d['Tarea']['finalizado'],
                        'tarea_id'=>$d['Tarea']['id'],
                        'tarea_nombre'=>$d['Tarea']['name'],
                        'usuario_id'=>null,
                        'total_prev'=>$d['Tarea']['horas_prev'],
                        'total_horas'=>$d['Tarea']['horas_total'],
                        'desv'=>$desvio,
                        '1'=>'0',
                        '2'=>'0',
                        '3'=>'0',
                        '4'=>'0',
                        '5'=>'0',
                        '6'=>'0',
                        '7'=>'0',
                        '8'=>'0',
                        '9'=>'0',
                        '10'=>'0',
                        '11'=>'0',
                        '12'=>'0',
                        'visible'=>'0',
                );

               foreach ($hitoak as $de) {
                   if ( isset($de['Linea']['proyecto_id']) && (isset($d['Proyecto']['id']))
                     && isset($de['Linea']['hito_id'])      && isset($d['Hito']['id'])) {
                        if ( ($de['Linea']['proyecto_id']== $d['Proyecto']['id']) &&
                             ($de['Linea']['hito_id'] == $d['Hito']['id'] )
                            )
                            {
                                $hituak[$cont][$de['0']['Mes']]=$de['0']['Horas'];
                            }
                   }
               }

                foreach ($tareak1 as $de) {
                    if (isset($de['Linea']['proyecto_id']) && isset($d['Proyecto']['id'])
                     && isset($de['Linea']['hito_id'])     && isset($d['Hito']['id'])
                     && isset($de['Linea']['tarea_id'])    && isset($d['Tarea']['id']))
                    {
                        if (($de['Linea']['proyecto_id'] == $d['Proyecto']['id'])
                        &&  ($de['Linea']['hito_id']     == $d['Hito']['id'])
                        &&  ($de['Linea']['tarea_id']    == $d['Tarea']['id']))
                        {
                            $aTareak[$cont][$de['0']['Mes']]=$de['0']['Horas'];
                            $aTareak[$cont]['visible']='1';
                            $aTareak[$cont]['usuario_id']=$de['Linea']['usuario_id'];
                        }
                   }

                }

                //si hay filtro por tecnico en la linea y no hay datos
                if (empty($tareak1)){
                    $aTareak[$cont]['visible']='0';
                }

                //calculo del desvio
                if ( ($aTareak[$cont]['total_prev']!=null) && ($aTareak[$cont]['total_prev']!=0) ){
                    $aTareak[$cont]['desv']=$aTareak[$cont]['total_horas'] * 100 / $aTareak[$cont]['total_prev'];
                } else {
                    $aTareak[$cont]['desv']="---";
                }

                $cont+=1;
            }
            $this->set(compact('aTareak'));

            // *********************************************************************************************************************************
            // **************************************** TOTAL DE HORAS por mes*******************************************************
            // *********************************************************************************************************************************

            $this->loadModel('Linea');

            if (!empty($this->data)) {
							$this->Session->write("aa",$con3);
               // if (!empty ($this->data['Linea']['tecnico_id'])) {
							if (!empty ( $this->params['url']['q'] )) { // con el usuario de login
                    $orduak=$this->Linea->find('all', array(
                           // 'conditions'    =>  array('Linea.usuario_id' => $this->data['Linea']['tecnico_id']),
							'conditions'	=> $con3,
							'conditions'	=> $conurtea,
                            'order'         =>  array('YEAR(Linea.fecha)','MONTH(Linea.fecha)','Linea.usuario_id'),
                            'group'         =>  array('YEAR(Linea.fecha)','MONTH(Linea.fecha)','Linea.usuario_id'),
                            'fields'        =>  array('Linea.usuario_id','Usuario.name','MONTH(Linea.fecha) as Mes',
                                             'SUM(Linea.total_horas) as Horas'),
                            'recursive'     =>  '1'
                        ));
							} else {
							$this->Session->write("bbbb","bbbb");
							$orduak=$this->Linea->find('all', array(
							'conditions'	=> $conurtea,
                           	'order'         =>  array('YEAR(Linea.fecha)','MONTH(Linea.fecha)'),
                            'group'         =>  array('YEAR(Linea.fecha)','MONTH(Linea.fecha)'),
                            'fields'        =>  array('MONTH(Linea.fecha) as Mes',
                                             'SUM(Linea.total_horas) as Horas'),
                            'recursive'     =>  '1'
                        ));
							}
            //    }
            } else {
				if (!empty ( $this->params['url']['q'] )) { // con el usuario de login
								$this->Session->write("bb","bb");
                $orduak=$this->Linea->find('all', array(
                        'conditions'    =>  array('Linea.usuario_id' => $this->Session->read('Usuario_id')),
                        'conditions'	=> $conurtea,
                        'conditions'    => array("2233"=>"2233"),
                        'order'         =>  array('YEAR(Linea.fecha)','Linea.usuario_id','MONTH(Linea.fecha)'),
                        'group'         =>  array('YEAR(Linea.fecha)','Linea.usuario_id','MONTH(Linea.fecha)'),
                        'fields'        =>  array('Linea.usuario_id','Usuario.name','MONTH(Linea.fecha) as Mes',
                                         'SUM(Linea.total_horas) as Horas'),
                        'recursive'     =>  '1'
                    ));
				} else {
					$this->Session->write("cc","cc");
					$orduak=$this->Linea->find('all', array(
						'conditions'=>$conurtea,
                        'order'         =>  array('YEAR(Linea.fecha)','Linea.usuario_id','MONTH(Linea.fecha)'),
                        'group'         =>  array('YEAR(Linea.fecha)','Linea.usuario_id','MONTH(Linea.fecha)'),
                        'fields'        =>  array('Linea.usuario_id','Usuario.name','MONTH(Linea.fecha) as Mes',
                                         'SUM(Linea.total_horas) as Horas'),
                        'recursive'     =>  '1'
                    ));
							}
            }
            
  
            $this->set('orduak',$orduak);

            // *********************************************************************************************************************************
            // **************************************** TOTAL DE HORAS por Categorias*******************************************************
            // *********************************************************************************************************************************

            if (!empty($this->data)) {
           //     if (!empty ($this->data['Linea']['tecnico_id'])) {
				$this->Session->write("iejjaaaa",$con3);
                $katorduak=$this->Linea->find('all', array(
                            //'conditions'    =>  array('Linea.usuario_id' => $this->data['Linea']['tecnico_id']),
                            'conditions'	=> $con3,
                            'conditions'	=> $conurtea,
							'order'         =>  array('Proyecto.categoria_id','MONTH(Linea.fecha)'),
                            'group'         =>  array('Proyecto.categoria_id','MONTH(Linea.fecha)'),
                            'fields'        =>  array('Proyecto.categoria_id','MONTH(Linea.fecha) as Mes',
                                             'SUM(Linea.total_horas) as Horas'),
                            'recursive'     =>  '1'
                        ));
            //    }
            } else {
							if (!empty ( $this->params['url']['q'] )) { // con el usuario de login
								$katorduak=$this->Linea->find('all', array(
                        'conditions'    =>  array('Linea.usuario_id' => $this->Session->read('Usuario_id')),
                        'conditions'	=> $conurtea,
                        'order'         =>  array('Proyecto.categoria_id','MONTH(Linea.fecha)'),
                        'group'         =>  array('Proyecto.categoria_id','MONTH(Linea.fecha)'),
                        'fields'        =>  array('Proyecto.categoria_id','MONTH(Linea.fecha) as Mes',
                                         'SUM(Linea.total_horas) as Horas'),
                        'recursive'     =>  '1'
                    ));
							} else {
								$katorduak=$this->Linea->find('all', array(
									'conditions' => $conurtea,
                        'order'         =>  array('Proyecto.categoria_id','MONTH(Linea.fecha)'),
                        'group'         =>  array('Proyecto.categoria_id','MONTH(Linea.fecha)'),
                        'fields'        =>  array('Proyecto.categoria_id','MONTH(Linea.fecha) as Mes',
                                         'SUM(Linea.total_horas) as Horas'),
                        'recursive'     =>  '1'
                    ));
							}
                
            }

            $this->loadModel('Categoria');
            $categorias = $this->Categoria->find('all');

            $kategoriaorduak=array();
            $cont=0;
            $lehena=0;
            foreach ( $categorias as $c ) {
                $kategoriaorduak[$cont]=array(
                        'categoria_id'=>$c['Categoria']['id'],
                        'categoria_nombre'=>$c['Categoria']['name'],
                        '1'=>'0',
                        '2'=>'0',
                        '3'=>'0',
                        '4'=>'0',
                        '5'=>'0',
                        '6'=>'0',
                        '7'=>'0',
                        '8'=>'0',
                        '9'=>'0',
                        '10'=>'0',
                        '11'=>'0',
                        '12'=>'0',
                );

                foreach ($katorduak as $k1) {
                    if ($k1['Proyecto']['categoria_id']==$c['Categoria']['id']) {
                        //$k[$cont][$k1['0']['Mes']] = $k1['0']['Horas'];
                        $kategoriaorduak[$cont][$k1['0']['Mes']] = $k1['0']['Horas'];
                    }
                }
                $cont=$cont+1;
            }
            $this->set('kategoriaorduak',$kategoriaorduak);


			// ***********************************************************************************************************************************
			// ************************************  Para el 80 / 020 ****************************************************************************
			// ***********************************************************************************************************************************
			$this->loadModel('Familia');
			$familias = $this->Familia->find('all');
			
      //if (!empty ($this->data['Linea']['tecnico_id'])) {
			if (!empty($this->data))
			{
				if ( !empty($this->data['Linea']['tecnico_id']) )
				{
					if ((empty($this->data['Linea']['desde'])) || (empty($this->data['Linea']['hasta'])))
					{
						$this->Session->write("1","1");
						$ochenta = $this->Linea->query(
							"select sum(Linea.total_horas) as suma,familia_id, Familia.name, MONTH(Linea.fecha) as Mes
							from lineas as Linea
							left join proyectos as Proyecto on (Linea.proyecto_id = Proyecto.id)
							left join familias as Familia on (Proyecto.familia_id = Familia.id)
							where Linea.usuario_id=" . $this->data['Linea']['tecnico_id'] . "
							 AND YEAR(Linea.fecha)=" .  $urtea ."
							group by Proyecto.familia_id,MONTH(Linea.fecha)"
							);
					} else { //if ((empty($this->linea['Linea']['desde'])) || (empty($this->data['Linea']['hasta'])) ) )
						$this->Session->write("222","222");
						$ochenta = $this->Linea->query(
							"select sum(Linea.total_horas) as suma,familia_id, Familia.name, MONTH(Linea.fecha) as Mes
							from lineas as Linea
							left join proyectos as Proyecto on (Linea.proyecto_id = Proyecto.id)
							left join familias as Familia on (Proyecto.familia_id = Familia.id)
							where Linea.usuario_id=" . $this->data['Linea']['tecnico_id'] . "
						 	 AND Linea.fecha >= '" . $this->data['Linea']['desde'] . "'
						   AND Linea.fecha <= '" . $this->data['Linea']['hasta'] . "'
						    AND YEAR(Linea.fecha)=" . $urtea ." 
							 group by Proyecto.familia_id,MONTH(Linea.fecha)"
							);
					} //if ((empty($this->linea['Linea']['desde'])) || (empty($this->data['Linea']['hasta'])) ) )
				}	else { //if ((!empty($this->data['Linea']['tecnico_id'])))
					if ((!empty ( $this->data['Linea']['desde'] )) || (!empty ( $this->data['Linea']['desde'] ))) { // con el usuario de login
						$this->Session->write("3","3");
						$ochenta = $this->Linea->query(
							"select sum(Linea.total_horas) as suma,familia_id, Familia.name, MONTH(Linea.fecha) as Mes
							from lineas as Linea
							left join proyectos as Proyecto on (Linea.proyecto_id = Proyecto.id)
							left join familias as Familia on (Proyecto.familia_id = Familia.id)
							where Linea.fecha >= '" . $this->data['Linea']['desde'] . "'
						   AND Linea.fecha <= '" . $this->data['Linea']['hasta'] . "' 
						   	 AND YEAR(Linea.fecha)=" . $urtea ." 
							group by Proyecto.familia_id,MONTH(Linea.fecha)"
							);
					} else {
							$this->Session->write("4","4");			
							$ochenta = $this->Linea->query( 
								"select sum(Linea.total_horas) as suma,familia_id, Familia.name, MONTH(Linea.fecha) as Mes
								from lineas as Linea
								left join proyectos as Proyecto on (Linea.proyecto_id = Proyecto.id)
								left join familias as Familia on (Proyecto.familia_id = Familia.id) 
								 AND YEAR(Linea.fecha)=" . $urtea ."  
								group by Proyecto.familia_id,MONTH(Linea.fecha)"
								);
					}
				}	//if ((!empty($this->data['Linea']['tecnico_id'])))
			} else {
					if (!empty ( $this->params['url']['q'] )) { // con el usuario de login
						$this->Session->write("5","5");			
						$ochenta = $this->Linea->query( 
							"select sum(Linea.total_horas) as suma,familia_id, Familia.name, MONTH(Linea.fecha) as Mes
							from lineas as Linea
							left join proyectos as Proyecto on (Linea.proyecto_id = Proyecto.id)
							left join familias as Familia on (Proyecto.familia_id = Familia.id)
							where Linea.usuario_id=" . $this->Session->read('Usuario_id') . "
							AND YEAR(Linea.fecha) = " . $urtea . "
							group by Proyecto.familia_id,MONTH(Linea.fecha)"
							);
          } else { //todos
						$this->Session->write("6","6");			
						$ochenta = $this->Linea->query( 
							"select sum(Linea.total_horas) as suma,familia_id, Familia.name, MONTH(Linea.fecha) as Mes
							from lineas as Linea
							left join proyectos as Proyecto on (Linea.proyecto_id = Proyecto.id)
							left join familias as Familia on (Proyecto.familia_id = Familia.id)
							WHERE YEAR(Linea.fecha) = " . $urtea . "
							group by Proyecto.familia_id,MONTH(Linea.fecha)"
							);
					}
					
			}
			
			
			$larogei=array();
            $cont=0;
            $lehena=0;
            foreach ( $familias as $c ) {
                $larogei[$cont]=array(
                        'familia_id'	=>$c['Familia']['id'],
												'familia_name'	=>$c['Familia']['name'],
                        'usuario_id'	=>'0',
												'usuario_name'	=>"",
                        '1'=>'0',
                        '2'=>'0',
                        '3'=>'0',
                        '4'=>'0',
                        '5'=>'0',
                        '6'=>'0',
                        '7'=>'0',
                        '8'=>'0',
                        '9'=>'0',
                        '10'=>'0',
                        '11'=>'0',
                        '12'=>'0',
                );

                foreach ($ochenta as $o) {
					$kontatu=-1;
                    if ($o['Proyecto']['familia_id']==$c['Familia']['id']) {
                        $kontatu=$kontatu+1;
                        $larogei[$cont][$o['0']['Mes']] = $o['0']['suma'];
                    }
                }
              $cont=$cont+1;  
            }	
				
			
			// AÑADIMOS LO REAL, LO PREVISTO Y LA DESVIACION SEGUN LA TABLA HORAS
			
			$this->loadModel('Hora');
			
      if (!empty ($this->data['Linea']['tecnico_id'])) {
						$beitu = $this->Hora->find('all',array('conditions'=>array(
																		'usuario_id' => $this->data['Linea']['tecnico_id']
																	)));
			} else {
				if (!empty ( $this->params['url']['q'] )) {
					$beitu = $this->Hora->find('all',array('conditions'=>array(
																'usuario_id' => $this->Session->read('Usuario_id')
																)));
				} else { //todos
					$beitu1 = $this->Hora->find('all', array(
						'group' 	=> array('familia_id'),
						'fields'	=> array('familia_id','SUM(enero) as enero', 'SUM(febrero) as febrero', 'SUM(marzo) as marzo' ,'SUM(abril) as abril', 
																'SUM(Mayo) as mayo', 'SUM(junio) as junio', 'SUM(julio) as julio', 'SUM(agosto) as agosto', 
																'SUM(septiembre) as septiembre', 'SUM(octubre) as octubre', 'SUM(noviembre) as noviembre', 
																'SUM(diciembre) as diciembre'	),
					
					));	
					 $katorduak=$this->Linea->find('all', array(
                    //'conditions'    =>  array('Linea.usuario_id' => $this->data['Linea']['tecnico_id']),
                    'conditions'		=> $con3,
                    'conditions'	=> $conurtea,
					'order'         =>  array('Proyecto.categoria_id','MONTH(Linea.fecha)'),
                    'group'         =>  array('Proyecto.categoria_id','MONTH(Linea.fecha)'),
                    'fields'        =>  array('Proyecto.categoria_id','MONTH(Linea.fecha) as Mes',
                                     'SUM(Linea.total_horas) as Horas'),
                    'recursive'     =>  '1'
                ));
				}
			}
			if (!empty($beitu)){
				foreach ($beitu as $b) {
					$hilabetea = -1;
					foreach ($larogei as $l) {
						$hilabetea = $hilabetea +1;
						if ( $l['familia_id'] == $b['Hora']['familia_id'] ) {
							$larogei[$hilabetea]['r_enero'] 	= $b['Hora']['enero'];
							$larogei[$hilabetea]['r_febrero'] 	= $b['Hora']['febrero'];
							$larogei[$hilabetea]['r_marzo'] 	= $b['Hora']['marzo'];
							$larogei[$hilabetea]['r_abril'] 	= $b['Hora']['abril'];
							$larogei[$hilabetea]['r_mayo'] 		= $b['Hora']['mayo'];
							$larogei[$hilabetea]['r_junio'] 	= $b['Hora']['junio'];
							$larogei[$hilabetea]['r_julio'] 	= $b['Hora']['julio'];
							$larogei[$hilabetea]['r_agosto'] 	= $b['Hora']['agosto'];
							$larogei[$hilabetea]['r_septiembre'] = $b['Hora']['septiembre'];
							$larogei[$hilabetea]['r_octubre'] 	= $b['Hora']['octubre'];
							$larogei[$hilabetea]['r_noviembre'] = $b['Hora']['noviembre'];
							$larogei[$hilabetea]['r_diciembre'] = $b['Hora']['diciembre'];
						}
					}
				}	
				
				$this->set('beitu',$beitu);
				
			} else {
				foreach ($beitu1 as $b) {
					$hilabetea = -1;
					foreach ($larogei as $l) {
						$hilabetea = $hilabetea +1;
						if ( $l['familia_id'] == $b['Hora']['familia_id'] ) {
							$larogei[$hilabetea]['r_enero'] 	= $b['0']['enero'];
							$larogei[$hilabetea]['r_febrero'] 	= $b['0']['febrero'];
							$larogei[$hilabetea]['r_marzo'] 	= $b['0']['marzo'];
							$larogei[$hilabetea]['r_abril'] 	= $b['0']['abril'];
							$larogei[$hilabetea]['r_mayo'] 		= $b['0']['mayo'];
							$larogei[$hilabetea]['r_junio'] 	= $b['0']['junio'];
							$larogei[$hilabetea]['r_julio'] 	= $b['0']['julio'];
							$larogei[$hilabetea]['r_agosto'] 	= $b['0']['agosto'];
							$larogei[$hilabetea]['r_septiembre'] = $b['0']['septiembre'];
							$larogei[$hilabetea]['r_octubre'] 	= $b['0']['octubre'];
							$larogei[$hilabetea]['r_noviembre'] = $b['0']['noviembre'];
							$larogei[$hilabetea]['r_diciembre'] = $b['0']['diciembre'];
						}
					}
				}
				
				$this->set('beitu',$beitu1);
					
			}
			
			
			
			$this->set('larogei',$larogei);	


            // **********************************************************************************************************************************
            // **********************************  SET-s para el form de filtros ****************************************************
            // ********************************************************************************************************************************

            $coo = $this->Linea->Usuario->find ('list');
            $this->set('coo',$coo);

            $proy = $this->Linea->Proyecto->find ('list');
            $this->set('proy',$proy);

            $kat = $this->Linea->Proyecto->Categoria->find ('list');
            $this->set('kat',$kat);

            $tek = $this->Linea->Usuario->find ('list');
            $this->set('tek',$tek);

            $hitos = $this->Linea->Hito->find('all');
            $this->set('hitos',$hitos);

            $tareas = $this->Linea->Tarea->find('all');
            $this->set('tareas',$tareas);

			// Iker: 16/01/2012
			$josebe=$this->Linea->find('all', array(
 	           'order'  =>  array('YEAR(Linea.fecha) DESC', 'MONTH(Linea.fecha)', 'Proyecto.familia_id', 'Proyecto.id','Proyecto.name','Linea.usuario_id','Usuario.name','YEAR(Linea.fecha)'),
               'group'  =>  array('Proyecto.familia_id','Proyecto.id','Proyecto.name','Linea.usuario_id','Usuario.name',
               					'MONTH(Linea.fecha)','YEAR(Linea.fecha)'),
               'fields' =>  array('Proyecto.familia_id','Proyecto.id','Proyecto.name','Linea.usuario_id','Usuario.name',
               					'MONTH(Linea.fecha) as Mes','YEAR(Linea.fecha) as Urtea','SUM(Linea.total_horas) as Horas'),
                        'recursive'     =>  '1'
                    ));
			$this->set('josebe',$josebe);
			
			// PARA TRASPASO A EXCEL
			$this->Session->write('ExcelProyecto',$proiektuak);
			$this->Session->write('ExcelHito',$hituak);		
			$this->Session->write('ExcelTarea',$aTareak);
			$this->Session->write('ExcelJosebe',$josebe);	
//			$this->Session->write('ikerproycond',$conditions1);
    }

    function index() {
			$this->Linea->recursive = 0;
			$this->paginate = array(
	                    'Linea' => array(
	                        'limit'=>'15',
	                        'conditions' => array(
	                            'Linea.usuario_id'=>$this->Session->read('Usuario_id')
	                        )),
	                    'order'=>array('created'=>'DESC'),
	                    'group'=>array('proyecto_id','hito_id')
	                    );
	                 $this->set('lineas',$this->paginate());
		}

    function view($id = null) {
			if (!$id) {
				$this->Session->setFlash(__('Invalid linea', true));
				$this->redirect(array('action' => 'index'));
			}
			$this->set('linea', $this->Linea->read(null, $id));
		}

		function add() {
			if (!empty($this->data)) {
				$this->Linea->create();
	                        //var_dump($this->data);
				if ($this->Linea->save($this->data)) {
					$this->Session->setFlash(__('The linea has been saved', true));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The linea could not be saved. Please, try again.', true));
				}
			}
			$proyectos = $this->Linea->Proyecto->find('list');
			$hitos = $this->Linea->Hito->find('list');
			$tareas = $this->Linea->Tarea->find('list');
			$usuarios = $this->Linea->Usuario->find('list');
			$this->set(compact('proyectos', 'hitos', 'tareas', 'usuarios'));
		}

    function guardarhoras() {
            //xdebug_break();
            if (!empty($this->data) && ($this->data!=null)) {
//                if ( !empty($this->data['Linea']['fechaseleccionada'])) {
//                    $this->redirect(array('controller' => 'lineas', 'action' => 'addhoras','?'=>array('q'=>$this->data['Linea']['fechaseleccionada'])));
//                }
  
                         foreach($this->data['Linea'] as $l){
                             if (isset ($l['fechaseleccionada'])) {
                            if ( ($l['fechaseleccionada']!=null) && ( $l['fechaseleccionada']!="" ) ) {
                                continue;
                            }
                             }
                            if ( ( $l['total_horas']!=NULL ) ) {
                                $j=array();
                                $j['Linea']=$l;
                                $ano=substr($l['fecha'],0,4);
                                $mes=substr($l['fecha'],5,2);
                                $dia=substr($l['fecha'],8,2);
                                
                                $j['Linea']['fecha']=array(
                                                        'month' => $mes ,
                                                        'day'   => $dia,
                                                        'year'  => $ano);

                                if (empty($l['tarea_id']) || ( $l['tarea_id']=="" )) {
                                    $con = array(
                                                'Linea.proyecto_id' => $l['proyecto_id'],
                                                'Linea.hito_id' => $l['hito_id'],
                                                'Linea.fecha' => $l['fecha']
                                        );
                                } else {
                                    $con = array(
                                                'Linea.proyecto_id' => $l['proyecto_id'],
                                                'Linea.hito_id' => $l['hito_id'],
                                                'Linea.tarea_id' => $l['tarea_id'],
                                                'Linea.fecha' => $l['fecha']
                                        );
                                }
                                $lin = $this->Linea->find('all', array(
                                                                    'conditions'=>$con
                                                                    ));


                                $this->Linea->create();
                                if ( $this->Linea->save($j) ) {

                                    // SI SON HORAS DE JUAN, NO RESTA
                                    if ($l['usuario_id']!=8){
                                        // ACTUALIZAMOS TOTALES DE HORAS
                                        //PROYECTO
                                        //obtenemos las horas actuales del proyecto
                                        //$proyectoa = $this->Linea->Proyecto->findById($this->data['Linea']['proyecto_id']);
                                        $proyectoa = $this->Linea->Proyecto->findById($l['proyecto_id']);

                                        $this->set('lin',$lin);
                                        //Actualizamos las horas del Hito
                                        $proy1 = array();
                                        $proy1['Proyecto']['id'] =$l['proyecto_id'];
                                        
                                        if ( (!empty($lin)) ) {
                                            $proy1['Proyecto']['total_horas'] =$proyectoa['Proyecto']['total_horas'] + $l['total_horas'] - $lin[0]['Linea']['total_horas'];
                                        } else {
                                            $proy1['Proyecto']['total_horas'] =$proyectoa['Proyecto']['total_horas'] + $l['total_horas'];
                                        }
                                     
                                        $this->Linea->Proyecto->save($proy1);
                                    
                                     } // if ($l['usuario_id'] != 8) <= SI ES JUAN


                                        //HITO
                                        //obtenemos las horas actuales del hito
                                        $hitoa = $this->Linea->Hito->findById($l['hito_id']);

                                        //Actualizamos las horas del Hito
                                        $hito1 = array();
                                        $hito1['Hito']['id'] =$l['hito_id'];
                                        //$hito1['Hito']['horas_real'] =$hitoa['Hito']['horas_real'] + $l['total_horas'];
                                        if ( (!empty($lin)) ) {
                                            $hito1['Hito']['horas_real'] =$hitoa['Hito']['horas_real'] + $l['total_horas'] - $lin[0]['Linea']['total_horas'];
                                        } else {
                                            $hito1['Hito']['horas_real'] =$hitoa['Hito']['horas_real'] + $l['total_horas'];
                                        }
                                        $this->Linea->Hito->save($hito1);

                                       
                                        //TAREA
                                        //miramos si tiene tarea relacionada
                                        //xdebug_break();
                                        if ((!empty($l['tarea_id'])) && ($l['total_horas']!=NULL)) {
          
                                            $tareaa = $this->Linea->Tarea->findById($l['tarea_id']);
                                            //$this->Session->write('tareaaaa',$tareaa);
                                            if ( !empty($tareaa)) {
                                                // Ya que no esta vacio, hay que actualizar los datos de la tarea
                                                $tare1 = array();
                                                $tare1['Tarea']['id'] =$l['tarea_id'];
                                                //$tare1['Tarea']['horas_total'] =$tareaa['Tarea']['horas_total'] + $l['total_horas'];
                                                if ( (!empty($lin)) ) {
                                                    $tare1['Tarea']['horas_total'] =$tareaa['Tarea']['horas_total'] + $l['total_horas']- $lin[0]['Linea']['total_horas'];
                                                } else {
                                                    $tare1['Tarea']['horas_total'] =$tareaa['Tarea']['horas_total'] + $l['total_horas'];
                                                }
                                                $this->Linea->Tarea->save($tare1);
                                            }
                                        }
                                   
                                        // REDIRIGIMOS
                                        //$this->redirect(array('controller'=>'proyectos','action' => 'view',$this->data['Linea']['proyecto_id'],'#'=>'azkena'));
                                } else {
                                    $this->Session->setFlash(__('Arazonbat eon dek', true));

                                }
                            }
                         }

                         //xdebug_break();
                         $request_params = Router::getParams();
                        if ( !empty($request_params['pass']) ) { // si addhoras/3
                            $this->redirect(array(
                                'action' => 'addhoras',$request_params['pass'][0],
                                '?'     => array('fetxa'=>$this->data['Linea']['fechaseleccionada'])
                            ));
                        }
                        if (!empty ( $this->params['url']['filtro'] )) {
                            if ( $_GET['filtro']=='coordino' ) {
                                $this->redirect(array(
                                            'action'=>'addhoras',
                                            '?'=>array('filtro'=>'coordino',
                                            'fetxa'=>$this->data['Linea']['fechaseleccionada'])
                                    ));
                            }
                            if ( $_GET['filtro']=='mistareas' ) {
                                $this->redirect(array(
                                            'action'=>'addhoras',
                                            '?'=>array('filtro'=>'mistareas',
                                                'fetxa'=>$this->data['Linea']['fechaseleccionada'])
                                        ));
                            }
                            if ( $_GET['fetxa']!='' ) {
                                $this->redirect(array(
                                            'action'=>'addhoras',
                                            '?'=>array('fetxa'=>$this->data['Linea']['fechaseleccionada'])));
                            }
                        }
            }
    }

    function addhoras($proyectoid = null, $hitoid=null, $tareaid=null ) {
                $i=0;
                //xdebug_break();
                // soy coordinador
                if (!empty ( $this->params['url']['filtro'] )) {
                    if ( $_GET['filtro']=='coordino' ) {
                        $condicion[$i]=array( 'Proyecto.usuario_id'=> $this->Session->read('Usuario_id'));
                        $i+=1;

                    }
                }

                if (!empty($this->params['url']['fetxa'])) {
										// Obligamos a que comience el lunes
                        $this->data['Linea']['fechaseleccionada']=$this->params['url']['fetxa'];
                }

                if (!empty ($this->data['Linea']['fechaseleccionada'])) {
										// Obligamos a que comience el lunes
										$eguna = date("N",strtotime($this->data['Linea']['fechaseleccionada']));										
										$kenketa = 0;

										// si es sabado 
										if ($eguna == 6 ) {
											$kenketa = 5;
										}
										//si es domingo 
										if ($eguna == 7) {
												$kenketa = 6;
										}
			

										if ( ($eguna != 1) && ( $kenketa == 0 ) ) {
				
											//$kenketa = 7 - $eguna;
											switch ($eguna) {
												case 2: //asteartea
													$kenketa=1;
													break;
												case 3: //asteazkena
													$kenketa=2;
													break;
												case 4: //osteguna
													$kenketa=3;
													break;
												case 5: //ostirala
													$kenteta=4;
													break;
											}													
									}
			

									$azkenday = strtotime(date("Y-m-d",strtotime($this->data['Linea']['fechaseleccionada'])) . " -" . $kenketa . " day");

									$i = $kenketa;
		  						//get timestamp for past/future date I want
		  						$pf_time = strtotime("-".$i." day");
		  						//format the date using the timestamp generated
		  						$pf_date = date("Y-m-d", $pf_time);
									//echo $pf_date;
	
	
	
	
                    //$desde =$this->data['Linea']['fechaseleccionada'];
										$desde =  date("Y-m-d",$azkenday);
										$this->data['Linea']['fechaseleccionada']=date("Y-m-d",$azkenday);
                    $hasta = strtotime(date("Y-m-d", strtotime( $this->data['Linea']['fechaseleccionada'])) . " +7 day");
                    $hasta = date("Y-m-d",$hasta);
                    $condicion[$i]=array( 'Linea.fecha >=' => $desde);
                    $i+=1;
                    $condicion[$i]=array( 'Linea.fecha <=' => $hasta);
                    $i+=1;
                }

                if ($proyectoid!=null) {
                    // PROYECTOS, CARGAMOS TODOS
					$fields = array('Proyecto.Id','Proyecto.name','Proyecto.total_horas','Proyecto.total_prev','Proyecto.finalizado');
                    $proyectos = $this->Linea->Proyecto->find('all',array(
																'fields'=>$fields,
                                                                'conditions' => array(
                                                                                    'Proyecto.id'=>$proyectoid),
                                                                                    
																'order' => 'Proyecto.familia_id DESC,Proyecto.name',
                                                                 )
															);
                } else {

                    if (!empty ( $this->params['url']['filtro'] )) {
                        if ( $_GET['filtro']=='coordino' ) {
                            $this->loadModel('Proyecto');

                            $conditions1=array(
                            'OR'   =>  array(
                             'Hito.usuario_id'=>$this->Session->read('Usuario_id'),
                             'Proyecto.usuario_id'  =>  $this->Session->read('Usuario_id')
                             )
                            );

							$fields = array('Proyecto.Id','Proyecto.name','Proyecto.total_horas','Proyecto.total_prev','Proyecto.finalizado');
                            $proyectos=$this->Proyecto->Hito->find('all', array(
														'fields' => $fields,
                                                        'conditions' =>$conditions1,
                                                        'group'=>array('Proyecto.id'),
														'order' => 'Proyecto.familia_id DESC,Proyecto.name',
                                                        'recursive'=>'2'
                                                        ));
                            $this->loadModel('Linea');
                                                        
                            // QUITAR ESTO PARA ACTIVAR FILTRO DE TECNICO POR HITO
                            //
                            $conditions1=array('Hito.usuario_id'=>$this->Session->read('Usuario_id'));
                            $hitos = $this->Linea->Hito->find('all',array(
                                                        'conditions' => $conditions1,
                                                        'group'=>array('Hito.id'),
                                                        ));
                         }

                      
                        if ( $_GET['filtro']=='mistareas' ) {
                            $this->loadModel('Proyecto');
							$proyectos = $this->Proyecto->query(
								"select distinct Proyecto.id, Proyecto.name,Proyecto.total_horas,Proyecto.total_prev, Proyecto.finalizado
											from proyectos as Proyecto
											left join hitos on (Proyecto.id = hitos.proyecto_id)
											left join tareas on (Proyecto.id = tareas.proyecto_id)
											where (hitos.usuario_id=" . $this->Session->read('Usuario_id') . " 
											or tareas.usuario_id=" . $this->Session->read('Usuario_id') . " 
											or Proyecto.usuario_id=" . $this->Session->read('Usuario_id') . "
											 ) AND (Proyecto.finalizado=0)" . " 
											order by Proyecto.familia_id, Proyecto.name"
							);
										
                            $this->loadModel('Hito');
							$this->Hito->recursive=2;
                            $hitos = $this->Hito->query(
                                    "select Hito.id, Hito.name, Hito.proyecto_id, Hito.usuario_id, Hito.horas_prev, Hito.horas_real,
									Hito.finalizado, Usuario.id, Usuario.name
                                        from hitos as Hito
                                        left join tareas as Tarea on (Hito.id = Tarea.hito_id)
                                        left join usuarios as Usuario on (Hito.usuario_id = Usuario.id)
                                        where Hito.usuario_id =" . $this->Session->read('Usuario_id') . "
                                            OR Tarea.usuario_id=" . $this->Session->read('Usuario_id') . "
                                        group by Hito.id"
										
										);

                            $this->loadModel('Tarea');
          
                            //$campos = array('Tarea.id','Tarea.name','Tarea.hito_id','Tarea.Proyecto_id','Tarea.usuario_id');                           
                            $tareas = $this->Tarea->find('all',array(
														//'fields' => $campos,
                                                        'conditions' => array(
                                                                         'Tarea.usuario_id'=>$this->Session->read('Usuario_id')),
                                                        'group'=>array('Tarea.id'),
														'recursive'=>2
                                                        ));
                        }
                    } else {
                        if (!isset($proyectos)) {
                    
                        }
                    }
                }
               
                if ($hitoid!=null) {
                    $condicion[$i]=array( 'Hito.id' => $hitoid);
                    $i+=1;
                }
                
                if ( empty($hitos) ) {
                    $hitos = $this->Linea->Hito->find('all',array('recursive'=>2));
                }

                if (empty($tareas)) {
                    $tareas = $this->Linea->Tarea->find('all',array('conditions'=>array('Tarea.usuario_id'=>$this->Session->read('Usuario_id'))));
                }

                $condicion[$i]=array('Linea.usuario_id'=>$this->Session->read('Usuario_id'));
                $i+=1;

                $azkenfetxak = $this->Linea->find('all', array(
                                            //'conditions' => array('Linea.usuario_id'=>$this->Session->read('Usuario_id')),
                                            'conditions' =>$condicion,
                                            'group' => 'Linea.fecha',
                                            'order' => 'Linea.fecha DESC',
                                            'limit'=>'5'
											,'recursive'=>-1
                                            ));


                $ultimos = $this->Linea->find('all',array(
                                            // 2011-05-16 iker hau kendu det...'conditions' => array('Linea.usuario_id'=>$this->Session->read('Usuario_id')),
																						'conditions' =>$condicion,
                                            'order' => 'Linea.fecha DESC',
                                            'limit' => '100'
                 ));

                if (!empty ( $this->params['url']['filtro'] )) {
                    if ( $_GET['filtro']=='coordino' ) {
                        if ( empty($proyectos) ) {
                            $this->Session->setFlash(__('No eres coordinador de ningun proyecto', true));
                            $this->redirect(array('action' => 'relacion'));
                        }
                    }
                }

                 
                $this->set(compact('proyectos', 'tareas','hitos','ultimos','azkenfetxak'));

    }
		
		function ver_ficha($id=null) {
			if (!$id) {
				$this->Session->setFlash(__('Invalid proyecto', true));
				$this->redirect(array('action' => 'relaciona'));
			}
      
			$this->Linea->recursive = 0;
      $proy=$this->Linea->Proyecto->findById($id);
      $this->set('proyecto',$proy);

			$lineak=$this->Linea->find('all', array(
                    'fields'=>array('Linea.fecha','Linea.usuario_id','SUM(Linea.total_horas) as suma','Linea.obs'),
                    'group'=>array('Linea.fecha','Linea.usuario_id'),
                    'conditions' => array('Linea.proyecto_id'=>$id),
                    'recursive'=>2
                    
                    ));
      $this->set('lineas',$lineak);
    }

}
?>