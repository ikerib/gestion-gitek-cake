<?php
class ProyectosController extends AppController {

	var $name = 'Proyectos';

        function beforeFilter(){
		$this->checkSession();
	}

	function finalizar ($id=null) {
		if ($id!=null) {
			
			$this->Proyecto->id = $id;
			$this->Proyecto->saveField('finalizado',1);
			
			$this->redirect($this->referer());
			
		}
	}

	function index() {
		$this->Proyecto->recursive = 0;
		$this->set('proyectos', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid proyecto', true));
			$this->redirect(array('action' => 'index'));
		}
                $this->Proyecto->recursive = 2;
                $this->paginate = array(
                    'order'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','Linea.hito_id','MONTH(Linea.fecha)'),
                    'group'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','Linea.hito_id','MONTH(Linea.fecha)'),
                    'fields'=>array('Proyecto.categoria_id','Proyecto.usuario_id','Linea.proyecto_id','Linea.hito_id','MONTH(Linea.fecha) as Mes','Proyecto.total_prev','Proyecto.total_horas','SUM(Linea.total_horas) as Horas'),

                );
		$usuarios = $this->Proyecto->Usuario->find('list');
		//$this->set('proyecto', $this->Proyecto->read(null, $id));
		$this->set('proyecto', $this->Proyecto->find('first',array(	
														'conditions'=>array('Proyecto.id'=>$id),

														)));
		$this->set('usuarios',$usuarios);
	}

	function add() {
		if (!empty($this->data)) {
			$this->Proyecto->create();
			if ($this->Proyecto->save($this->data)) {
				$this->Session->setFlash(__('The proyecto has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proyecto could not be saved. Please, try again.', true));
			}
		}
		$usuarios = $this->Proyecto->Usuario->find('list');
		$categorias = $this->Proyecto->Categoria->find('list');
		$familias = $this->Proyecto->Familia->find('list');
		$this->set(compact('usuarios', 'categorias', 'familias'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid proyecto', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Proyecto->save($this->data)) {
				//$this->Session->setFlash(__('The proyecto has been saved', true));
				//$this->redirect(array('action' => 'index'));
                            $this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('The proyecto could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Proyecto->read(null, $id);
		}
		$usuarios = $this->Proyecto->Usuario->find('list');
		$categorias = $this->Proyecto->Categoria->find('list');
		$familias = $this->Proyecto->Familia->find('list');
		$this->set(compact('usuarios', 'categorias', 'familias'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for proyecto', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Proyecto->delete($id)) {
			$this->Session->setFlash(__('Proyecto deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Proyecto was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>