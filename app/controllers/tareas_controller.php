<?php
class TareasController extends AppController {

	var $name = 'Tareas';

        function beforeFilter(){
		$this->checkSession();
	}

	function finalizar ($id=null) {
		if ($id!=null) {
			
			$this->Tarea->id = $id;
			$this->Tarea->saveField('finalizado',1);
			
			$this->redirect($this->referer());
			
		}
	}

	function index() {
		$this->Tarea->recursive = 0;
		$this->set('tareas', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid tarea', true));
			$this->redirect(array('action' => 'index'));
		}
                $this->Tarea->recursive = 2;
		$this->set('tarea', $this->Tarea->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Tarea->create();
			if ($this->Tarea->save($this->data)) {
				$this->Session->setFlash(__('The tarea has been saved', true));
				//$this->redirect(array('action' => 'index'));
                                $this->redirect(array('controller'=>'proyectos','actoion'=>'view',$this->data['Tarea']['proyecto_id']));
			} else {
				$this->Session->setFlash(__('The tarea could not be saved. Please, try again.', true));
			}
		}
		$proyectos = $this->Tarea->Proyecto->find('list');
		$hitos = $this->Tarea->Hito->find('list');
		$usuarios = $this->Tarea->Usuario->find('list');
		$this->set(compact('proyectos', 'hitos', 'usuarios'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid tarea', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Tarea->save($this->data)) {
				//$this->Session->setFlash(__('The tarea has been saved', true));
				//$this->redirect(array('action' => 'index'));
                                $this->redirect(array('controller'=>'proyectos','action'=>'view',$this->data['Tarea']['proyecto_id']));
			} else {
				$this->Session->setFlash(__('The tarea could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Tarea->read(null, $id);
		}
		$proyectos = $this->Tarea->Proyecto->find('list');
		$hitos = $this->Tarea->Hito->find('list');
		$usuarios = $this->Tarea->Usuario->find('list');
		$this->set(compact('proyectos', 'hitos', 'usuarios'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for tarea', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Tarea->delete($id)) {
			$this->Session->setFlash(__('Tarea deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Tarea was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>