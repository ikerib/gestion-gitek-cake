<?php
class UsuariosController extends AppController {

	var $name = 'Usuarios';
        var $components = array('Cookie');

        
function phpinformazioa () {
}
	function index() {
		$this->Usuario->recursive = 0;
		$this->set('usuarios', $this->paginate());
	}

        function selecciona() {
                $this->layout='login';
		$this->Usuario->recursive = 0;
		$this->set('usuarios', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid usuario', true));
			$this->redirect(array('action' => 'index'));
		}
                $this->Usuario->recursive = 2;
		$this->set('usuario', $this->Usuario->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Usuario->create();
			if ($this->Usuario->save($this->data)) {
				$this->Session->setFlash(__('El usuario ha sido añadido correctamente.', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The usuario could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid usuario', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Usuario->save($this->data)) {
				$this->Session->setFlash(__('The usuario has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The usuario could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Usuario->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for usuario', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Usuario->delete($id)) {
			$this->Session->setFlash(__('Usuario deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Usuario was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}

        function login() {
 
            if (empty($this->data)) {
			$this->Session->setFlash(__('Usuario no válido, seleccione usuario:', true));
			$this->redirect(array('action' => 'index'));
            }
            if (!empty($this->data)) {
                $this->Session->write('Usuario_id', $this->data['Usuario']['id']);
                $this->Cookie->write('Gestion.Usuario_id',$this->data['Usuario']['id']);

		$usu= $this->Usuario->findById($this->data['Usuario']['id']);
                $this->set('usu',$usu);

                if ($usu['Usuario']['admin']=='1'){
                    $this->Session->write('admin','1');
                    $this->Cookie->write('Gestion.Admin','1');
                } else {
                    $this->Session->write('admin','0');
                    $this->Cookie->write('Gestion.Admin','0');
                }

                if ($usu['Usuario']['gestion']=='1'){
                    $this->Session->write('gestion','1');
                    $this->Cookie->write('Gestion.gestion','1');
                } else {
                    $this->Session->write('gestion','0');
                    $this->Cookie->write('Gestion.gestion','0');
                }

                $this->Cookie->write('Gestion.Usuario',$usu['Usuario']['name']);
                $this->Session->write('Usuario', $usu['Usuario']['name']);
                //$this->redirect(array('controller'=>'lineas','action' => 'relacion','?'=>array('q'=>'login')));
                                //array('controller'=>'lineas','action'    =>'addhoras','?'         => array('filtro'=>'mistareas')
                $this->redirect(array(
                    'controller'=>'lineas',
                    'action'    =>'addhoras',
                    '?'         => array('filtro'=>'mistareas')
                    ));

            }
        }

        function logout() {
            $this->Session->delete('Usuario_id');
            $this->Session->delete('Usuario');
            $this->Session->delete('admin');
            $this->Session->delete('gestion');
          //  $this->Cookie->del('Gestion.Usuario_id');
            //$this->Cookie->del('Gestion.Usuario');
            $this->Cookie->destroy();


            $this->redirect('/login');
        }
}
?>