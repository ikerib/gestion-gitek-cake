<?php
/* Categorias Test cases generated on: 2010-12-06 01:12:51 : 1291595331*/
App::import('Controller', 'Categorias');

class TestCategoriasController extends CategoriasController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class CategoriasControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.categoria', 'app.proyecto', 'app.usuario', 'app.hito', 'app.linea', 'app.tarea');

	function startTest() {
		$this->Categorias =& new TestCategoriasController();
		$this->Categorias->constructClasses();
	}

	function endTest() {
		unset($this->Categorias);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
?>