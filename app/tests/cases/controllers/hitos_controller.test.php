<?php
/* Hitos Test cases generated on: 2010-12-06 01:12:58 : 1291595338*/
App::import('Controller', 'Hitos');

class TestHitosController extends HitosController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class HitosControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.hito', 'app.usuario', 'app.linea', 'app.proyecto', 'app.categoria', 'app.tarea');

	function startTest() {
		$this->Hitos =& new TestHitosController();
		$this->Hitos->constructClasses();
	}

	function endTest() {
		unset($this->Hitos);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
?>