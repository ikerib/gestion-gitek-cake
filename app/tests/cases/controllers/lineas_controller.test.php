<?php
/* Lineas Test cases generated on: 2010-12-06 01:12:07 : 1291595347*/
App::import('Controller', 'Lineas');

class TestLineasController extends LineasController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class LineasControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.linea', 'app.proyecto', 'app.usuario', 'app.hito', 'app.tarea', 'app.categoria');

	function startTest() {
		$this->Lineas =& new TestLineasController();
		$this->Lineas->constructClasses();
	}

	function endTest() {
		unset($this->Lineas);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
?>