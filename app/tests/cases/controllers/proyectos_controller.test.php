<?php
/* Proyectos Test cases generated on: 2010-12-06 01:12:14 : 1291595354*/
App::import('Controller', 'Proyectos');

class TestProyectosController extends ProyectosController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ProyectosControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.proyecto', 'app.usuario', 'app.hito', 'app.linea', 'app.tarea', 'app.categoria');

	function startTest() {
		$this->Proyectos =& new TestProyectosController();
		$this->Proyectos->constructClasses();
	}

	function endTest() {
		unset($this->Proyectos);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
?>