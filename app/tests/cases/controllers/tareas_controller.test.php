<?php
/* Tareas Test cases generated on: 2010-12-06 01:12:26 : 1291595366*/
App::import('Controller', 'Tareas');

class TestTareasController extends TareasController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class TareasControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.tarea', 'app.proyecto', 'app.usuario', 'app.hito', 'app.linea', 'app.categoria');

	function startTest() {
		$this->Tareas =& new TestTareasController();
		$this->Tareas->constructClasses();
	}

	function endTest() {
		unset($this->Tareas);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
?>