<div class="categorias view">
<h2><?php  __('Categoria');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $categoria['Categoria']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $categoria['Categoria']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $categoria['Categoria']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $categoria['Categoria']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Acciones'); ?></h3>
        <?php if($this->Session->read('admin') == '0') :        ?>
        <ul>
		<li><?php echo $this->Html->link(__('Nueva Linea', true),               array('controller'=>'lineas', 'action' => 'addajax')); ?></li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Vista Proyecto-Hito', true),       array('controller'=>'lineas', 'action' => 'index2')); ?></li>
                <li><?php echo $this->Html->link(__('Vista Proyecto-Hito-Tarea', true), array('controller'=>'lineas', 'action' => 'index3')); ?></li>
                <li><?php echo $this->Html->link(__('Vista Proyecto', true),            array('controller'=>'lineas', 'action' => 'indexajax')); ?></li>
	</ul>
        <?php else : ?>
	<ul>
                <li><?php echo $this->Html->link(__('Lista Categorias', true),          array('controller'=>'categorias','action' => 'index')); ?> </li>
                <li><?php echo $this->Html->link(__('Nuevo Categoria', true),           array('controller'=>'categorias','action' => 'add')); ?> </li>
                <li><?php echo $this->Html->link(__('Editar Categoria', true),          array('controller'=>'categorias','action' => 'edit', $categoria['Categoria']['id'])); ?> </li>
				<li><?php echo $this->Html->link(__('Eliminar Categoria', true),        array('controller'=>'categorias','action' => 'delete', $categoria['Categoria']['id']), null, sprintf(__('Seguro que deseas eliminar la categoria # %s?', true), $categoria['Categoria']['name'])); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Proyectos', true),           array('controller' => 'proyectos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nuevo Proyecto', true),            array('controller' => 'proyectos', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Hitos', true),               array('controller' => 'hitos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nuevo Hito', true),                array('controller' => 'hitos', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Tareas', true),              array('controller' => 'tareas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Tarea', true),               array('controller' => 'tareas', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Lineas', true),              array('controller' => 'lineas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Linea', true),               array('controller' => 'lineas', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Categorias', true),          array('controller' => 'categorias', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Categoria', true),           array('controller' => 'categorias', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Usuarios', true),            array('controller' => 'usuarios', 'action' => 'index')); ?> </li>
                <li><?php echo $this->Html->link(__('Nuevo Usuario', true),             array('controller' => 'usuarios', 'action' => 'add')); ?> </li>
	</ul>
        <?php endif; ?>
</div>
<div class="related">
	<h3><?php __('Related Proyectos');?></h3>
	<?php if (!empty($categoria['Proyecto'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Num Oferta'); ?></th>
		<th><?php __('Usuario Id'); ?></th>
		<th><?php __('Codigo'); ?></th>
		<th><?php __('Total Horas'); ?></th>
		<th><?php __('Total Prev'); ?></th>
		<th><?php __('Categoria Id'); ?></th>
		<th><?php __('Obs'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($categoria['Proyecto'] as $proyecto):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $proyecto['id'];?></td>
			<td><?php echo $proyecto['name'];?></td>
			<td><?php echo $proyecto['num_oferta'];?></td>
			<td><?php echo $proyecto['usuario_id'];?></td>
			<td><?php echo $proyecto['codigo'];?></td>
			<td><?php echo $proyecto['total_horas'];?></td>
			<td><?php echo $proyecto['total_prev'];?></td>
			<td><?php echo $proyecto['categoria_id'];?></td>
			<td><?php echo $proyecto['obs'];?></td>
			<td><?php echo $proyecto['created'];?></td>
			<td><?php echo $proyecto['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'proyectos', 'action' => 'view', $proyecto['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'proyectos', 'action' => 'edit', $proyecto['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'proyectos', 'action' => 'delete', $proyecto['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $proyecto['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proyecto', true), array('controller' => 'proyectos', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
