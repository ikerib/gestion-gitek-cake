     <?php

      class ThickboxHelper extends AppHelper {



          var $helpers = array('Javascript', 'Html');

          /**
   7.
           * Set properties – DOM ID, Height and Width, Type of thickbox window – inline or ajax
   8.
           *
   9.
           * @param array $options
  10.
           */

          function setProperties($options = array())

          {

              if(!isset($options['type']))

              {

                  $options['type'] = 'inline';

              }

              $this->options = $options;

          }

          function setPreviewContent($content)

          {

              $this->options['previewContent'] = $content;

          }

          function setMainContent($content)
          {
              $this->options['mainContent'] = $content;
          }

          function reset()
            {

              $this->options = array();

          }



          function output()

          {

              extract($this->options);

              if($type=='inline')

              {

                  $href = '#TB_inline?';

                  $href .= '&inlineId='.$id;

              }

              elseif($type=='ajax')

              {

                  $ajaxUrl = $this->Html->url($ajaxUrl);

                  $href = $ajaxUrl.'?';

              }



              if(isset($height))

              {

                  $href .= '&height='.$height;

              }

              if(isset($width))

              {

                  $href .= '&width='.$width;

              }




              $output = "<a class='thickbox' href='".$href."'>'.$previewContent.'</a>";



              if($type=='inline')

              {

                  $output .= "<div id='".$id."' style='display:none;'>".$mainContent."</div>";

              }



              unset($this->options);



              return $output;

          }

          function beforeRender()
          {

            $out = $this->Html->css('thickbox').'<script src="'.$this->Html->url('/js/thickbox-compressed.js').'"></script>';
//            $out = $this->Html->css('thickbox').$javascript->link('thickbox-compressed');;
           
            $view =& ClassRegistry::getObject('view');

              $view->addScript($out);

          }
}
?>

