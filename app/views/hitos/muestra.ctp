<div class="related" id="divHitos">
	<h3><?php __('Hitos relacionados');?></h3>
	<?php if (!empty($proyecto['Hito'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Name'); ?></th>
		<th><?php __('Horas Prev'); ?></th>
		<th><?php __('Horas Real'); ?></th>
		<th><?php __('Obs'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
                <?php if($this->Session->read('admin') == '1') :        ?>
                    <th class="actions"><?php __('Actions');?></th>
                <?php endif; ?>
	</tr>
	<?php
		$i = 0;
		foreach ($proyecto['Hito'] as $hito):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
                <?php
                    //calculamos el semaforo
                    // si 50% de las horas = amarillo
                    // si 70% de las horas = naranja
                    // si 100% de las horas = rojo
                    $semaforo ="ezer";
                    $r=0;
                    if ( !empty($hito['horas_prev']) && (!empty ($hito['horas_real'])) && ($hito['horas_prev']!=0) && ($hito['horas_real']!=0)) {

                        $r = $hito['horas_real'] * 100 / $hito['horas_prev'];

                        if ( ($r > 50) && ($r<70) ) {
                            $semaforo = 'oria';
                        } elseif ( ($r > 70) && ($r < 100)) {
                            $semaforo = 'naranja';
                        } elseif ( $r > 100 ) {
                            $semaforo = 'gorria';
                        } else {
                            $semaforo = 'berdea';
                        }
                    }
                ?>
		<tr<?php echo $class;?>>
			<td <?php echo "class=$semaforo"; ?>><?php echo $hito['name'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $hito['horas_prev'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $hito['horas_real'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $hito['obs'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $hito['created'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $hito['updated'];?></td>
                        <?php if($this->Session->read('admin') == '1') :        ?>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'hitos', 'action' => 'view', $hito['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'hitos', 'action' => 'edit', $hito['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'hitos', 'action' => 'delete', $hito['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $hito['id'])); ?>
			</td>
                        <?php endif; ?>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

        <?php if($this->Session->read('admin') == '1') :        ?>
	<div class="actions">
		<ul>
                    <li><a href="#" id="newHito">Agregar Hito</a> </li>
                    <script type="text/javascript" >

                        $("#newHito").click(function () {
                            $("#addHito").slideToggle("slow");
                        });
                    </script>
		</ul>
            <div id="addHito" style="display: none">
                <?php
                echo $this->Form->create('Hito');
                echo $this->Form->input('name');
		echo $this->Form->input('horas_prev');
		echo $this->Form->input('horas_real');
		echo $this->Form->input('obs');
		echo $this->Form->hidden($proyecto['Proyecto']['id']);
                echo $ajax->submit('Agregar hito', array('url'=> array('controller'=>'hitos', 'action'=>'addajax'), 'update' => 'divHitos'));
                ?>
            </div>
	</div>
        <?php endif; ?>
</div>