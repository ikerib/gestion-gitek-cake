<div class="hitos view">
<h2><?php  __('Hito');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $hito['Hito']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $hito['Hito']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Horas Prev'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $hito['Hito']['horas_prev']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Horas Real'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $hito['Hito']['horas_real']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Obs'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $hito['Hito']['obs']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Proyecto'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($hito['Proyecto']['name'], array('controller' => 'proyectos', 'action' => 'view', $hito['Proyecto']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $hito['Hito']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $hito['Hito']['updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Acciones'); ?></h3>
        <?php if($this->Session->read('admin') == '0') :        ?>
        <ul>
		<li><?php echo $this->Html->link(__('Nueva Linea', true),               array('controller'=>'lineas', 'action' => 'addajax')); ?></li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Vista Proyecto-Hito', true),       array('controller'=>'lineas', 'action' => 'index2')); ?></li>
                <li><?php echo $this->Html->link(__('Vista Proyecto-Hito-Tarea', true), array('controller'=>'lineas', 'action' => 'index3')); ?></li>
                <li><?php echo $this->Html->link(__('Vista Proyecto', true),            array('controller'=>'lineas', 'action' => 'indexajax')); ?></li>
	</ul>
        <?php else : ?>
	<ul>
                <li><?php echo $this->Html->link(__('Lista Hitos', true),               array('controller'=>'hitos','action' => 'index')); ?> </li>
                <li><?php echo $this->Html->link(__('Nuevo Hito', true),                array('controller'=>'hitos','action' => 'add')); ?> </li>
                <li><?php echo $this->Html->link(__('Editar Hito', true),               array('controller'=>'hitos','action' => 'edit', $hito['Hito']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Eliminar Hito', true),             array('controller'=>'hitos','action' => 'delete', $hito['Hito']['id']), null, sprintf(__('Seguro que deseas eliminar el hito # %s?', true), $hito['Hito']['name'])); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Proyectos', true),           array('controller' => 'proyectos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nuevo Proyecto', true),            array('controller' => 'proyectos', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Hitos', true),               array('controller' => 'hitos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nuevo Hito', true),                array('controller' => 'hitos', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Tareas', true),              array('controller' => 'tareas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Tarea', true),               array('controller' => 'tareas', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Lineas', true),              array('controller' => 'lineas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Linea', true),               array('controller' => 'lineas', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Categorias', true),          array('controller' => 'categorias', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Categoria', true),           array('controller' => 'categorias', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Usuarios', true),            array('controller' => 'usuarios', 'action' => 'index')); ?> </li>
                <li><?php echo $this->Html->link(__('Nuevo Usuario', true),             array('controller' => 'usuarios', 'action' => 'add')); ?> </li>
	</ul>
        <?php endif; ?>
</div>

<div class="related">
	<h3><?php __('Tareas relacionadas');?></h3>
	<?php if (!empty($hito['Tarea'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Name'); ?></th>
		<th><?php __('Proyecto Id'); ?></th>
		<th><?php __('Usuario Id'); ?></th>
		<th><?php __('Horas Prev'); ?></th>
		<th><?php __('Horas Total'); ?></th>
		<th><?php __('Obs'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
                <?php if($this->Session->read('admin') == '1') :        ?>
                    <th class="actions"><?php __('Actions');?></th>
                <?php endif; ?>
	</tr>
	<?php
		$i = 0;
		foreach ($hito['Tarea'] as $tarea):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
              <?php
                        //calculamos el semaforo
                        // si 50% de las horas = amarillo
                        // si 70% de las horas = naranja
                        // si 100% de las horas = rojo
                        $semaforo ="ezer";
                        $r=0;
                        if ( !empty($tarea['horas_prev']) && (!empty ($tarea['horas_total'])) && ($tarea['horas_prev']!=0) && ($tarea['horas_total']!=0)) {

                            $r = $tarea['horas_total'] * 100 / $tarea['horas_prev'];

                            if ( ($r > 50) && ($r<70) ) {
                                $semaforo = 'oria';
                            } elseif ( ($r > 70) && ($r < 100)) {
                                $semaforo = 'naranja';
                            } elseif ( $r > 100 ) {
                                $semaforo = 'gorria';
                            } else {
                                $semaforo = 'berdea';
                            }
                        }
                    ?>
		<tr<?php echo $class;?>>
			<td <?php echo "class=$semaforo"; ?>><?php echo $tarea['name'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php if (!empty($tarea['Proyecto'])) echo $tarea['Proyecto']['name'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php if (!empty($tarea['Usuario'])) echo $tarea['Usuario']['name'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $tarea['horas_prev'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $tarea['horas_total'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $tarea['obs'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $tarea['created'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $tarea['modified'];?></td>
                        <?php if($this->Session->read('admin') == '1') :        ?>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'tareas', 'action' => 'view', $tarea['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'tareas', 'action' => 'edit', $tarea['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'tareas', 'action' => 'delete', $tarea['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $tarea['id'])); ?>
			</td>
                        <?php endif; ?>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
    <?php if($this->Session->read('admin') == '1') :        ?>
	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Tarea', true), array('controller' => 'tareas', 'action' => 'add'));?> </li>
		</ul>
	</div>
    <?php endif; ?>
</div>
<div class="related">
	<h3><?php __('Lineas relacionas');?></h3>
	<?php if (!empty($hito['Linea'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Proyecto Id'); ?></th>
		<th><?php __('Tarea Id'); ?></th>
		<th><?php __('Usuario Id'); ?></th>
		<th><?php __('Fecha'); ?></th>
		<th><?php __('Total Horas'); ?></th>
		<th><?php __('Obs'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($hito['Linea'] as $linea):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
                        <td><?php if (!empty ($linea['Proyecto'])) echo $linea['Proyecto']['name'];?></td>
			<td><?php if (!empty ($linea['Tarea'])) echo $linea['Tarea']['name'];?></td>
			<td><?php if (!empty ($linea['Usuario'])) echo $linea['Usuario']['name'];?></td>
			<td><?php echo $linea['fecha'];?></td>
			<td><?php echo $linea['total_horas'];?></td>
			<td><?php echo $linea['obs'];?></td>
			<td><?php echo $linea['created'];?></td>
			<td><?php echo $linea['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'lineas', 'action' => 'view', $linea['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'lineas', 'action' => 'edit', $linea['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'lineas', 'action' => 'delete', $linea['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $linea['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Linea', true), array('controller' => 'lineas', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>

