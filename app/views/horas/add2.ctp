<div class="Horasform">
<?php echo $this->Form->create('Hora');?>
	<fieldset>
 		<legend><?php __('Horas Técnico-Mes'); ?></legend>

		<table border="0" cellspacing="5" cellpadding="5">
			<tr>
				<th>Usuario</th>
				<th>Familia</th>
				<th>Año</th>
				<th>Enero</th>
				<th>Febrero</th>
				<th>Marzo</th>
				<th>Abril</th>
				<th>Mayo</th>
				<th>Junio</th>
				<th>Julio</th>
				<th>Agosto</th>
				<th>Septiembre</th>
				<th>Octubre</th>
				<th>Noviembre</th>
				<th>Diciembre</th>				
			</tr>
			<?php 
			$cont = -1;
			foreach ($horas as $hora):
				$cont=$cont+1;
			 ?>
			<tr>
				<td>
					<input name="data[Hora][<?php echo $cont ?>][id]" type="hidden" value="<?php echo $hora['Hora']['id'] ?>" />
					<input name="data[Hora][<?php echo $cont ?>][usuario_id]" type="hidden" value="<?php echo $hora['Hora']['usuario_id'] ?>" />
					<input name="data[Hora][<?php echo $cont ?>][famlilia_id]" type="hidden" value="<?php echo $hora['Hora']['familia_id'] ?>" />
					
				<?php if ($cont % 2 == 0): ?>
					<?php echo $hora['Usuario']['name'] ?>
				<?php endif ?>
				</td>
				<td><?php echo $hora['Familia']['name'] ?></td>
				<td><?php echo $hora['Hora']['year'] ?></td>
				<td><input name="data[Hora][<?php echo $cont ?>][enero]" type="text" value="<?php echo $hora['Hora']['enero']; ?>" class="tdhoras" /></td>
				<td><input name="data[Hora][<?php echo $cont ?>][febrero]" type="text" value="<?php echo $hora['Hora']['febrero']; ?>" class="tdhoras" /></td>
				<td><input name="data[Hora][<?php echo $cont ?>][marzo]" type="text" value="<?php echo $hora['Hora']['marzo']; ?>" class="tdhoras" /></td>
				<td><input name="data[Hora][<?php echo $cont ?>][abril]" type="text" value="<?php echo $hora['Hora']['abril']; ?>" class="tdhoras" /></td>
				<td><input name="data[Hora][<?php echo $cont ?>][mayo]" type="text" value="<?php echo $hora['Hora']['mayo']; ?>" class="tdhoras" /></td>
				<td><input name="data[Hora][<?php echo $cont ?>][junio]" type="text" value="<?php echo $hora['Hora']['junio']; ?>" class="tdhoras" /></td>
				<td><input name="data[Hora][<?php echo $cont ?>][julio]" type="text" value="<?php echo $hora['Hora']['julio']; ?>" class="tdhoras" /></td>
				<td><input name="data[Hora][<?php echo $cont ?>][agosto]" type="text" value="<?php echo $hora['Hora']['agosto']; ?>" class="tdhoras" /></td>
				<td><input name="data[Hora][<?php echo $cont ?>][septiembre]" type="text" value="<?php echo $hora['Hora']['septiembre']; ?>" class="tdhoras" /></td>
				<td><input name="data[Hora][<?php echo $cont ?>][octubre]" type="text" value="<?php echo $hora['Hora']['octubre']; ?>" class="tdhoras" /></td>
				<td><input name="data[Hora][<?php echo $cont ?>][noviembre]" type="text" value="<?php echo $hora['Hora']['noviembre']; ?>" class="tdhoras" /></td>
				<td><input name="data[Hora][<?php echo $cont ?>][diciembre]" type="text" value="<?php echo $hora['Hora']['diciembre']; ?>" class="tdhoras" /></td>

			</tr>	
			<?php endforeach ?>
			
		</table>

	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
