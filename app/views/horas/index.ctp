<div class="Horas index">
	<h2><?php __('Horas');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('proyecto_id');?></th>
			<th><?php echo $this->Paginator->sort('hito_id');?></th>
			<th><?php echo $this->Paginator->sort('usuario_id');?></th>
			<th><?php echo $this->Paginator->sort('horas_prev');?></th>
			<th><?php echo $this->Paginator->sort('horas_total');?></th>
			<th><?php echo $this->Paginator->sort('obs');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($Horas as $Hora):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $Hora['Hora']['id']; ?>&nbsp;</td>
		<td><?php echo $Hora['Hora']['name']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($Hora['Proyecto']['name'], array('controller' => 'proyectos', 'action' => 'view', $Hora['Proyecto']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($Hora['Hito']['name'], array('controller' => 'hitos', 'action' => 'view', $Hora['Hito']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($Hora['Usuario']['name'], array('controller' => 'usuarios', 'action' => 'view', $Hora['Usuario']['id'])); ?>
		</td>
		<td><?php echo $Hora['Hora']['horas_prev']; ?>&nbsp;</td>
		<td><?php echo $Hora['Hora']['horas_total']; ?>&nbsp;</td>
		<td><?php echo $Hora['Hora']['obs']; ?>&nbsp;</td>
		<td><?php echo $Hora['Hora']['created']; ?>&nbsp;</td>
		<td><?php echo $Hora['Hora']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $Hora['Hora']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $Hora['Hora']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $Hora['Hora']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $Hora['Hora']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Acciones'); ?></h3>
        <?php if($this->Session->read('admin') == '0') :        ?>
        <ul>
		<li><?php echo $this->Html->link(__('Nueva Linea', true),               array('controller'=>'lineas', 'action' => 'addajax')); ?></li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Vista Proyecto-Hito', true),       array('controller'=>'lineas', 'action' => 'index2')); ?></li>
                <li><?php echo $this->Html->link(__('Vista Proyecto-Hito-Hora', true), array('controller'=>'lineas', 'action' => 'index3')); ?></li>
                <li><?php echo $this->Html->link(__('Vista Proyecto', true),            array('controller'=>'lineas', 'action' => 'indexajax')); ?></li>
	</ul>
        <?php else : ?>
	<ul>
                <li><?php echo $this->Html->link(__('Lista Horas', true),            array('controller' => 'Horas', 'action' => 'index')); ?> </li>
                <li><?php echo $this->Html->link(__('Nuevo Hora', true),             array('controller' => 'Horas', 'action' => 'add')); ?> </li>
                <li><?php echo $this->Html->link(__('Editar Hora', true),            array('controller' => 'Horas', 'action' => 'edit',$Hora['Hora']['id'])); ?> </li>
                <li><?php echo $this->Html->link(__('Eliminar Hora', true),          array('controller' => 'Horas', 'action' => 'delete', $Hora['Hora']['id']),null,  sprintf(__('Seguro que quieres eliminar la Hora # %s?', true), $Hora['Hora']['name'])); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Proyectos', true),           array('controller'=>'proyectos','action' => 'index')); ?> </li>
                <li><?php echo $this->Html->link(__('Nuevo Proyecto', true),            array('controller'=>'proyectos','action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Hitos', true),               array('controller' => 'hitos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nuevo Hito', true),                array('controller' => 'hitos', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Lineas', true),              array('controller' => 'lineas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Linea', true),               array('controller' => 'lineas', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Categorias', true),          array('controller' => 'categorias', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Categoria', true),           array('controller' => 'categorias', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Usuarios', true),          array('controller' => 'usuarios', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nuevo Usuario', true),           array('controller' => 'usuarios', 'action' => 'add')); ?> </li>
	</ul>
        <?php endif; ?>
</div>