<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php __('GITEK: Gestión de horas de: '.$session->read('Usuario')); ?>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		
                echo $javascript->link('jquery-1.4.4.min');
                echo $javascript->link('jquery-ui-1.8.6.custom.min');
                echo $javascript->link('fullcalendar.min');
                echo $scripts_for_layout;


                echo $this->Html->css('fullcalendar');
                echo $this->Html->css('cake.generic');
                echo $html->css('fullcalendar');


                echo $this->Html->css('gitek');
        ?>

</head>
<body>
	<div id="container">
		<div id="header">
			<h1><?php echo $this->Html->link(__('GITEK: Gestión de horas de: '.$session->read('Usuario').'  ', true), '#'); ?>
                        <?php echo $html->link('Salir',array('controller' => 'usuarios', 'action' => 'logout')); ?>
                        </h1>
		</div>
		<div id="content">

			<?php echo $this->Session->flash(); ?>

			<?php echo $content_for_layout; ?>

		</div>
		<div id="footer">

		</div>
	</div>
	<?php //echo $this->element('sql_dump'); ?>
</body>
</html>