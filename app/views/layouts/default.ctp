<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php //__('GITEK: Gestión de horas de: '.$session->read('Usuario')); ?>
		<?php __('App Horas v1.0.1'); ?>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('cake.generic');
                //echo $javascript->link('jquery-1.4.4.min');
                echo $javascript->link('jquery-1.4.4');

                echo $javascript->link('jquery-ui-1.8.8.custom.min');
                echo $this->Html->css('jquery-ui-1.8.8.custom.css');
		echo $scripts_for_layout;

                

                echo $this->Html->css('gitek');
        ?>

</head>
<body>
	<div id="container">
		<div id="header">
                    <div class="userbox">
                        <div class="inner">
                            <div class="avatarname">
                                <?php echo $this->Html->link(__('GITEK: Gestión de horas de: '.$session->read('Usuario').'  ', true), '#'); ?>
                            </div>
                                <ul class="usernav">
                                    <li>
                                         <?php echo $html->link('Salir',array('controller' => 'usuarios', 'action' => 'logout')); ?> &nbsp; &nbsp;                                         
                                    </li>
                                    <li>
                                       <?php echo $html->link('Relación',array(
                                                                'controller' => 'lineas',
                                                                'action' => 'relazioa',
                                                                '?' => array('q' => 'login')
                                                            ));
                                       ?>
                                    </li>
                                    <li><?php echo $html->link('Soy coordinador',array(
                                                                'controller'=>'lineas',
                                                                'action'    =>'addhoras',
                                                                '?'         => array('filtro'=>'coordino')
                                                            )); ?>
                                    </li>
                                    <li><?php echo $html->link('Tengo tareas',array(
                                                                'controller'=>'lineas',
                                                                'action'    =>'addhoras',
                                                                '?'         => array('filtro'=>'mistareas')
                                    )); ?></li>
                                    <?php if($this->Session->read('admin') == '1') : ?>
                                    <li>
                                        <?php

                                                echo $html->link( 'Administración', array(
                                                    'controller'=>'proyectos',
                                                    'action'=>'index'
                                                ));
                                        ?>
                                    </li>
									<li>
										<?php
											echo $html->link('Horas/Técnico', array(
													'controller' => 'horas',
													'action' => 'add2'
												));
										?>
									</li>
                                    <?php endif; ?>
                                </ul>
                            </div>
			</div>
		</div>
		<div id="content">

			<?php echo $this->Session->flash(); ?>

			<?php echo $content_for_layout; ?>

		</div>
		<div id="footer">

		</div>
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>