<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php //__('GITEK: Gestión de horas de: '.$session->read('Usuario')); ?>
		<?php __('App Horas v1.0.1'); ?>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('cake.generic');
                echo $javascript->link('jquery-1.4.2.min');
                echo $javascript->link('jquery-ui-1.8.4.custom.min');
                echo $this->Html->css('jquery-ui-1.8.4.custom.css');
		echo $scripts_for_layout;

                

                echo $this->Html->css('gitek');
        ?>

</head>
<body>
	<div id="container">
		<div id="header">

		</div>
		<div id="content">

			<?php echo $this->Session->flash(); ?>

			<?php echo $content_for_layout; ?>

		</div>
		<div id="footer">

		</div>
	</div>
</body>
</html>