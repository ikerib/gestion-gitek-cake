<?php echo $javascript->link('rowcolumn');?>
<?php echo $javascript->link('sumas');?>
<?php echo $javascript->link('milightbox');?>
<?php echo $javascript->link('date');?>
<?php //echo $javascript->link('jquery.DOMWindow');?>
<?php //echo $javascript->link('jquery-ui-1.8.7.custom.min');?>
<script>
    $(function() {

        $('.semanaanterior').click(function(){
            var f = $('#datepicker').val();
            var j = Date.parse(f,'yyyy-MM-dd').toString('yyyy-MM-dd');
            var d3 = new Date().clearTime();
            d3 = Date.parse(f,'yyyy-MM-dd').add({ days: -7 });
            $('#datepicker').val(d3.toString('yyyy-MM-dd'));
            $('#LineaAddhorasForm').submit();
        });

        $('.semanasiguiente').click(function(){
            var f = $('#datepicker').val();
            var j = Date.parse(f,'yyyy-MM-dd').toString('yyyy-MM-dd');
            var d3 = new Date().clearTime();
            d3 = Date.parse(f,'yyyy-MM-dd').add({ days: 7 });
            $('#datepicker').val(d3.toString('yyyy-MM-dd'));
            $('#LineaAddhorasForm').submit();
        });

        $('.simplehighlight').hover(function(){
            $(this).children().addClass('datahighlight');
	},function(){
            $(this).children().removeClass('datahighlight');
	});


    });
</script>


<?php //echo $form->create('Linea', array('url' => array('controller' => 'lineas', 'action' => 'guardarhoras'))); ?>
<div id="dialog-form" title="Observaciones">
	<form action="">
	<fieldset>
                <input type="hidden" name="rela" id="rela" value="" />
                <textarea name="name" cols="40" rows="9" id="obs"  class="text ui-widget-content ui-corner-all"></textarea>
	</fieldset>
	</form>
</div>

         <?php
                $request_params = Router::getParams();
                if ( !empty($request_params['pass']) ) { // si addhoras/3
                    echo $form->create('Linea', array(
                        'url' => array(
                            'controller' => 'lineas',
                            'action' => 'guardarhoras',$request_params['pass'][0]
                            )));
                } elseif (!empty ( $this->params['url']['filtro'] )) { // si addhoras?filtro=???
                     if ( $_GET['filtro']=='coordino' ) {
                        echo $form->create('Linea', array(
                            'url' => array(
                                'controller' => 'lineas',
                                'action' => 'guardarhoras','?'=>array('filtro'=>'coordino')
                                )));
                     } elseif ( $_GET['filtro']=='mistareas' ) {
                         echo $form->create('Linea', array(
                             'url' => array(
                                 'controller' => 'lineas',
                                 'action' => 'guardarhoras','?'=>array('filtro'=>'mistareas')
                                 )));
                     }
                } else {
                    echo $form->create('Linea', array(
                        'url' => array(
                            'controller' => 'lineas',
                            'action' => 'guardarhoras'
                            )));
                }
            ?>
            <div id="divFechaDesdeAddHoras">
                <ul class="ulfechasel">
                    <li><a href="#" class="semanaanterior">Semana anterior</a></li>
                    <li class="lifechasel">
                        <div class="fezkerra"><span class="spanfechasel">Fetxa:</span></div>
                        <input type="hidden" id="fetxagorde" value="" />
                        <div class="fezkerra">
                            <input class="textfechasel" id="datepicker"
                                   type="text" size="14"
                                   name="data[Linea][fechaseleccionada]"
                                   value=
                                   <?php 
                                   	if( (!empty ($this->data['Linea']['fechaseleccionada'])) 
                                   	&& ($this->data['Linea']['fechaseleccionada']!="") ) 
																		{
																			//echo "Aa";
																						// //hacemos que la semana comience esde el lunes
																						// 					$eguna = date("N",strtotime($this->data['Linea']['fechaseleccionada']));										
																						// 					$kenketa = 0;
																						// 
																						// 					// si es sabado 
																						// 					if ($eguna == 6 ) {
																						// 						$kenketa = 5;
																						// 					}
																						// 					//si es domingo 
																						// 					if ($eguna == 7) {
																						// 							$kenketa = 6;
																						// 					}
																						// 
																						// 
																						// 					if ( ($eguna != 1) && ( $kenketa == 0 ) ) {
																						// 
																						// 						//$kenketa = 7 - $eguna;
																						// 						switch ($eguna) {
																						// 							case 2: //asteartea
																						// 								$kenketa=1;
																						// 								break;
																						// 							case 3: //asteazkena
																						// 								$kenketa=2;
																						// 								break;
																						// 							case 4: //osteguna
																						// 								$kenketa=3;
																						// 								break;
																						// 							case 5: //ostirala
																						// 								$kenteta=4;
																						// 								break;
																						// 						}													
																						// 				}
																						// 
																						// 
																						// 				$azkenday = strtotime(date("Y-m-d",strtotime($this->data['Linea']['fechaseleccionada'])) . " -" . $kenketa . " day");
																						// 
																						// 				$i = $kenketa;
																						// 														  						//get timestamp for past/future date I want
																						// 														  						$pf_time = strtotime("-".$i." day");
																						// 														  						//format the date using the timestamp generated
																						// 														  						$pf_date = date("Y-m-d", $pf_time);
																						// 				echo $pf_date;
																			
																   	 	// miramos si la fecha es sabado o domingo y la modificamos
	
		                                  	$azkenfecha = date("Y-m-d",strtotime(date("Y-m-d",strtotime($this->data['Linea']['fechaseleccionada']))));
		                                    $azkenday = strtotime(date("Y-m-d", strtotime($azkenfecha)));
	
		                                    // comprobamos que no sea fin de semana
		                                   if ( date("N",$azkenday)== 6) {
		                                       $azkenday = strtotime(date("Y-m-d", strtotime($azkenfecha)) . " +2 day");
		                                   }
	
		                                   if ( date("N",$azkenday)== 7) {
		                                       $azkenday = strtotime(date("Y-m-d", strtotime($azkenfecha)) . " +1 day");
		                                   }
												
																			echo date("Y-m-d",$azkenday);
                         
                               			} 
																		else 
																		{
																			//hacemos que la semana comience esde el lunes
																			$eguna = date("N",time());										
																			$kenketa = 0;

																			// si es sabado 
																			if ($eguna == 6 ) {
																				$kenketa = 5;
																			}
																			//si es domingo 
																			if ($eguna == 7) {
																					$kenketa = 6;
																			}
												

																			if ( ($eguna != 1) && ( $kenketa == 0 ) ) {
													
																				//$kenketa = 7 - $eguna;
																				switch ($eguna) {
																					case 2: //asteartea
																						$kenketa=1;
																						break;
																					case 3: //asteazkena
																						$kenketa=2;
																						break;
																					case 4: //osteguna
																						$kenketa=3;
																						break;
																					case 5: //ostirala
																						$kenteta=4;
																						break;
																				}													
																		}
												

																		$azkenday = strtotime(date("Y-m-d") . " -" . $kenketa . " day");
	
																		$i = $kenketa;
											  						//get timestamp for past/future date I want
											  						$pf_time = strtotime("-".$i." day");
											  						//format the date using the timestamp generated
											  						$pf_date = date("Y-m-d", $pf_time);
																		echo $pf_date;
                           				}
                                  ?>
                        />
                        </div>
                        <div class="fezkerra"><input class="inputfechasel" type="submit" value="OK" /></div>
                        <?php echo $ajax->datepicker('datepicker',array(
                            'firstDay' => '1',
                            'dayNamesMin' => "['Do','Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa']",
                            'monthNames' => "['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'] "
                        )); ?>
                        <script type="text/javascript">
                            $(function() {
                                $("#datepicker").change(function() {
                                    $('#datepicker').datepicker(
                                        //'option', "dateFormat", 'yy-mm-dd' );
                                        'option', {dateFormat: 'yy-mm-dd'}
                                        );
                                });

                            });
                        </script>
                    </li>
                    <li><a href="#" class="semanasiguiente">Semana siguiente</a></li>
                </ul>
            </div>



<style type="text/css">
    body { font-size: 62.5%; }
    label, input { display:block; }
    input.text { margin-bottom:12px; width:95%; padding: .4em; }
    fieldset { padding:0; border:0; margin-top:25px; }
    h1 { font-size: 1.2em; margin: .6em 0; }
    div#users-contain { width: 350px; margin: 20px 0; }
    div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
    div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
</style>





<table id="sartuOrduak">
    <thead>
    <tr class="simplehighlight">
       <td class="tdproyecto"></td>
       <td class="tdtecnico">Técnico</td>
	   <td class="tdtecnico">Previsto</td>
	   <td class="tdtecnico">Real</td>
        <?php
        $iker=0;
        $kontatu=0;
        $azkenfecha=0;
        $fetxak= array();
        $ifetxak=0;
        
        if ( $kontatu == 0) :
            if ( empty ($this->data['Linea']['fechaseleccionada']) ) {
							$eguna = date("N",time());
				
							$kenketa = 0;
							// si es sabado 
							if ($eguna == 6 ) {
								$kenketa = 5;
							}
							//si es domingo 
							if ($eguna == 7) {
									$kenketa = 6;
							}
				

							if ( ($eguna != 1) && ( $kenketa == 0 ) ) {
					
							//$kenketa = 7 - $eguna;
							switch ($eguna) {
								case 2: //asteartea
									$kenketa=1;
									break;
								case 3: //asteazkena
									$kenketa=2;
									break;
								case 4: //osteguna
									$kenketa=3;
									break;
								case 5: //ostirala
									$kenteta=4;
									break;
							}			
				}
				//$newtimestamp = strtotime("-" . $kenketa . " days",$timestamp); 
				//$mynewdate = strftime("%Y-%m-%d",$newtimestamp); 
				$azkenday = strtotime(date("Y-m-d") . " -" . $kenketa . " day");

				$i = $kenketa+1;
			  //get timestamp for past/future date I want
			  	$pf_time = strtotime("-".$i." day");
			  //format the date using the timestamp generated
			  	$azkenfecha = date("Y-m-d", $pf_time);
				//echo $pf_date;
                				
            } else {
				// hacemos que siempre sea lunes
	
				$ikerfecha = date("Y-m-d",strtotime($this->data['Linea']['fechaseleccionada'])) ;
				$eguna = -1;
				$astekoeguneralol = strtotime(date("Y-m-d", strtotime($ikerfecha)));
				$eguna = date("N",$astekoeguneralol);
				$kenketa = 0;
				// si es sabado 
				if ($eguna == 6 ) {
					$kenketa = 5;
				}
				//si es domingo 
				if ($eguna == 7) {
						$kenketa = 6;
				}
				if ( ($eguna != 1) && ( $kenketa == 0 ) ) {
					//$kenketa = 7 - $eguna;
					switch ($eguna) {
						case 2: //asteartea
							$kenketa=1;
							break;
						case 3: //asteazkena
							$kenketa=2;
							break;
						case 4: //osteguna
							$kenketa=3;
							break;
						case 5: //ostirala
							$kenteta=4;
							break;
					}
				}
								
				//$newtimestamp = strtotime("-" . $kenketa . " days",$timestamp); 
				//$mynewdate = strftime("%Y-%m-%d",$newtimestamp); 
				$azkenday = strtotime(date($this->data['Linea']['fechaseleccionada']) . " -" . $kenketa . " day");

				$i = $kenketa+1;
			  //get timestamp for past/future date I want
			  	$pf_time = strtotime(date($this->data['Linea']['fechaseleccionada']) . "-".$i." day");
			  //format the date using the timestamp generated
			  	$azkenfecha = date("Y-m-d", $pf_time);
				//echo $pf_date;
				
                //$azkenfecha = date("Y-m-d",strtotime(date("Y-m-d",strtotime($this->data['Linea']['fechaseleccionada'])) . " -1 day"));
            }
            
        endif;
        $gei=0;
        for ($i=0; $i<5; $i++ ) {
            if (($i+$ifetxak)<5){ //para asegurarnos que solo coje 5 fetxas
                $gei = $gei+1;
                $azkenday = strtotime(date("Y-m-d", strtotime($azkenfecha)) . " +$gei day");
               
                // comprobamos que no sea fin de semana

                //DESACTIVADO

                if ( date("N",$azkenday)== 6) {
                    // si es sabado
                    $gei =$gei+2;
                    $azkenday = strtotime(date("Y-m-d", strtotime($azkenfecha)) . " +$gei day");
                }
                if ( date("N",$azkenday)==7) {
                    // // si es domingo
                    $gei =$gei+1;
                    $azkenday = strtotime(date("Y-m-d", strtotime($azkenfecha)) . " +$gei day");
                }
                $fetxak[$ifetxak+$i]=$azkenday;

                // Aqui se imprimen las fechas de la cabezera, de los INPUT
                //echo date("Y-m-d",$azkenday);
                echo '<td class="tdFecha">'.date("Y-m-d",$azkenday).'</td>';
                echo '<td class="tdnota"></td>';

            } //endif => if ($ifetxak<5){
        } //endfor => for ($i=0; $i<5; $i++ ) {
        ?>
    </tr>
    </thead>
    <tbody>
        <?php $ppp=0; ?>
        <?php $hhh=0; ?>
        <?php $ttt=0; ?>
    <?php foreach ($proyectos as $proyecto) { ?>
        <?php $ppp+=1; ?>
    <tr class="simplehighlight">
        <td class="tdProyecto"><?php echo "<strong>" . $proyecto['Proyecto']['name'] . "</strong>"; ?></td>
        <td>&nbsp;</td>
		<td class="tdtecnico"><?php echo $proyecto['Proyecto']['total_prev'] ?></td>
		<td class="tdtecnico"><?php echo $proyecto['Proyecto']['total_horas'] ?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
        <?php
        foreach ( $hitos as $hito ) { ?>
            <?php
                if ( $hito['Hito']['proyecto_id']==$proyecto['Proyecto']['id'] ) {
                    //xdebug_break();
                ?>
                <tr class="simplehighlight">
                    <td class="tdProyecto">--(h):<?php echo $hito['Hito']['name']; ?></td>                    
                    <td class="tdtecnico"><?php echo $hito['Usuario']['name']; ?></td>
					<td class="tdtecnico"><?php echo $hito['Hito']['horas_prev'] ?></td>
					 <?php
              // SEMAFORO
							if (($hito['Hito']['horas_prev'] == 0) || (empty($hito['Hito']['horas_prev'] ))){
								$desbioa = 0;
							} else {
								$desbioa = $hito['Hito']['horas_real'] * 100 / $hito['Hito']['horas_prev'];	
							}
							$semaforo="berde";
              if ( $desbioa < 50 ) 
							{
								$semaforo="berde";
              }
              if ( ($desbioa > 50) && ($desbioa < 70) ) {
              	$semaforo="amarillo";
              }
              if ( ($desbioa > 69) && ($desbioa < 100) ) 
							{
								$semaforo="naranja";
              }
              if ( ($desbioa > 99) || ($desbioa < 0) )
							{
              	$semaforo="gorria";
              }
              if (($desbioa < 50) && ($desbioa>-1) ) 
							{
              	$semaforo="berde";
          		}
					?>
					<td class="tdtecnico <?php echo  $semaforo ?>"><?php echo $hito['Hito']['horas_real'] ?></td>
                    <?php
                    for ( $q=0; $q < count($fetxak); $q++ ) {
                        $impr=0;
                        $zenbat=$q+1;
                        $aurkitua=0;
                        foreach(array_reverse($ultimos, true) as $key=>$ultimo) {
                            if ( ($ultimo['Hito']['id']==$hito['Hito']['id']) && ( $ultimo['Tarea']['id']==NULL )) {
                                if ( $fetxak[$q] == strtotime($ultimo['Linea']['fecha']) ) {
                                    $aurkitua+=1;
                                    //$zenbat=$zenbat+$aurkitua-1;
                                    //xdebug_break();
                                    ?>
                                    <td>
                                        <input name="data[Linea][<?php echo $iker;?>][id]" type="hidden" value=" <?php echo $ultimo['Linea']['id'] ?>" />
                                        <input name="data[Linea][<?php echo $iker;?>][proyecto_id]" type="hidden" value=" <?php echo $proyecto['Proyecto']['id'] ?>" />
                                        <input name="data[Linea][<?php echo $iker;?>][hito_id]" type="hidden" value=" <?php echo $hito['Hito']['id'] ?>" />
                                        <input name="data[Linea][<?php echo $iker;?>][tarea_id]" type="hidden" value="" />
                                        <input name="data[Linea][<?php echo $iker;?>][usuario_id]" type="hidden" value=" <?php echo $session->read('Usuario_id') ?>" />
                                        <input name="data[Linea][<?php echo $iker;?>][fecha]" type="hidden" value="<?php echo date("Y-m-d",$fetxak[$q]); ?> " />
                                        <input name="data[Linea][<?php echo $iker;?>][obs]" type="hidden" id="LineaTotalHorasObs<?php echo$iker; ?>"
                                               value="<?php echo $ultimo['Linea']['obs'];?>" />
                                        <input name="data[Linea][<?php echo $iker;?>][total_horas]" type="text" id="LineaTotalHoras<?php echo$iker; ?>"
                                               value="<?php echo $ultimo['Linea']['total_horas']; ?>"  <?php echo "class='orduak".$zenbat."'"; ?> />
                                    </td>
                                    <td class="tdnota">
                                               <?php
                                                if ( (!empty ( $ultimo['Linea']['obs'] )) && ( $ultimo['Linea']['obs']!="" ) ) {
                                                    $mirel = "LineaTotalHorasObs$iker";
                                                    echo $html->image("nota_rojo.png", array(
                                                                                        "alt" => "Tiene observaciones",
                                                                                        'url' => '#',
                                                                                        'class'=>'create-user',
                                                                                        'rel' => "$mirel" ));

                                                } else {
                                                    $mirel = "LineaTotalHorasObs$iker";
                                                    echo $html->image("nota_berde.gif", array(
                                                                                        "alt" => "Añador observaciones",
                                                                                        'url' => '#',
                                                                                        'class'=>'create-user',
                                                                                        'rel' => "$mirel" ));
                                                }
                                                ?>
                                    </td>
                                    <?php
                                        $zenbat+=1;
                                        $impr=1;
                                        $iker+=1;
                                    }
                                }
                            }
                        if ($aurkitua>1) {
                            $q=$q+$aurkitua-1;
                        }
                    if ($impr==0) { ?>
                    <td>
                        <input name="data[Linea][<?php echo $iker;?>][proyecto_id]" type="hidden" value=" <?php echo $proyecto['Proyecto']['id'] ?>" />
                        <input name="data[Linea][<?php echo $iker;?>][hito_id]" type="hidden" value=" <?php echo $hito['Hito']['id'] ?>" />
                        <input name="data[Linea][<?php echo $iker;?>][tarea_id]" type="hidden" value="" />
                        <input name="data[Linea][<?php echo $iker;?>][usuario_id]" type="hidden" value=" <?php echo $session->read('Usuario_id') ?>" />
                        <input name="data[Linea][<?php echo $iker;?>][fecha]" type="hidden" value="<?php echo date("Y-m-d",$fetxak[$q]); ?> " />
                        <input name="data[Linea][<?php echo $iker;?>][obs]" type="hidden"  id="LineaTotalHorasObs<?php echo$iker; ?>" value="" />
                        <input name="data[Linea][<?php echo $iker;?>][total_horas]" type="text" id="LineaTotalHoras<?php echo$iker; ?>"
                               value=""  <?php $cl=$q+1; echo "class='orduak".$cl."'"; ?> />
                    </td>
                    <td class="tdnota">
                               <?php
                                    $mirel = "LineaTotalHorasObs$iker";
                                    echo $html->image("nota_berde.gif", array(
                                                            "alt" => "Añadir observaciones",
                                                            'url' => '#',
                                                            'class'=>'create-user',
                                                            'rel' => "$mirel" ));
                                ?>
                    </td>
                    <?php $iker+=1;
                    }
                    }
                    ?>

                </tr>

                <?php foreach ($tareas as $tarea) {
                if ( ($tarea['Tarea']['hito_id']== $hito['Hito']['id']) && ( $tarea['Proyecto']['id'] == $proyecto['Proyecto']['id'] ))
                {
                    ?>
                    <tr class="simplehighlight">
                        <td class="tdProyecto">----(t):<?php echo $tarea['Tarea']['name']; ?></td>
                        <td class="tdtecnico"><?php echo $tarea['Usuario']['name']; ?></td>
						<td class="tdtecnico"><?php echo $tarea['Tarea']['horas_prev'] ?></td>
						<td class="tdtecnico"><?php echo $tarea['Tarea']['horas_total'] ?></td>
                        <?php                            
                            for ( $z=0; $z < count($fetxak); $z++ )
                            {
                                $impr=0;
                                foreach(array_reverse($ultimos, true) as $key=>$ultimo)
                                {
                                    if ( $ultimo['Hito']['id']==$hito['Hito']['id'] && ( $ultimo['Tarea']['id']==$tarea['Tarea']['id'] )) 
                                    {
                                        if ( $fetxak[$z] == strtotime($ultimo['Linea']['fecha']) ) 
                                        { ?>
                                            <td>
                                                <input name="data[Linea][<?php echo $iker;?>][id]" type="hidden" value=" <?php echo $ultimo['Linea']['id'] ?>" />
                                                <input name="data[Linea][<?php echo $iker;?>][proyecto_id]" type="hidden" value=" <?php echo $proyecto['Proyecto']['id'] ?>" />
                                                <input name="data[Linea][<?php echo $iker;?>][hito_id]" type="hidden" value=" <?php echo $hito['Hito']['id'] ?>" />
                                                <input name="data[Linea][<?php echo $iker;?>][tarea_id]" type="hidden" value=" <?php echo $tarea['Tarea']['id'] ?>" />
                                                <input name="data[Linea][<?php echo $iker;?>][usuario_id]" type="hidden" value=" <?php echo $session->read('Usuario_id') ?>" />
                                                <input name="data[Linea][<?php echo $iker;?>][fecha]" type="hidden" value="<?php echo date("Y-m-d",$fetxak[$z]); ?> " />
                                                <input name="data[Linea][<?php echo $iker;?>][obs]" type="hidden" id="LineaTotalHorasObs<?php echo$iker; ?>"
                                                       value="<?php echo $ultimo['Linea']['obs'];?>" />
                                                <input name="data[Linea][<?php echo $iker;?>][total_horas]" type="text" id="LineaTotalHoras<?php echo$iker; ?>"
                                                       value="<?php echo $ultimo['Linea']['total_horas'];?>" <?php $cl=$z+1; echo "class='orduak".$cl."'"; ?> />
                                            </td>
                                            <td class="tdnota">
                                                       <?php
                                                if ( (!empty ( $ultimo['Linea']['obs'] )) && ( $ultimo['Linea']['obs']!="" ) ) {
                                                    $mirel = "LineaTotalHorasObs$iker";
                                                    echo $html->image("nota_rojo.png", array(
                                                                                        "alt" => "Tiene observaciones",
                                                                                        'url' => '#',
                                                                                        'class'=>'create-user',
                                                                                        'rel' => "$mirel" ));

                                                } else {
                                                    $mirel = "LineaTotalHorasObs$iker";
                                                    echo $html->image("nota_berde.gif", array(
                                                                                        "alt" => "Tiene observaciones",
                                                                                        'url' => '#',
                                                                                        'class'=>'create-user',
                                                                                        'rel' => "$mirel" ));
                                                }
                                                ?></td>
                                            <?php
                                                $impr=1;
                                                $iker+=1;
                                            } //if ( $fetxak[$z] == strtotime($ultimo['Linea']['fecha']) )
                                        } //if ( $ultimo['Hito']['id']==$hito['Hito']['id'] && ( $ultimo['Tarea']['id']==$tarea['Tarea']['id'] ))
                                    } //foreach(array_reverse($ultimos, true) as $key=>$ultimo)

                                if ($impr==0) {

                                    ?>
                                <td>
                                    <input name="data[Linea][<?php echo $iker;?>][proyecto_id]" type="hidden" value=" <?php echo $proyecto['Proyecto']['id'] ?>" />
                                    <input name="data[Linea][<?php echo $iker;?>][hito_id]" type="hidden" value=" <?php echo $hito['Hito']['id'] ?>" />
                                    <input name="data[Linea][<?php echo $iker;?>][tarea_id]" type="hidden" value=" <?php echo $tarea['Tarea']['id'] ?>" />
                                    <input name="data[Linea][<?php echo $iker;?>][usuario_id]" type="hidden" value=" <?php echo $session->read('Usuario_id') ?>" />
                                    <input name="data[Linea][<?php echo $iker;?>][fecha]" type="hidden" value="<?php echo date("Y-m-d",$fetxak[$z]); ?> " />
                                    <input name="data[Linea][<?php echo $iker;?>][obs]" type="hidden"  id="LineaTotalHorasObs<?php echo$iker; ?>" value="" />
                                    <input name="data[Linea][<?php echo $iker;?>][total_horas]" type="text" id="LineaTotalHoras<?php echo$iker; ?>"
                                           value=""  <?php $cl=$z+1; echo "class='orduak".$cl."'"; ?> />
                                </td>
                                <td class="tdnota"><?php
//                                                if ( (!empty ( $ultimo['Linea']['obs'] )) && ( $ultimo['Linea']['obs']!="" ) ) {
//                                                    $mirel = "LineaTotalHorasObs$iker";
//                                                    echo $html->image("nota_rojo.png", array(
//                                                                                        "alt" => "Tiene observaciones",
//                                                                                        'url' => '#',
//                                                                                        'class'=>'create-user',
//                                                                                        'rel' => "$mirel" ));
//
//                                                } else {
                                                    $mirel = "LineaTotalHorasObs$iker";
                                                    echo $html->image("nota_berde.gif", array(
                                                                                        "alt" => "Tiene observaciones",
                                                                                        'url' => '#',
                                                                                        'class'=>'create-user',
                                                                                        'rel' => "$mirel" ));
//                                                }
                                ?></td>
                                <?php $iker+=1;
                                } // if ($impr==0)
                            } //for ( $z=0; $z < count($fetxak); $z++ ) :
                            ?>
                     </tr>
                <?php
                    } //if ( ($tarea['Tarea']['hito_id']== $hito['Hito']['id']) && ( $tarea['Proyecto']['id'] == $proyecto['Proyecto']['id'] ))
                } //foreach ($tareas as $tarea)
                  ?>
            <?php } //if ( $hito['Hito']['proyecto_id']==$proyecto['Proyecto']['id'] ) ?>
        <?php } //foreach ( $hitos as $hito ) ?>
    <?php } //foreach ($proyectos as $proyecto) ?>
    
    <tr class="simplehighlight">
        <td colspan="7">&nbsp;</td>
    </tr>
    <tr>
        <td>Totales:</td>
        <td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td>
            <input type="text" disabled="disabled" value="0" id="total-orduak1" />
         </td>
         <td></td>
        <td>
            <input type="text" disabled="disabled" value="0" id="total-orduak2" />
         </td>
         <td></td>
         <td>
            <input type="text" disabled="disabled" value="0" id="total-orduak3" />
         </td>
         <td></td>
         <td>
            <input type="text" disabled="disabled" value="0" id="total-orduak4" />
         </td>
         <td></td>
         <td>
            <input type="text" disabled="disabled" value="0" id="total-orduak5" />
         </td>
         <td></td>
    </tr>
    </tbody>
</table>

<?php
$options = array(
    'name' => 'Update',
    'label' => 'Grabar datos',
    'div' => array(
        'class' => 'glass-pill',
    )
);
echo $form->end($options);
?>

