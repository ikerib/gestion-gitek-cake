<script type="text/javascript">
$(document).ready(function(){

	var fin = $('#finalizado').attr('checked');
    //$('.divtr').click(function(){
    $('.trigger').click(function(){
		$(this).parent().parent().parent().children('.divtrhito').slideToggle();
        $(this).toggleClass('open');

		if ( fin==true ) {
			$(this).children('.hitofinalizado').slideToggle();
		 } else {
			$(this).children('.hitofinalizado').hide();
		// 	$( ".hitofinalizado" ).each(function( intIndex) {
		// 		$( this ).hide();
		// 	});
		}


        return false;
	});

	$(".trigger").effect("highlight", {}, 3000);


    //$('.divtrhito').click(function(){
    $('.trigger2').click(function(){
        $(this).parent().parent().parent().children('.divtrtarea').slideToggle();
        $(this).toggleClass('open');
		// $( ".tareafinalizado" ).each(function( intIndex) {
		// 	$(this).slideToggle()
		// })
		//$(this).children('.divtrtarea').toggle('slow');
                return false;
	});

   $('.expand').click(function(){
        $( ".divtrhito" ).each(
            function( intIndex ){
                $(this).slideToggle()
            }
        )
   });

	$('#finalizado').click(function() {

		$( ".finalizado" ).each(function( intIndex) {
			$(this).slideToggle()
		})

		//$( ".hitofinalizado" ).each(function( intIndex) {
		//	$(this).slideToggle()
		//})

		// $( ".tareafinalizado" ).each(function( intIndex) {
		// 	$(this).slideToggle()
		// })

	});

});
</script>
 <?php echo $form->create('Linea', array('action' => 'relazioa'));?>
    <div id="nav">
        <div id="divCategoria">
            <ul>
                <li class="titulua">Cat. </li>
                <li><?php echo $form->select('categoria_id',array($kat),null,array('id'=>'categorias','width'=>'120px'),array('Selecciona categoria')); ?></li>
            </ul>
        </div>
        <div id="divCoordinador">
            <ul>
                <li class="titulua">Coor. </li>
                <li><?php echo $form->select('usuario_id',array($coo),null,array('id'=>'usuarios'),array('Selecciona usuario')); ?></li>
            </ul>
        </div>
        <div id="divProyecto">
            <ul>
                <li class="titulua">Proyectos:</li>
                <li>
                <?php echo $form->select('proyecto_id',array($proy),null,array(
                                                                                'id'=>'proyectos',
                                                                                'class'=>'selproy',
                                                                                ),array('Selecciona proyecto')); ?>
                </li>
            </ul>
        </div>
        <div id="divTecnico">
            <ul>
                <li class="titulua">Tecnico: </li>
                <li><?php echo $form->select('tecnico_id',array($tek),null,array('id'=>'tecnicos'),array('Selecciona tecnico')); ?></li>
            </ul>
        </div>
        <div id="divFechaDesde">
            <ul>
                <li class="titulua">desde: </li>
                <li><input id="datepicker" type="text" size="40" name="data[Linea][desde]" style="width:70px" />
                        <?php echo $ajax->datepicker('datepicker',array(
                            'firstDay' => '1',
                            'dayNamesMin' => "['Do','Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa']",
                            'monthNames' => "['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'] "
                        )); ?>
                    <script type="text/javascript">
                        $(function() {
                            $("#datepicker").change(function() {
                                $('#datepicker').datepicker('option', 'dateFormat', 'yy/mm/dd' ); });

                        });
                    </script>
                    </li>
            </ul>
        </div>
        <div id="divFechaHasta">
            <ul>
                <li class="titulua">     hasta: </li>
                <li><input id="datepickerra" type="text" size="40" name="data[Linea][hasta]"  style="width:70px" />
                    <?php //echo $ajax->datepicker('datepickerra'); ?>
                    <?php echo $ajax->datepicker('datepickerra',array(
                            'firstDay' => '1',
                            'dayNamesMin' => "['Do','Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa']",
                            'monthNames' => "['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'] "
                        )); ?>
                    <script type="text/javascript">
                        $(function() {
                            $("#datepickerra").change(function() {
                                $('#datepickerra').datepicker('option', 'dateFormat', 'yy/mm/dd' ); });

                        });
                    </script>
               </li>
            </ul>
        </div>
<!--        <div id="divDetalle">
            <ul>
                <li class="titulua">Detalle: </li>
                <li>
                    <select id="tipo" name="data[Linea][tipo]">
                        <option value="resumido" selected>Resumido</option>
                        <option value="hitos">Hitos</option>
                        <option value="Tareas">Tareas</option>
                    </select>
                </li>
            </ul>
        </div>-->
        <div id="divSituacion">
            <ul>
                <li class="titulua">Situación: </li>
                <li>
                    <select id="situacion" name="data[Linea][situacion]">
                        <option value="-1">Todos</option>
                        <option value="50">50%</option>
                        <option value="70">70%</option>
                        <option value="100">100%</option>
                    </select>
                </li>
            </ul>
        </div>
		<div id="divFinalizado">
			<ul>
				<li class="titulua">Finalizados</li>
				<li>
					<input type="checkbox" value="0" name="finalizado" id="finalizado" />
				</li>
			</ul>
		</div>
		<div id="year">
			<select name="data[Linea][year]" id="year">
                <?php if ( $_POST['data']) { ?>
				    <option value="2013" <?php if ($_POST['data']['Linea']['year'] =="2013") { echo 'selected'; } ?>>2013</option>
                    <option value="2012" <?php if ($_POST['data']['Linea']['year'] =="2012") { echo 'selected'; } ?>>2012</option>
				    <option value="2011" <?php if ($_POST['data']['Linea']['year'] =="2011") { echo 'selected'; } ?>>2011</option>
                <?php } else { ?>
                    <option value="2013" >2013</option>
                    <option value="2012" >2012</option>
                    <option value="2011" >2011</option>
                <?php } ?>
			</select>


		</div>
        <div id="divBotoia">
            <input type="submit" value="Actualizar datos" />
        </div>
        <!-- <div id="divAgrupar">
            <ul>
                <li><a href="#" class="expand">Expandir/Colapsar</a></li>
            </ul>
        </div> -->
    </div>
    <?php// echo "Urtea:" . $_POST['data']['Linea']['year'] ?>

	<?php //var_dump($_POST['data']['Linea']); ?>

    <?php //print_r($_POST); ?>
<?php
    echo $form->end();
    // RELAZIOA
?>

    <div id="report">
        <div class="divtrtitulo">
            <ul>
                <li class="thmas">&nbsp;</li>
                <li class="th1">Cat.</li>
                <li class="th2">Coor.</li>
                <li class="th3">Proyecto</li>
                <li class="th5">Prev.</li>
                <!-- <li class="th4">Real</li> -->
				<li class="thtotal">Real</li>
                <li class="th6">Desv.</li>
                <li class="thmes">Ene</li>
                <li class="thmes">Feb</li>
                <li class="thmes">Mar</li>
                <li class="thmes">Abr</li>
                <li class="thmes">May</li>
                <li class="thmes">Jun</li>
                <li class="thmes">Jul</li>
                <li class="thmes">Ago</li>
                <li class="thmes">Sep</li>
                <li class="thmes">Oct</li>
                <li class="thmes">Nov</li>
                <li class="thmes">Dic</li>
				<li class="lifinalizado">F</li>
                <!--<li class="thtotal">Sel.</li>-->
				<li class="thfin">Finalizar</li>
            </ul>
        </div>
<div id="azpititulo">
	<p>PROYECTOS</p>
</div>
<?php
	$separau = ""; // proyektuak eta gestioa separatzeko
    foreach ($proiektuak as $p):
		 if ($separau=="") {
		 	$separau = $p['familia_id'];
		 }

		if ($separau != $p['familia_id']) {
			$separau = $p['familia_id'];
			?>
			<div class="divtr">
		      <ul>
		        <li class="thmas">&nbsp;</li>
		        <li class="th1">&nbsp;</li>
		        <li class="th2">&nbsp;</li>
		        <li class="th3">&nbsp;</li>
		        <li class="th5">&nbsp;</li>
		        <!-- <li class="th4">&nbsp;</li> -->
		        <li class="thtotal">Control</li>
                <li class="th6">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
				<li class="lifinalizado">&nbsp;</li>
		        <!--<li class="thtotal">&nbsp;</li>-->
		        <li class="thfin">&nbsp;</li>
		    </ul>
		    </div>
			<div id="azpititulo">
				<p>GESTIÓN</p>
			</div>
		<?php
		}
        ?>
        <?php

            //PREVISTO
            if ( $_POST ) {
                if ($_POST['data']['Linea']['year']!="2013" ) {
                    $benetakoa = $p['total_prev'];
                } else {
                    $benetakoa = $p['total_prev']-$p['orduk'];
                }
            } else {
                $benetakoa = $p['total_prev']-$p['orduk'];
            }

            // REAL
            $daramagu = $p['1']+$p['2']+$p['3']+$p['4']+$p['5']+$p['6']+$p['7']+$p['8']+$p['9']+$p['10']+$p['11']+$p['12'];
            $daramagu = round($daramagu,0);

            // DESVIO
            if ($benetakoa == 0) {
                $desbioa = "0";
            } else {
                $desbioa = round($daramagu * 100 / $benetakoa,0);
            }

            // SEMAFORO
            $semaforo="berde";
            if ( $desbioa<50 ) {
                $semaforo="berde";
            }
            if ( ($desbioa>50) && ($desbioa<70) ) {
              $semaforo="amarillo";
            }
            if ( ($desbioa>69) && ($desbioa<100) ) {
              $semaforo="naranja";
            }
            if ( ($desbioa>99) || ($desbioa<0) ){
              $semaforo="gorria";
            }
            if (($desbioa < 50) && ($desbioa>-1) ) {
              $semaforo="berde";
            }

			//finalizado
			$miclass ="divtr" . " " . $p['finalizado'];
			if ($p['finalizado'] =='1') {
				$miclass= "divtr finalizado";
			}
            ?>
        <div class="<?php echo $miclass ?>">

        <ul>
            <li class="thmas <?php echo $semaforo ?>">&nbsp;<a href="#" class="trigger">&nbsp;</a></li>
            <li class="th1 <?php echo $semaforo ?>"><?php echo mb_substr($p['categoria_nombre'], 0, 30) . "..."; ?>&nbsp;</li>
            <li class="th2 <?php echo $semaforo ?>"><?php echo $p['coordinador_nombre']; ?>&nbsp;</li>
            <li class="th3 <?php echo $semaforo ?>"><?php echo $html->link(mb_substr($p['proyecto_nombre'], 0, 30) . "...",array('controller' => 'lineas', 'action' => 'ver_ficha',$p['proyecto_id'])); ?>&nbsp;</li>
            <li class="th5 <?php echo $semaforo ?>"><?php echo round($benetakoa,0);?>&nbsp;</li>

            <!-- <li class="th4 <?php //echo $semaforo ?>"><?php //echo $p['total_horas'];?>&nbsp;</li> -->
			<li class="thtotal <?php echo $semaforo ?>">
               <?php echo $daramagu ?>
            &nbsp;</li>
            <li class="th6 <?php echo $semaforo ?>">
                <?php
                    echo $desbioa;
                ?>%&nbsp;</li>
            <li class="thmes <?php echo $semaforo ?>"><?php echo round($p['1'],0); ?>&nbsp;</li>
            <li class="thmes <?php echo $semaforo ?>"><?php echo round($p['2'],0); ?>&nbsp;</li>
            <li class="thmes <?php echo $semaforo ?>"><?php echo round($p['3'],0); ?>&nbsp;</li>
            <li class="thmes <?php echo $semaforo ?>"><?php echo round($p['4'],0); ?>&nbsp;</li>
            <li class="thmes <?php echo $semaforo ?>"><?php echo round($p['5'],0); ?>&nbsp;</li>
            <li class="thmes <?php echo $semaforo ?>"><?php echo round($p['6'],0); ?>&nbsp;</li>
            <li class="thmes <?php echo $semaforo ?>"><?php echo round($p['7'],0); ?>&nbsp;</li>
            <li class="thmes <?php echo $semaforo ?>"><?php echo round($p['8'],0); ?>&nbsp;</li>
            <li class="thmes <?php echo $semaforo ?>"><?php echo round($p['9'],0); ?>&nbsp;</li>
            <li class="thmes <?php echo $semaforo ?>"><?php echo round($p['10'],0); ?>&nbsp;</li>
            <li class="thmes <?php echo $semaforo ?>"><?php echo round($p['11'],0); ?>&nbsp;</li>
            <li class="thmes <?php echo $semaforo ?>"><?php echo round($p['12'],0); ?>&nbsp;</li>

			<li class="lifinalizado">
				<?php
					if ($p['finalizado']=='0') {
						echo "N";
					} else {
						echo "S";
					}
				?>
			</li>

            <!--<li class="thtotal <?php //echo $semaforo ?>">
                 <?php //echo $html->link('Selec.',array(
                      //                      'controller'=>'lineas',
                        //                    'action'    =>'addhoras',
                          //                  $p['proyecto_id'],null,null
                          //  ));
                ?>
            </li>-->
			<li class="thfin <?php echo $semaforo ?>">
		         <?php echo $html->link('Finalizar.',array(
                                            'controller'=>'proyectos',
                                            'action'    =>'finalizar',
                                            $p['proyecto_id']),
											null,
											sprintf(__('¿Seguro que quieras terminar el proyecto %s ?', true), $p['proyecto_nombre']
                            				));
                ?>
            </li>
        </ul>

            <?php foreach ( $hituak as $h ) :
        //xdebug_break();
            if ($h['proyecto_id']==$p['proyecto_id']) : ?>
            <?php

                // SEMAFORO
                $semaforo="berde";
                if ( $h['desv']<50 ) {
                    $semaforo="berde";
                }
                if ( ($h['desv']>50) && ($h['desv']<70) ) {
                    $semaforo="amarillo";
                }
                if ( ($h['desv'] > 69) && ($h['desv']<100) ) {
                    $semaforo="naranja";
                }
                if ( ($h['desv']>99) || ($h['desv']<0) ){
                    $semaforo="rojo";
                }
                if (($h['desv'] < 50) && ($h['desv']>-1) ) {
                    $semaforo="berde";
                }

				//finalizado
				$hitoclass="";
				if ($h['finalizado'] =='1') {
					$hitoclass= " hitofinalizado";
				}

            ?>
            <div class="divtrhito <?php echo $hitoclass //.$semaforo   ?>">
                <ul>
                    <li class="thmas <?php //echo $semaforo ?>">&nbsp;<a href="#" class="trigger2">&nbsp;</a></li>
                    <li class="th1 <?php //echo $semaforo ?>"><?php //echo $h['categoria_nombre']; ?>&nbsp;</li>
                    <li class="th2 <?php //echo $semaforo ?>"><?php echo $h['coordinador_nombre']; ?>&nbsp;</li>
                    <li class="th3 <?php echo $semaforo ?>">HITO:<?php echo mb_substr($h['hito_nombre'], 0, 30) . "...";?>&nbsp;</li>
                    <li class="th5 <?php //echo $semaforo ?>"><?php echo round($h['total_prev'],0);?>&nbsp;</li>
                    <!-- <li class="th4 <?php //echo $semaforo ?>"><?php //echo $h['total_horas'];?>&nbsp;</li> -->
					<li class="thtotal <?php //echo $semaforo ?>">
                        <?php
                            $bahori1 = $h['1']+$h['2']+$h['3']+$h['4']+$h['5']+$h['6']+$h['7']+$h['8']+$h['9']+$h['10']+$h['11']+$h['12'];
                            $bahori = round($bahori1,0);
							echo $bahori1;
                        ?>
                    &nbsp;</li>
                    <li class="th6 <?php //echo $semaforo ?>"><?php echo round($h['desv'], 0); ?>%&nbsp;</li>
                    <li class="thmes <?php //echo $semaforo ?>"><?php echo round($h['1'],0); ?>&nbsp;</li>
                    <li class="thmes <?php //echo $semaforo ?>"><?php echo round($h['2'],0); ?>&nbsp;</li>
                    <li class="thmes <?php //echo $semaforo ?>"><?php echo round($h['3'],0); ?>&nbsp;</li>
                    <li class="thmes <?php //echo $semaforo ?>"><?php echo round($h['4'],0); ?>&nbsp;</li>
                    <li class="thmes <?php //echo $semaforo ?>"><?php echo round($h['5'],0); ?>&nbsp;</li>
                    <li class="thmes <?php //echo $semaforo ?>"><?php echo round($h['6'],0); ?>&nbsp;</li>
                    <li class="thmes <?php //echo $semaforo ?>"><?php echo round($h['7'],0); ?>&nbsp;</li>
                    <li class="thmes <?php //echo $semaforo ?>"><?php echo round($h['8'],0); ?>&nbsp;</li>
                    <li class="thmes <?php //echo $semaforo ?>"><?php echo round($h['9'],0); ?>&nbsp;</li>
                    <li class="thmes <?php //echo $semaforo ?>"><?php echo round($h['10'],0); ?>&nbsp;</li>
                    <li class="thmes <?php //echo $semaforo ?>"><?php echo round($h['11'],0); ?>&nbsp;</li>
                    <li class="thmes <?php //echo $semaforo ?>"><?php echo round($h['12'],0); ?>&nbsp;</li>

					<li class="lifinalizado">
						<?php
							if ($h['finalizado']=='0') {
								echo "N";
							} else {
								echo "S";
							}
						?>
					</li>

                    <!--<li class="thtotal <?php //echo $semaforo ?>">
                        <?php //echo $html->link('Selec.',array(
                              //              'controller'=>'lineas',
                                //            'action'    =>'addhoras',
                                  //          $h['hito_id'],null
                            //));
                        ?>
                    </li>-->
					<li class="thfin <?php //echo $semaforo ?>">
				         <?php echo $html->link('Finalizar.',array(
		                                            'controller'=>'hitos',
		                                            'action'    =>'finalizar',
		                                            $h['hito_id']),
													null,
													sprintf(__('¿Seguro que quieras terminar el hito %s ?', true), $h['hito_nombre']
		                            				));
		                ?>
		            </li>
                </ul>

            <?php foreach ( $aTareak as $a ) :
                //xdebug_break();
                //if ($a['visible']!=0) : ?>
                    <?php if (($a['proyecto_id']==$h['proyecto_id']) && ($a['hito_id']==$h['hito_id'])) : ?>
                    <?php
                        // SEMAFORO
                        $semaforo="berde";
                        if ( $a['desv']<50 ) {
                            $semaforo="berde";
                        }
                        if ( ($a['desv']>50) && ($a['desv']<70) ) {
                            $semaforo="amarillo";
                        }
                        if ( ($a['desv']>69) && ($a['desv']<100) ) {
                            $semaforo="naranja";
                        }
                        if ( ($a['desv']>99) || ($a['desv']<0) ){
                            $semaforo="rojo";
                        }
                        if (($a['desv'] < 50) && ($a['desv']>-1) ) {
                            $semaforo="berde";
                        }
						//finalizado
						$tareaclass="";
						if ($a['finalizado'] =='1') {
							$tareaclass= " tareafinalizado";
						}
                    ?>
                    <div class="divtrtarea <?php echo $semaforo . $tareaclass ?>">
                        <ul>
                            <li class="thmas <?php //echo $semaforo ?>">&nbsp;</li>
                            <li class="th1 <?php //echo $semaforo ?>"><?php echo $a['categoria_nombre']; ?>&nbsp;</li>
                            <li class="th2 <?php //echo $semaforo ?>"><?php echo $a['coordinador_nombre']; ?>&nbsp;</li>
                            <li class="th3 <?php echo $semaforo ?>">TAR:<?php echo mb_substr($a['tarea_nombre'], 0, 30) . "...";?>&nbsp;</li>
                            <li class="th5 <?php //echo $semaforo ?>"><?php echo round($a['total_prev'],0);?>&nbsp;</li>
                            <!-- <li class="th4 <?php //echo $semaforo ?>"><?php //echo $a['total_horas'];?>&nbsp;</li> -->
							<li class="thtotal <?php //echo $semaforo ?>">
                                <?php
                                    $bahori3 = $a['1']+$a['2']+$a['3']+$a['4']+$a['5']+$a['6']+$a['7']+$a['8']+$a['9']+$a['10']+$a['11']+$a['12'];
                                    $bahori = round($bahori3,0);
									echo $bahori3;
                                ?>
                            &nbsp;</li>
                            <li class="th6 <?php //echo $semaforo ?>"><?php echo round($a['desv'], 0);?>%&nbsp;</li>
                            <li class="thmes <?php //echo $semaforo ?>"><?php echo round($a['1'],0); ?>&nbsp;</li>
                            <li class="thmes <?php //echo $semaforo ?>"><?php echo round($a['2'],0); ?>&nbsp;</li>
                            <li class="thmes <?php //echo $semaforo ?>"><?php echo round($a['3'],0); ?>&nbsp;</li>
                            <li class="thmes <?php //echo $semaforo ?>"><?php echo round($a['4'],0); ?>&nbsp;</li>
                            <li class="thmes <?php //echo $semaforo ?>"><?php echo round($a['5'],0); ?>&nbsp;</li>
                            <li class="thmes <?php //echo $semaforo ?>"><?php echo round($a['6'],0); ?>&nbsp;</li>
                            <li class="thmes <?php //echo $semaforo ?>"><?php echo round($a['7'],0); ?>&nbsp;</li>
                            <li class="thmes <?php //echo $semaforo ?>"><?php echo round($a['8'],0); ?>&nbsp;</li>
                            <li class="thmes <?php //echo $semaforo ?>"><?php echo round($a['9'],0); ?>&nbsp;</li>
                            <li class="thmes <?php //echo $semaforo ?>"><?php echo round($a['10'],0); ?>&nbsp;</li>
                            <li class="thmes <?php //echo $semaforo ?>"><?php echo round($a['11'],0); ?>&nbsp;</li>
                            <li class="thmes <?php //echo $semaforo ?>"><?php echo round($a['12'],0); ?>&nbsp;</li>

							<li class="lifinalizado">
								<?php
									if ($a['finalizado']=='0') {
										echo "N";
									} else {
										echo "S";
									}
								?>
							</li>

                            <!--<li class="thtotal <?php //echo $semaforo ?>">
                                 <?php //echo $html->link('Selec.',array(
                                 //                           'controller'=>'lineas',
                                   //                         'action'    =>'addhoras',
                                     //                       $a['proyecto_id'],$a['hito_id'],$a['tarea_id']
                                       //     ));
                                ?>
                            </li>-->
                            <li class="thtotal <?php //echo $semaforo ?>">
              			         <?php echo $html->link('Finalizar.',array(
				                                            'controller'=>'tareas',
				                                            'action'    =>'finalizar',
				                                           	$a['tarea_id']),
															null,
															sprintf(__('¿Seguro que quieras terminar la hito %s ?', true), $a['tarea_nombre']
				                            				));
				                ?>

                            </li>

                        </ul>
                    </div>
                    <?php //endif;?>
                    <?php endif;?>
                    <?php endforeach;?> <!-- Tareas -->

            </div>
            <?php endif;?>
            <?php endforeach;?> <!-- Hitos -->
        </div>

    <?php endforeach;?> <!-- Proyectos -->

    <div class="divtr">
      <ul>
		        <li class="thmas">&nbsp;</li>
		        <li class="th1">&nbsp;</li>
		        <li class="th2">&nbsp;</li>
		        <li class="th3">&nbsp;</li>
		        <li class="th5">&nbsp;</li>
		        <!-- <li class="th4">&nbsp;</li> -->
		        <li class="thtotal">&nbsp;</li>
                <li class="th6">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <li class="thmes">&nbsp;</li>
		        <!--<li class="thtotal">&nbsp;</li>-->
		        <li class="thfin">&nbsp;</li>
    </ul>
    </div>
<?php
$kontau=-1;
$nireclass="";
foreach ($larogei as $l):
	$kontau+=1;
	if ($kontau==0) {
		$nireclass="0";
	} else {
		$nireclass="1";
	}
?>
    <div class="divtr">
	      <ul>
	        <li class="thmas">&nbsp;</li>
	        <li class="th1">&nbsp;</li>
	        <li class="th2"><strong><?php echo $l['familia_name'] ?></strong></li>
	        <li class="th3">Previsto</li>
	        <li class="th5">&nbsp;</li>
	        <!-- <li class="th4">&nbsp;</li> -->
			<li class="thtotal">&nbsp;</li>
	        <li class="th6">&nbsp;</li>
	        <li class="thmes <?php echo "prev1-".$nireclass ?>"><?php echo $l['r_enero'] ?></li>
	        <li class="thmes <?php echo "prev2-".$nireclass ?>"><?php echo $l['r_febrero'] ?></li>
	        <li class="thmes <?php echo "prev3-".$nireclass ?>"><?php echo $l['r_marzo'] ?></li>
	        <li class="thmes <?php echo "prev4-".$nireclass ?>"><?php echo $l['r_abril'] ?></li>
	        <li class="thmes <?php echo "prev5-".$nireclass ?>"><?php echo $l['r_mayo'] ?></li>
	        <li class="thmes <?php echo "prev6-".$nireclass ?>"><?php echo $l['r_junio'] ?></li>
	        <li class="thmes <?php echo "prev7-".$nireclass ?>"><?php echo $l['r_julio'] ?></li>
	        <li class="thmes <?php echo "prev8-".$nireclass ?>"><?php echo $l['r_agosto'] ?></li>
	        <li class="thmes <?php echo "prev9-".$nireclass ?>"><?php echo $l['r_septiembre'] ?></li>
	        <li class="thmes <?php echo "prev10-".$nireclass ?>"><?php echo $l['r_octubre'] ?></li>
	        <li class="thmes <?php echo "prev11-".$nireclass ?>"><?php echo $l['r_noviembre'] ?></li>
	        <li class="thmes <?php echo "prev12-".$nireclass ?>"><?php echo $l['r_diciembre'] ?></li>
	        <!--<li class="thtotal">&nbsp;</li>-->
	        <li class="thfin">&nbsp;</li>
	    </ul>
    </div>

    <div class="divtr">
	      <ul>
	        <li class="thmas">&nbsp;</li>
	        <li class="th1">&nbsp;</li>
	        <li class="th2">&nbsp;</li>
	        <li class="th3">Real:</li>
	        <li class="th5">&nbsp;</li>
	        <!-- <li class="th4">&nbsp;</li> -->
			<li class="thtotal">&nbsp;</li>
	        <li class="th6">&nbsp;</li>
	        <!-- <li class="thmes <?php echo "real1-".$nireclass ?>"><?php //echo round($l['1'],0); ?></li> -->
					<li class="thmes <?php echo "real1-".$nireclass ?>"><?php echo (round($l['1'],0)==0) ? "&nbsp;": round($l['1'],0); ?></li>
	        <li class="thmes <?php echo "real2-".$nireclass ?>"><?php echo (round($l['2'],0)==0) ? "&nbsp;": round($l['2'],0); ?></li>
	        <li class="thmes <?php echo "real3-".$nireclass ?>"><?php echo (round($l['3'],0)==0) ? "&nbsp;": round($l['3'],0); ?></li>
	        <li class="thmes <?php echo "real4-".$nireclass ?>"><?php echo (round($l['4'],0)==0) ? "&nbsp;": round($l['4'],0); ?></li>
	        <li class="thmes <?php echo "real5-".$nireclass ?>"><?php echo (round($l['5'],0)==0) ? "&nbsp;": round($l['5'],0); ?></li>
	        <li class="thmes <?php echo "real6-".$nireclass ?>"><?php echo (round($l['6'],0)==0) ? "&nbsp;": round($l['6'],0); ?></li>
	        <li class="thmes <?php echo "real7-".$nireclass ?>"><?php echo (round($l['7'],0)==0) ? "&nbsp;": round($l['7'],0); ?></li>
	        <li class="thmes <?php echo "real8-".$nireclass ?>"><?php echo (round($l['8'],0)==0) ? "&nbsp;": round($l['8'],0); ?></li>
	        <li class="thmes <?php echo "real9-".$nireclass ?>"><?php echo (round($l['9'],0)==0) ? "&nbsp;": round($l['9'],0); ?></li>
	        <li class="thmes <?php echo "real10-".$nireclass ?>"><?php echo (round($l['10'],0)==0) ? "&nbsp;": round($l['10'],0); ?></li>
	        <li class="thmes <?php echo "real11-".$nireclass ?>"><?php echo (round($l['11'],0)==0) ? "&nbsp;": round($l['11'],0); ?></li>
	        <li class="thmes <?php echo "real12-".$nireclass ?>"><?php echo (round($l['12'],0)==0) ? "&nbsp;": round($l['12'],0); ?></li>
	        <!--<li class="thtotal">&nbsp;</li>-->
	        <li class="thfin">&nbsp;</li>
	    </ul>
    </div>


    <div class="divtr">
	      <ul>
	        <li class="thmas">&nbsp;</li>
	        <li class="th1">&nbsp;</li>
	        <li class="th2">&nbsp;</li>
	        <li class="th3">Desviación:</li>
	        <li class="th5">&nbsp;</li>
	        <!-- <li class="th4">&nbsp;</li> -->
			<li class="thtotal">&nbsp;</li>
	        <li class="th6">&nbsp;</li>
	        <!-- <li class="thmes <?php echo "desv1-".$nireclass ?>"><?php //echo round($l['r_enero']-$l['1'],0); ?></li> -->
	        <li class="thmes <?php echo "desv1-".$nireclass ?>"><?php echo (round($l['r_enero']-$l['1'],0)==0) ? "&nbsp;": round($l['r_enero']-$l['1'],0); ?></li>
	        <!-- <li class="thmes <?php echo "desv2-".$nireclass ?>"><?php //echo round($l['r_febrero']-$l['2'],0); ?></li> -->
					<li class="thmes <?php echo "desv1-".$nireclass ?>"><?php echo (round($l['r_febrero']-$l['2'],0)==0) ? "&nbsp;": round($l['r_febrero']-$l['2'],0); ?></li>
	        <!-- <li class="thmes <?php echo "desv3-".$nireclass ?>"><?php //echo round($l['r_marzo']-$l['3'],0); ?></li> -->
					<li class="thmes <?php echo "desv1-".$nireclass ?>"><?php echo (round($l['r_marzo']-$l['3'],0)==0) ? "&nbsp;": round($l['r_marzo']-$l['3'],0); ?></li>
	        <!-- <li class="thmes <?php echo "desv4-".$nireclass ?>"><?php //echo round($l['r_abril']-$l['4'],0); ?></li> -->
					<li class="thmes <?php echo "desv1-".$nireclass ?>"><?php echo (round($l['r_abril']-$l['4'],0)==0) ? "&nbsp;": round($l['r_abril']-$l['4'],0); ?></li>
	        <!-- <li class="thmes <?php echo "desv5-".$nireclass ?>"><?php //echo round($l['r_mayo']-$l['5'],0); ?></li> -->
					<li class="thmes <?php echo "desv1-".$nireclass ?>"><?php echo (round($l['r_mayo']-$l['5'],0)==0) ? "&nbsp;": round($l['r_mayo']-$l['5'],0); ?></li>
	        <!-- <li class="thmes <?php echo "desv6-".$nireclass ?>"><?php //echo round($l['r_junio']-$l['6'],0); ?></li> -->
					<li class="thmes <?php echo "desv1-".$nireclass ?>"><?php echo (round($l['r_junio']-$l['6'],0)==0) ? "&nbsp;": round($l['r_junio']-$l['6'],0); ?></li>
	        <!-- <li class="thmes <?php echo "desv7-".$nireclass ?>"><?php //echo round($l['r_julio']-$l['7'],0); ?></li> -->
					<li class="thmes <?php echo "desv1-".$nireclass ?>"><?php echo (round($l['r_julio']-$l['7'],0)==0) ? "&nbsp;": round($l['r_julio']-$l['7'],0); ?></li>
	        <!-- <li class="thmes <?php echo "desv8-".$nireclass ?>"><?php //echo round($l['r_agosto']-$l['8'],0); ?></li> -->
					<li class="thmes <?php echo "desv1-".$nireclass ?>"><?php echo (round($l['r_agosto']-$l['8'],0)==0) ? "&nbsp;": round($l['r_agosto']-$l['8'],0); ?></li>
	        <!-- <li class="thmes <?php echo "desv9-".$nireclass ?>"><?php //echo round($l['r_septiembre']-$l['9'],0); ?></li> -->
					<li class="thmes <?php echo "desv1-".$nireclass ?>"><?php echo (round($l['r_septiembre']-$l['9'],0)==0) ? "&nbsp;": round($l['r_septiembre']-$l['9'],0); ?></li>
	        <!-- <li class="thmes <?php echo "desv10-".$nireclass ?>"><?php //echo round($l['r_octubre']-$l['10'],0); ?></li> -->
					<li class="thmes <?php echo "desv1-".$nireclass ?>"><?php echo (round($l['r_octubre']-$l['10'],0)==0) ? "&nbsp;": round($l['r_octubre']-$l['10'],0); ?></li>
	        <!-- <li class="thmes <?php echo "desv11-".$nireclass ?>"><?php //echo round($l['r_noviembre']-$l['11'],0); ?></li> -->
					<li class="thmes <?php echo "desv1-".$nireclass ?>"><?php echo (round($l['r_noviembre']-$l['11'],0)==0) ? "&nbsp;": round($l['r_noviembre']-$l['11'],0); ?></li>
	        <!-- <li class="thmes <?php echo "desv12-".$nireclass ?>"><?php //echo round($l['r_diciembre']-$l['12'],0); ?></li> -->
					<li class="thmes <?php echo "desv1-".$nireclass ?>"><?php echo (round($l['r_diciembre']-$l['12'],0)==0) ? "&nbsp;": round($l['r_diciembre']-$l['12'],0); ?></li>
	        <!--<li class="thtotal">&nbsp;</li>-->
	        <li class="thfin">&nbsp;</li>
	    </ul>
    </div>

    <div class="divtr">
	      <ul>
	        <li class="thmas">&nbsp;</li>
	        <li class="th1">&nbsp;</li>
	        <li class="th2">&nbsp;</li>
	        <li class="th3">Porcentaje %:</li>
	        <li class="th5">&nbsp;</li>
	        <!-- <li class="th4">&nbsp;</li> -->
			<li class="thtotal">&nbsp;</li>
	        <li class="th6">&nbsp;</li>
	        <li class="thmes <?php echo "porcen1-".$nireclass ?>">&nbsp;
				<?php
					// if ($l['r_enero']!=0) {
					// 	echo round(($l['r_enero']-$l['1']) * 100 / $l['r_enero'], 0) ."%";
					// }
				?>
			</li>
	        <li class="thmes <?php echo "porcen2-".$nireclass ?>">&nbsp;
				<?php
					// if ($l['r_febrero']!=0) {
					// 	echo round(($l['r_febrero']-$l['1']) * 100 / $l['r_febrero'], 0) ."%";
					// }
				?>
			</li>
	        <li class="thmes <?php echo "porcen3-".$nireclass ?>">&nbsp;
				<?php
					// if ($l['r_marzo']!=0) {
					// 	echo round(($l['r_marzo']-$l['1']) * 100 / $l['r_marzo'], 0) ."%";
					// }
				?>
			</li>
	        <li class="thmes <?php echo "porcen4-".$nireclass ?>">&nbsp;
				<?php
					// if ($l['r_abril']!=0) {
					// 	echo round(($l['r_abril']-$l['1']) * 100 / $l['r_abril'], 0) ."%";
					// }
				?>
			</li>
	        <li class="thmes <?php echo "porcen5-".$nireclass ?>">&nbsp;
				<?php
					// if ($l['r_mayo']!=0) {
					// 	echo round(($l['r_mayo']-$l['1']) * 100 / $l['r_mayo'], 0) ."%";
					// }
				?>
			</li>
	        <li class="thmes <?php echo "porcen6-".$nireclass ?>">&nbsp;
				<?php
					// if ($l['r_junio']!=0) {
					// 	echo round(($l['r_junio']-$l['1']) * 100 / $l['r_junio'], 0) ."%";
					// }
				?>
	</li>
	        <li class="thmes <?php echo "porcen7-".$nireclass ?>">&nbsp;
				<?php
					// if ($l['r_julio']!=0) {
					// 	echo round(($l['r_julio']-$l['1']) * 100 / $l['r_julio'], 0) ."%";
					// }
				?>
	</li>
	        <li class="thmes <?php echo "porcen8-".$nireclass ?>">&nbsp;
				<?php
					// if ($l['r_agosto']!=0) {
					// 	echo round(($l['r_agosto']-$l['1']) * 100 / $l['r_agosto'], 0) ."%";
					// }
				?>
	</li>
	        <li class="thmes <?php echo "porcen9-".$nireclass ?>">&nbsp;
				<?php
					// if ($l['r_septiembre']!=0) {
					// 	echo round(($l['r_septiembre']-$l['1']) * 100 / $l['r_septiembre'], 0) ."%";
					// }
				?>
	</li>
	        <li class="thmes <?php echo "porcenq10-".$nireclass ?>">&nbsp;
				<?php
					// if ($l['r_octubre']!=0) {
					// 	echo round(($l['r_octubre']-$l['1']) * 100 / $l['r_octubre'], 0) ."%";
					// }
				?>
	</li>
	        <li class="thmes <?php echo "porcen11-".$nireclass ?>">&nbsp;
				<?php
					// if ($l['r_noviembre']!=0) {
					// 	echo round(($l['r_noviembre']-$l['1']) * 100 / $l['r_noviembre'], 0) ."%";
					// }
				?>
	</li>
	        <li class="thmes <?php echo "porcen12-".$nireclass ?>">&nbsp;
				<?php
					// if ($l['r_diciembre']!=0) {
					// 	echo round(($l['r_diciembre']-$l['1']) * 100 / $l['r_diciembre'], 0) ."%";
					// }
				?>
	</li>
	        <!--<li class="thtotal">&nbsp;</li>-->
	        <li class="thfin">&nbsp;</li>
	    </ul>
    </div>

    <div class="divtr">
      <ul>
        <li class="thmas">&nbsp;</li>
        <li class="th1">&nbsp;</li>
        <li class="th2">&nbsp;</li>
        <li class="th3">&nbsp;</li>
        <li class="th5">&nbsp;</li>
        <!-- <li class="th4">&nbsp;</li> -->
		<li class="thtotal">&nbsp;</li>
        <li class="th6">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <!--<li class="thtotal">&nbsp;</li>-->
        <li class="thfin">&nbsp;</li>
    </ul>
    </div>

<?php endforeach ?>

    <div class="divtr">
      <ul>
        <li class="thmas">&nbsp;</li>
        <li class="th1">&nbsp;</li>
        <li class="th2">&nbsp;</li>
        <li class="th3">&nbsp;</li>
        <li class="th5">&nbsp;</li>
        <!-- <li class="th4">&nbsp;</li> -->
		<li class="thtotal">&nbsp;</li>
        <li class="th6">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <!--<li class="thtotal">&nbsp;</li>-->
        <li class="thfin">&nbsp;</li>
    </ul>
    </div>

    <div class="divtr">
      <ul>
        <li class="thmas">&nbsp;</li>
        <li class="th1">&nbsp;</li>
        <li class="th2">&nbsp;</li>
        <li class="th3">Horas previstas por mes:</li>
        <li class="th5">&nbsp;</li>
        <!-- <li class="th4">&nbsp;</li> -->
		<li class="thtotal">&nbsp;</li>
        <li class="th6">&nbsp;</li>
        <li class="thmes pmes1">&nbsp;</li>
        <li class="thmes pmes2">&nbsp;</li>
        <li class="thmes pmes3">&nbsp;</li>
        <li class="thmes pmes4">&nbsp;</li>
        <li class="thmes pmes5">&nbsp;</li>
        <li class="thmes pmes6">&nbsp;</li>
        <li class="thmes pmes7">&nbsp;</li>
        <li class="thmes pmes8">&nbsp;</li>
        <li class="thmes pmes9">&nbsp;</li>
        <li class="thmes pmes10">&nbsp;</li>
        <li class="thmes pmes11">&nbsp;</li>
        <li class="thmes pmes12">&nbsp;</li>
        <!--<li class="thtotal">&nbsp;</li>-->
        <li class="thfin">&nbsp;</li>
      </ul>
    </div>



    <div class="divtr">
        <ul>
            <li class="thmas">&nbsp;</li>
            <li class="th1">&nbsp;</li>
            <li class="th2">&nbsp;</li>
            <li class="th3">Horas reales por mes:</li>
            <li class="th5">&nbsp;</li>
            <li class="th4">&nbsp;</li>
						<!-- <li class="thtotal">&nbsp;</li> -->
						<li class="th6">&nbsp;</li>
							<?php
							$kontau=0;
							foreach($orduak as $o) {
								$kontau=$kontau+1;
							?>

						  	<li class="thmes rmes<?php echo $kontau; ?>"><?php echo round($o['0']['Horas'],0); ?>&nbsp;</li>

						  <?php } ?>
						<li class="thtotal">&nbsp;</li>
						<li class="thfin">&nbsp;</li>
        </ul>
    </div>

    <div class="divtr">
      <ul>
        <li class="thmas">&nbsp;</li>
        <li class="th1">&nbsp;</li>
        <li class="th2">&nbsp;</li>
        <li class="th3">Desviación:</li>
        <li class="th5">&nbsp;</li>
        <!-- <li class="th4">&nbsp;</li> -->
		<li class="thtotal">&nbsp;</li>
        <li class="th6">&nbsp;</li>
        <li class="thmes dmes1">&nbsp;</li>
        <li class="thmes dmes2">&nbsp;</li>
        <li class="thmes dmes3">&nbsp;</li>
        <li class="thmes dmes4">&nbsp;</li>
        <li class="thmes dmes5">&nbsp;</li>
        <li class="thmes dmes6">&nbsp;</li>
        <li class="thmes dmes7">&nbsp;</li>
        <li class="thmes dmes8">&nbsp;</li>
        <li class="thmes dmes9">&nbsp;</li>
        <li class="thmes dmes10">&nbsp;</li>
        <li class="thmes dmes11">&nbsp;</li>
        <li class="thmes dmes12">&nbsp;</li>
        <!--<li class="thtotal">&nbsp;</li>-->
        <li class="thfin">&nbsp;</li>
      </ul>
    </div>

        <div class="divtr">
      <ul>
        <li class="thmas">&nbsp;</li>
        <li class="th1">&nbsp;</li>
        <li class="th2">&nbsp;</li>
        <li class="th3">Porcentaje:</li>
        <li class="th5">&nbsp;</li>
        <!-- <li class="th4">&nbsp;</li> -->
		<li class="thtotal">&nbsp;</li>
        <li class="th6">&nbsp;</li>
        <li class="thmes porcen1">&nbsp;</li>
        <li class="thmes porcen2">&nbsp;</li>
        <li class="thmes porcen3">&nbsp;</li>
        <li class="thmes porcen4">&nbsp;</li>
        <li class="thmes porcen5">&nbsp;</li>
        <li class="thmes porcen6">&nbsp;</li>
        <li class="thmes porcen7">&nbsp;</li>
        <li class="thmes porcen8">&nbsp;</li>
        <li class="thmes porcen9">&nbsp;</li>
        <li class="thmes porcen10">&nbsp;</li>
        <li class="thmes porcen11">&nbsp;</li>
        <li class="thmes porcen12">&nbsp;</li>
        <!--<li class="thtotal">&nbsp;</li>-->
        <li class="thfin">&nbsp;</li>
      </ul>
    </div>

    <div class="divtr">
      <ul>
        <li class="thmas">&nbsp;</li>
        <li class="th1">&nbsp;</li>
        <li class="th2">&nbsp;</li>
        <li class="th3">&nbsp;</li>
        <li class="th5">&nbsp;</li>
        <!-- <li class="th4">&nbsp;</li> -->
		<li class="thtotal">&nbsp;</li>
        <li class="th6">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <li class="thmes">&nbsp;</li>
        <!--<li class="thtotal">&nbsp;</li>-->
        <li class="thfin">&nbsp;</li>
      </ul>
    </div>

    <?php
        foreach($kategoriaorduak as $k) {
    ?>

    <div class="divtr">
      <ul>
        <li class="thmas">&nbsp;</li>
        <li class="th1">&nbsp;</li>
        <li class="th2">Categoria:</li>
        <li class="th3"><?php echo $k['categoria_nombre'];  ?></li>
        <li class="th5">&nbsp;</li>
        <!-- <li class="th4">&nbsp;</li> -->
		<li class="thtotal">&nbsp;</li>
        <li class="th6">&nbsp;</li>
				<li class="thmes"><?php echo (round($k['1'],0)==0) ? "&nbsp;": round($k['1'],0); ?>
        <li class="thmes"><?php echo (round($k['2'],0)==0) ? "&nbsp;": round($k['2'],0); ?></li>
        <li class="thmes"><?php echo (round($k['3'],0)==0) ? "&nbsp;": round($k['3'],0); ?></li>
        <li class="thmes"><?php echo (round($k['4'],0)==0) ? "&nbsp;": round($k['4'],0); ?></li>
        <li class="thmes"><?php echo (round($k['5'],0)==0) ? "&nbsp;": round($k['5'],0); ?></li>
        <li class="thmes"><?php echo (round($k['6'],0)==0) ? "&nbsp;": round($k['6'],0); ?></li>
        <li class="thmes"><?php echo (round($k['7'],0)==0) ? "&nbsp;": round($k['7'],0); ?></li>
        <li class="thmes"><?php echo (round($k['8'],0)==0) ? "&nbsp;": round($k['8'],0); ?></li>
        <li class="thmes"><?php echo (round($k['9'],0)==0) ? "&nbsp;": round($k['9'],0); ?></li>
        <li class="thmes"><?php echo (round($k['10'],0)==0) ? "&nbsp;": round($k['10'],0); ?></li>
        <li class="thmes"><?php echo (round($k['11'],0)==0) ? "&nbsp;": round($k['11'],0); ?></li>
        <li class="thmes"><?php echo (round($k['11'],0)==0) ? "&nbsp;": round($k['12'],0); ?></li>
        <!--<li class="thtotal">&nbsp;</li>-->
        <li class="thfin">&nbsp;</li>
    </ul>
    </div>

    <?php
        }
    ?>

<h1><?php echo $html->link('Exportar datos Proyectos',array('action'=>'aexcel')) ?></h1>
<h1><?php echo $html->link('Exportar datos Hitos',array('action'=>'aexcelhitos')) ?></h1>
<h1><?php echo $html->link('Exportar datos Tareas',array('action'=>'aexceltareas')) ?></h1>
<h1><?php echo $html->link('Hora formato JOSEBE',array('action'=>'aexceljosebe')) ?></h1>
<h1><?php echo $html->link('Horas ALECOP',array('action'=>'aexcelalecop')) ?></h1>

 </div>

<?php echo $javascript->link('sumarelazioa');?>