<div class="proyectos  ">
    <h2><?php  __($proyecto["Proyecto"]["name"]);?></h2>

	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Num Oferta'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $proyecto['Proyecto']['num_oferta']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Coordinador'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $proyecto['Usuario']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Codigo'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $proyecto['Proyecto']['codigo']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Total Horas'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $proyecto['Proyecto']['total_horas']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Total Prev'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $proyecto['Proyecto']['total_prev']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Categoria'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $proyecto['Categoria']['name']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<br/>
<div class="related">
	<h3><?php __('Lineas relacionadas');?></h3>
	<?php if (!empty($proyecto['Linea'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Usuario'); ?></th>
		<th><?php __('Fecha'); ?></th>
		<th><?php __('Total Horas'); ?></th>
                <th><?php __('Observaciones'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($lineas as $linea):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $linea['Usuario']['name']; ?></td>
			<td><?php echo $linea['Linea']['fecha'];?></td>
			<td><?php echo $linea[0]['suma'];?></td>
                        <td><?php echo $linea['Linea']['obs'];?></td>
		</tr>
	<?php endforeach; ?>

	</table>
<?php endif; ?>


</div>
