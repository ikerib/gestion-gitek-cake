<div class="lineas view">
<h2><?php  __('Linea');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $linea['Linea']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Proyecto'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($linea['Proyecto']['name'], array('controller' => 'proyectos', 'action' => 'view', $linea['Proyecto']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Hito'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($linea['Hito']['name'], array('controller' => 'hitos', 'action' => 'view', $linea['Hito']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Tarea'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($linea['Tarea']['name'], array('controller' => 'tareas', 'action' => 'view', $linea['Tarea']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Usuario'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($linea['Usuario']['name'], array('controller' => 'usuarios', 'action' => 'view', $linea['Usuario']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Fecha'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $linea['Linea']['fecha']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Total Horas'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $linea['Linea']['total_horas']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Obs'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $linea['Linea']['obs']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $linea['Linea']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $linea['Linea']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Acciones'); ?></h3>
        <?php if($this->Session->read('admin') == '0') :        ?>
        <ul>
		<li><?php echo $this->Html->link(__('Nueva Linea', true),               array('controller'=>'lineas', 'action' => 'addajax')); ?></li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Vista Proyecto-Hito', true),       array('controller'=>'lineas', 'action' => 'index2')); ?></li>
                <li><?php echo $this->Html->link(__('Vista Proyecto-Hito-Tarea', true), array('controller'=>'lineas', 'action' => 'index3')); ?></li>
                <li><?php echo $this->Html->link(__('Vista Proyecto', true),            array('controller'=>'lineas', 'action' => 'indexajax')); ?></li>
	</ul>
        <?php else : ?>
	<ul>
                <li><?php echo $this->Html->link(__('Lista Lineas', true),              array('controller'=>'lineas','action' => 'index')); ?> </li>
                <li><?php echo $this->Html->link(__('Nueva Linea', true),               array('controller'=>'lineas','action' => 'add')); ?> </li>
                <li><?php echo $this->Html->link(__('Editar Linea', true),              array('controller'=>'lineas','action' => 'edit', $linea['Linea']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Eliminar Linea', true),            array('controller'=>'lineas','action' => 'delete', $linea['Linea']['id']), null, sprintf(__('Seguro que deseas eliminar la Linea de la fecha # %s?', true), $linea['Linea']['fecha'])); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Proyectos', true),           array('controller' => 'proyectos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Proyecto', true),            array('controller' => 'proyectos', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Hitos', true),               array('controller' => 'hitos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nuevo Hito', true),                array('controller' => 'hitos', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Tareas', true),              array('controller' => 'tareas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Tarea', true),               array('controller' => 'tareas', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Categorias', true),          array('controller' => 'categorias', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Categoria', true),           array('controller' => 'categorias', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Usuarios', true),            array('controller' => 'usuarios', 'action' => 'index')); ?> </li>
                <li><?php echo $this->Html->link(__('Nuevo Usuario', true),             array('controller' => 'usuarios', 'action' => 'add')); ?> </li>
	</ul>
        <?php endif; ?>
</div>
