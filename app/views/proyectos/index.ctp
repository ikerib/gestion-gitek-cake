<div class="proyectos index">
	<h2><?php __('Proyectos');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('num_oferta');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>			
			<th><?php echo $this->Paginator->sort('usuario_id');?></th>
			<th><?php echo $this->Paginator->sort('codigo');?></th>
			<th><?php echo $this->Paginator->sort('total_horas');?></th>
			<th><?php echo $this->Paginator->sort('total_prev');?></th>
			<th><?php echo $this->Paginator->sort('categoria_id');?></th>
			<th><?php echo $this->Paginator->sort('familia_id'); ?></th>
			<!-- <th><?php //echo $this->Paginator->sort('obs');?></th> -->
			<th><?php echo $this->Paginator->sort('finalizado') ?></th>
			<!-- <th><?php //echo $this->Paginator->sort('created');?></th>
			<th><?php //echo $this->Paginator->sort('modified');?></th> -->
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($proyectos as $proyecto):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $proyecto['Proyecto']['num_oferta']; ?>&nbsp;</td>
		<td><?php echo $proyecto['Proyecto']['name']; ?>&nbsp;</td>		
		<td>
			<?php echo $this->Html->link($proyecto['Usuario']['name'], array('controller' => 'usuarios', 'action' => 'view', $proyecto['Usuario']['id'])); ?>
		</td>
		<td><?php echo $proyecto['Proyecto']['codigo']; ?>&nbsp;</td>
		<td><?php echo $proyecto['Proyecto']['total_horas']; ?>&nbsp;</td>
		<td><?php echo $proyecto['Proyecto']['total_prev']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($proyecto['Categoria']['name'], array('controller' => 'categorias', 'action' => 'view', $proyecto['Categoria']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($proyecto['Familia']['name'], array('controller' => 'familias', 'action' => 'view', $proyecto['Familia']['id'])); ?>
		</td>		
		<!-- <td><?php //echo $proyecto['Proyecto']['obs']; ?>&nbsp;</td> -->
		<td><?php echo $proyecto['Proyecto']['finalizado']; ?></td>	
		<!-- <td><?php //echo $proyecto['Proyecto']['created']; ?>&nbsp;</td>
		<td><?php //echo $proyecto['Proyecto']['modified']; ?>&nbsp;</td> -->
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $proyecto['Proyecto']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $proyecto['Proyecto']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $proyecto['Proyecto']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $proyecto['Proyecto']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Acciones'); ?></h3>
        <?php if($this->Session->read('admin') == '0') :        ?>
        <ul>
		<li><?php echo $this->Html->link(__('Nueva Linea', true),               array('controller'=>'lineas', 'action' => 'addajax')); ?></li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Vista Proyecto-Hito', true),       array('controller'=>'lineas', 'action' => 'index2')); ?></li>
                <li><?php echo $this->Html->link(__('Vista Proyecto-Hito-Tarea', true), array('controller'=>'lineas', 'action' => 'index3')); ?></li>
                <li><?php echo $this->Html->link(__('Vista Proyecto', true),            array('controller'=>'lineas', 'action' => 'indexajax')); ?></li>
	</ul>
        <?php else : ?>
	<ul>
                <li><?php echo $this->Html->link(__('Lista Proyectos', true), array('controller'=>'proyectos','action' => 'index')); ?> </li>
                <li><?php echo $this->Html->link(__('Nuevo Proyecto', true), array('controller'=>'proyectos','action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Hitos', true), array('controller' => 'hitos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nuevo Hito', true), array('controller' => 'hitos', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Tareas', true), array('controller' => 'tareas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Tarea', true), array('controller' => 'tareas', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Lineas', true), array('controller' => 'lineas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Linea', true), array('controller' => 'lineas', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Categorias', true), array('controller' => 'categorias', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Categoria', true), array('controller' => 'categorias', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Usuarios', true), array('controller' => 'usuarios', 'action' => 'index')); ?> </li>
                <li><?php echo $this->Html->link(__('Nuevo Usuario', true), array('controller' => 'usuarios', 'action' => 'add')); ?> </li>
	</ul>
        <?php endif; ?>
</div>