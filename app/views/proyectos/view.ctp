      <?php
                        //calculamos el semaforo
                        // si 50% de las horas = amarillo
                        // si 70% de las horas = naranja
                        // si 100% de las horas = rojo
                        $semaforo ="ezer";
                        $r=0;
                        if ( !empty($proyecto['Proyecto']['total_prev']) && (!empty ($proyecto['Proyecto']['total_horas'])) && ($proyecto['Proyecto']['total_prev']!=0) && ($proyecto['Proyecto']['total_horas']!=0)) {

                            $r = $proyecto['Proyecto']['total_horas'] * 100 / $proyecto['Proyecto']['total_prev'];

                            if ( ($r > 50) && ($r<70) ) {
                                $semaforo = 'oria';
                            } elseif ( ($r > 70) && ($r < 100)) {
                                $semaforo = 'naranja';
                            } elseif ( $r > 100 ) {
                                $semaforo = 'gorria';
                            } else {
                                $semaforo = 'berdea';
                            }
                        }
                    ?>
<div class="proyectos view ">
    <h2 <?php echo 'class="'.$semaforo.'"';?>><?php  __('Proyecto');?></h2>

	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $proyecto['Proyecto']['id']; ?>
			&nbsp;
		</dd>
          
		<dt<?php if ($i % 2 == 0) echo $class.' '.$semaforo;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class.' '.$semaforo;?>>
         
			<?php echo ('<span class='.$semaforo.'>'.$proyecto["Proyecto"]["name"].'</span>'); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Num Oferta'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $proyecto['Proyecto']['num_oferta']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Coordinador'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($proyecto['Usuario']['name'], array('controller' => 'usuarios', 'action' => 'view', $proyecto['Usuario']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Codigo'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $proyecto['Proyecto']['codigo']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Total Horas'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $proyecto['Proyecto']['total_horas']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Total Prev'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $proyecto['Proyecto']['total_prev']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Categoria'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($proyecto['Categoria']['name'], array('controller' => 'categorias', 'action' => 'view', $proyecto['Categoria']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Obs'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $proyecto['Proyecto']['obs']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $proyecto['Proyecto']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $proyecto['Proyecto']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Acciones'); ?></h3>
        <?php if($this->Session->read('admin') == '0') :        ?>
        <ul>
		<li><?php echo $this->Html->link(__('Nueva Linea', true),               array('controller'=>'lineas', 'action' => 'addajax')); ?></li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Vista Proyecto-Hito', true),       array('controller'=>'lineas', 'action' => 'index2')); ?></li>
                <li><?php echo $this->Html->link(__('Vista Proyecto-Hito-Tarea', true), array('controller'=>'lineas', 'action' => 'index3')); ?></li>
                <li><?php echo $this->Html->link(__('Vista Proyecto', true),            array('controller'=>'lineas', 'action' => 'indexajax')); ?></li>
	</ul>
        <?php else : ?>
	<ul>
                <li><?php echo $this->Html->link(__('Lista Proyectos', true), array('controller'=>'proyectos','action' => 'index')); ?> </li>
                <li><?php echo $this->Html->link(__('Nuevo Proyecto', true), array('controller'=>'proyectos','action' => 'add')); ?> </li>
                <li><?php echo $this->Html->link(__('Editar Proyecto', true), array('controller'=>'proyectos','action' => 'edit', $proyecto['Proyecto']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Eliminar Proyecto', true), array('controller'=>'proyectos','action' => 'delete', $proyecto['Proyecto']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $proyecto['Proyecto']['id'])); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Hitos', true), array('controller' => 'hitos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nuevo Hito', true), array('controller' => 'hitos', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Tareas', true), array('controller' => 'tareas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Tarea', true), array('controller' => 'tareas', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Lineas', true), array('controller' => 'lineas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Linea', true), array('controller' => 'lineas', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Categorias', true), array('controller' => 'categorias', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Categoria', true), array('controller' => 'categorias', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Usuarios', true), array('controller' => 'usuarios', 'action' => 'index')); ?> </li>
                <li><?php echo $this->Html->link(__('Nuevo Usuario', true), array('controller' => 'usuarios', 'action' => 'add')); ?> </li>
	</ul>
        <?php endif; ?>
</div>

<div class="related" id="divHitos">
	<h3><?php __('Hitos relacionados');?></h3>
	<?php //if (!empty($proyecto['Hito'])):?>
	<?php if (!empty($hitos)):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th>Id</th>
		<th><?php __('Name'); ?></th>
                <th><?php __('Coordinador'); ?></th>
		<th><?php __('Horas Prev'); ?></th>
		<th><?php __('Horas Real'); ?></th>
		<th><?php __('Obs'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
                <?php if($this->Session->read('admin') == '1') :        ?>
                    <th class="actions"><?php __('Actions');?></th>
                <?php endif; ?>
	</tr>
	<?php
		$i = 0;
		foreach ($hitos as $hito):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
                <?php
                    //calculamos el semaforo
                    // si 50% de las horas = amarillo
                    // si 70% de las horas = naranja
                    // si 100% de las horas = rojo
                    $semaforo ="ezer";
                    $r=0;
                    if ( !empty($hito['horas_prev']) && (!empty ($hito['horas_real'])) && ($hito['horas_prev']!=0) && ($hito['horas_real']!=0)) {

                        $r = $hito['horas_real'] * 100 / $hito['horas_prev'];

                        if ( ($r > 50) && ($r<70) ) {
                            $semaforo = 'oria';
                        } elseif ( ($r > 70) && ($r < 100)) {
                            $semaforo = 'naranja';
                        } elseif ( $r > 100 ) {
                            $semaforo = 'gorria';
                        } else {
                            $semaforo = 'berdea';
                        }
                    }
                ?>
		<tr<?php echo $class;?>>
			<td <?php echo "class=$semaforo"; ?>><?php echo $hito['Hito']['id'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $hito['Hito']['name'];?></td>
                        <td <?php echo "class=$semaforo"; ?>><?php echo $hito['Usuario']['name'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $hito['Hito']['horas_prev'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $hito['Hito']['horas_real'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $hito['Hito']['obs'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $hito['Hito']['created'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $hito['Hito']['updated'];?></td>
                        <?php if($this->Session->read('admin') == '1') :        ?>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'hitos', 'action' => 'view', $hito['Hito']['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'hitos', 'action' => 'edit', $hito['Hito']['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'hitos', 'action' => 'delete', $hito['Hito']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $hito['Hito']['id'])); ?>
			</td>
                        <?php endif; ?>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

        <?php if($this->Session->read('admin') == '1') :        ?>
	<div class="actions">
		<ul>
                    <li><a href="#" id="newHito">Agregar Hito</a> </li>
                    <script type="text/javascript" >
               
                        $("#newHito").click(function () {
                            $("#addHito").slideToggle("slow");
                        });
                    </script>
		</ul>
            <div id="addHito" style="display: none">
                <?php
                echo $this->Form->create('Hito',array('controller'=>'hitos', 'action'=>'addajax'));
                echo $this->Form->input('name');
                echo $this->Form->input('usuario_id');
				echo $this->Form->input('horas_prev');
				echo $this->Form->input('horas_real');
				echo $this->Form->input('obs');
				echo '<input name="data[Hito][proyecto_id]" value="'.$proyecto['Proyecto']['id'].'" id="UserId" type="hidden">';
                //echo $ajax->submit('Agregar hito', array('url'=> array('controller'=>'hitos', 'action'=>'addajax'), 'update' => 'divHitos'));
                echo $form->end('Agregar Hito');
                ?>
            </div>
	</div>
        <?php endif; ?>
</div>
<div class="related">
	<h3><?php __('Tareas relacionadas');?></h3>
	<?php if (!empty($proyecto['Tarea'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Name'); ?></th>
		<th><?php __('Hito Id'); ?></th>
		<th><?php __('Horas Prev'); ?></th>
		<th><?php __('Horas Total'); ?></th>
		<th><?php __('Obs'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
                <?php if($this->Session->read('admin') == '1') :        ?>
                    <th class="actions"><?php __('Actions');?></th>
                    <?php endif; ?>
	</tr>
	<?php
		$i = 0;
		foreach ($proyecto['Tarea'] as $tarea):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
                <?php
                    //calculamos el semaforo
                    // si 50% de las horas = amarillo
                    // si 70% de las horas = naranja
                    // si 100% de las horas = rojo
                    $semaforo ="ezer";
                    $r=0;
                    if ( !empty($tarea['horas_prev']) && (!empty ($tarea['horas_total'])) && ($tarea['horas_prev']!=0) && ($tarea['horas_total']!=0)) {

                        $r = $tarea['horas_total'] * 100 / $tarea['horas_prev'];

                        if ( ($r > 50) && ($r<70) ) {
                            $semaforo = 'oria';
                        } elseif ( ($r > 70) && ($r < 100)) {
                            $semaforo = 'naranja';
                        } elseif ( $r > 100 ) {
                            $semaforo = 'gorria';
                        } else {
                            $semaforo = 'berdea';
                        }
                    }
                ?>
		<tr<?php echo $class;?>>
			<td <?php echo "class=$semaforo"; ?>><?php echo $tarea['name'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $tarea['Hito']['name'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $tarea['horas_prev'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $tarea['horas_total'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $tarea['obs'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $tarea['created'];?></td>
			<td <?php echo "class=$semaforo"; ?>><?php echo $tarea['modified'];?></td>
                        <?php if($this->Session->read('admin') == '1') :        ?>
			<td class="actions">
				<?php echo $this->Html->link(__('Ver', true), array('controller' => 'tareas', 'action' => 'view', $tarea['id'])); ?>
				<?php echo $this->Html->link(__('Editar', true), array('controller' => 'tareas', 'action' => 'edit', $tarea['id'])); ?>
				<?php echo $this->Html->link(__('Eliminar', true), array('controller' => 'tareas', 'action' => 'delete', $tarea['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $tarea['id'])); ?>
			</td>
                        <?php endif; ?>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

        <?php if($this->Session->read('admin') == '1') :        ?>
	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('Nueva Tarea', true), array('controller' => 'tareas', 'action' => 'add'));?> </li>
		</ul>
	</div>
        <?php endif; ?>
</div>

<div class="related">
	<h3><?php __('Lineas relacionadas');?></h3>
	<?php if (!empty($proyecto['Linea'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Hito Id'); ?></th>
		<th><?php __('Tarea Id'); ?></th>
		<th><?php __('Usuario Id'); ?></th>
		<th><?php __('Fecha'); ?></th>
		<th><?php __('Total Horas'); ?></th>
		<th><?php __('Obs'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($proyecto['Linea'] as $linea):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php if (!empty($linea['Hito']))  echo $linea['Hito']['name']; ?></td>
			<td><?php if (!empty($linea['Tarea'])) echo $linea['Tarea']['name']; ?></td>
			<td><?php if (!empty($linea['Usuario'])) echo $linea['Usuario']['name']; ?></td>
			<td><?php echo $linea['fecha'];?></td>
			<td><?php echo $linea['total_horas'];?></td>
			<td><?php echo $linea['obs'];?></td>
			<td><?php echo $linea['created'];?></td>
			<td><?php echo $linea['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('Ver', true), array('controller' => 'lineas', 'action' => 'view', $linea['id'])); ?>
				<?php echo $this->Html->link(__('Editar', true), array('controller' => 'lineas', 'action' => 'edit', $linea['id'])); ?>
				<?php echo $this->Html->link(__('Eliminar', true), array('controller' => 'lineas', 'action' => 'delete', $linea['id']), null, sprintf(__('¿Eliminar linea con fecha # %s?', true), $linea['fecha'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
             
	</table>
<?php endif; ?>
   
	<div class="actions" id="azkena">
		<ul>
			<li><?php echo $this->Html->link(__('Nueva Linea', true), array('controller' => 'lineas', 'action' => 'addajax'));?> </li>
		</ul>
	</div>
</div>
