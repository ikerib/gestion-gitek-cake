<div class="tareas index">
	<h2><?php __('Tareas');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<!-- <th><?php //echo $this->Paginator->sort('id');?></th> -->
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('proyecto_id');?></th>
			<th><?php echo $this->Paginator->sort('hito_id');?></th>
			<th><?php echo $this->Paginator->sort('usuario_id');?></th>
			<th><?php echo $this->Paginator->sort('horas_prev');?></th>
			<th><?php echo $this->Paginator->sort('horas_total');?></th>
			<!-- <th><?php //echo $this->Paginator->sort('obs');?></th> -->
			<!-- <th><?php //echo $this->Paginator->sort('created');?></th>
			<th><?php //echo $this->Paginator->sort('modified');?></th> -->
			<th><?php echo $this->Paginator->sort('finalizado'); ?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($tareas as $tarea):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<!-- <td><?php //echo $tarea['Tarea']['id']; ?>&nbsp;</td> -->
		<td><?php echo $tarea['Tarea']['name']; ?>&nbsp;</td>
		<td>
			<?php 
			echo $this->Html->link(mb_substr($tarea['Proyecto']['name'], 0, 30) . "...", array('controller' => 'proyectos', 'action' => 'view', $tarea['Proyecto']['id'])); 
			?>
		</td>
		<td>
			<?php echo $this->Html->link($tarea['Hito']['name'], array('controller' => 'hitos', 'action' => 'view', $tarea['Hito']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($tarea['Usuario']['name'], array('controller' => 'usuarios', 'action' => 'view', $tarea['Usuario']['id'])); ?>
		</td>
		<td><?php echo $tarea['Tarea']['horas_prev']; ?>&nbsp;</td>
		<td><?php echo $tarea['Tarea']['horas_total']; ?>&nbsp;</td>
		<!-- <td><?php //echo $tarea['Tarea']['obs']; ?>&nbsp;</td> -->
		<!-- <td><?php //echo $tarea['Tarea']['created']; ?>&nbsp;</td> -->
		<!-- <td><?php //echo $tarea['Tarea']['modified']; ?>&nbsp;</td> -->
		<td><?php echo $tarea['Tarea']['finalizado']; ?></td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $tarea['Tarea']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $tarea['Tarea']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $tarea['Tarea']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $tarea['Tarea']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Acciones'); ?></h3>
        <?php if($this->Session->read('admin') == '0') :        ?>
        <ul>
		<li><?php echo $this->Html->link(__('Nueva Linea', true),               array('controller'=>'lineas', 'action' => 'addajax')); ?></li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Vista Proyecto-Hito', true),       array('controller'=>'lineas', 'action' => 'index2')); ?></li>
                <li><?php echo $this->Html->link(__('Vista Proyecto-Hito-Tarea', true), array('controller'=>'lineas', 'action' => 'index3')); ?></li>
                <li><?php echo $this->Html->link(__('Vista Proyecto', true),            array('controller'=>'lineas', 'action' => 'indexajax')); ?></li>
	</ul>
        <?php else : ?>
	<ul>
                <li><?php echo $this->Html->link(__('Lista Tareas', true),            array('controller' => 'tareas', 'action' => 'index')); ?> </li>
                <li><?php echo $this->Html->link(__('Nuevo Tarea', true),             array('controller' => 'tareas', 'action' => 'add')); ?> </li>
                <li><?php echo $this->Html->link(__('Editar Tarea', true),            array('controller' => 'tareas', 'action' => 'edit',$tarea['Tarea']['id'])); ?> </li>
                <li><?php echo $this->Html->link(__('Eliminar Tarea', true),          array('controller' => 'tareas', 'action' => 'delete', $tarea['Tarea']['id']),null,  sprintf(__('Seguro que quieres eliminar la Tarea # %s?', true), $tarea['Tarea']['name'])); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Proyectos', true),           array('controller'=>'proyectos','action' => 'index')); ?> </li>
                <li><?php echo $this->Html->link(__('Nuevo Proyecto', true),            array('controller'=>'proyectos','action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Hitos', true),               array('controller' => 'hitos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nuevo Hito', true),                array('controller' => 'hitos', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Lineas', true),              array('controller' => 'lineas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Linea', true),               array('controller' => 'lineas', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Categorias', true),          array('controller' => 'categorias', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Categoria', true),           array('controller' => 'categorias', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Usuarios', true),          array('controller' => 'usuarios', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nuevo Usuario', true),           array('controller' => 'usuarios', 'action' => 'add')); ?> </li>
	</ul>
        <?php endif; ?>
</div>