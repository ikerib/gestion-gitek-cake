<div class="tareas view">
<h2><?php  __('Tarea');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $tarea['Tarea']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $tarea['Tarea']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Proyecto'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($tarea['Proyecto']['name'], array('controller' => 'proyectos', 'action' => 'view', $tarea['Proyecto']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Hito'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($tarea['Hito']['name'], array('controller' => 'hitos', 'action' => 'view', $tarea['Hito']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Horas Prev'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $tarea['Tarea']['horas_prev']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Horas Total'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $tarea['Tarea']['horas_total']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Obs'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $tarea['Tarea']['obs']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $tarea['Tarea']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $tarea['Tarea']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Acciones'); ?></h3>
        <?php if($this->Session->read('admin') == '0') :        ?>
        <ul>
		<li><?php echo $this->Html->link(__('Nueva Linea', true),               array('controller'=>'lineas', 'action' => 'addajax')); ?></li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Vista Proyecto-Hito', true),       array('controller'=>'lineas', 'action' => 'index2')); ?></li>
                <li><?php echo $this->Html->link(__('Vista Proyecto-Hito-Tarea', true), array('controller'=>'lineas', 'action' => 'index3')); ?></li>
                <li><?php echo $this->Html->link(__('Vista Proyecto', true),            array('controller'=>'lineas', 'action' => 'indexajax')); ?></li>
	</ul>
        <?php else : ?>
	<ul>
                <li><?php echo $this->Html->link(__('Lista Tareas', true),            array('controller' => 'tareas', 'action' => 'index')); ?> </li>
                <li><?php echo $this->Html->link(__('Nuevo Tarea', true),             array('controller' => 'tareas', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Proyectos', true),           array('controller'=>'proyectos','action' => 'index')); ?> </li>
                <li><?php echo $this->Html->link(__('Nuevo Proyecto', true),            array('controller'=>'proyectos','action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Hitos', true),               array('controller' => 'hitos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nuevo Hito', true),                array('controller' => 'hitos', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
		<li><?php echo $this->Html->link(__('Lista Lineas', true),              array('controller' => 'lineas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Linea', true),               array('controller' => 'lineas', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Categorias', true),          array('controller' => 'categorias', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva Categoria', true),           array('controller' => 'categorias', 'action' => 'add')); ?> </li>
                <li>&nbsp;</li>
                <li><?php echo $this->Html->link(__('Lista Usuarios', true),          array('controller' => 'usuarios', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nuevo Usuario', true),           array('controller' => 'usuarios', 'action' => 'add')); ?> </li>
	</ul>
        <?php endif; ?>
</div>
<div class="related">
	<h3><?php __('Related Lineas');?></h3>
	<?php if (!empty($tarea['Linea'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
            <th><?php __('Tarea'); ?></th>
            <th><?php __('Fecha'); ?></th>
            <th><?php __('Total Horas'); ?></th>
            <th><?php __('Obs'); ?></th>
            <th><?php __('Created'); ?></th>
            <th><?php __('Modified'); ?></th>
            <th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($tarea['Linea'] as $linea):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $linea['Tarea']['name'];?></td>
			<td><?php echo $linea['fecha'];?></td>
			<td><?php echo $linea['total_horas'];?></td>
			<td><?php echo $linea['obs'];?></td>
			<td><?php echo $linea['created'];?></td>
			<td><?php echo $linea['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'lineas', 'action' => 'view', $linea['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'lineas', 'action' => 'edit', $linea['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'lineas', 'action' => 'delete', $linea['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $linea['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Linea', true), array('controller' => 'lineas', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
