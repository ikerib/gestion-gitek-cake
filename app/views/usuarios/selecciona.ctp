<div class="usuarioSeleccion">
<!--	<h2><?php // __('Seleccion de usuarios');?></h2>-->

	<?php echo $form->create('Usuario', array('action' => 'login')); ?>
 
        <fieldset>
 		<legend><?php __('Seleccione Usuario'); ?></legend>

                <select id="usuarios" name="data[Usuario][id]" size="10">

                <?php foreach ($usuarios as $usuario): ?>
                        <option value="<?php echo $usuario['Usuario']['id']; ?>"><?php echo $usuario['Usuario']['name'];  ?></option>
                    <?php endforeach; ?>

                </select>
            
	</fieldset>
    
        <?php echo $this->Form->end(__('ENTRAR', true));?>

</div>
	