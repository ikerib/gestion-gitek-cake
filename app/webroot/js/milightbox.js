
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$( "#dialog:ui-dialog" ).dialog( "destroy" );

		var name = $( "#name" ),
			email = $( "#email" ),
			password = $( "#password" ),
			allFields = $( [] ).add( name ).add( email ).add( password ),
			tips = $( ".validateTips" );

		function updateTips( t ) {
			tips
				.text( t )
				.addClass( "ui-state-highlight" );
			setTimeout(function() {
				tips.removeClass( "ui-state-highlight", 1500 );
			}, 500 );
		}

		function checkLength( o, n, min, max ) {
			if ( o.text().length > max || o.text().length < min ) {
				o.addClass( "ui-state-error" );
				updateTips( "El número de carácteres de " + n + " tiene que ser entre " +
					min + " y " + max + "." );
				return false;
			} else {
				return true;
			}
		}



		$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 300,
			width: 350,
			modal: true,
			buttons: {
				"Guardar y cerrar": function() {
					var bValid = true;
					allFields.removeClass( "ui-state-error" );

					bValid = bValid && checkLength( name, "username", 0, 200 );

					if ( bValid ) {
                                                var dest = $("#rela").val();
                                                $("#"+dest).val($("#obs").val());
                                                $("#obs").val("");
						$( this ).dialog( "close" );
                                                //var ik = $(this).attr("rel");
                                                //$('#'+ik).attr('src', "/gestion/img/nota_rojo.png").attr('alt', 'Tiene nota');

                                                // Cambiamos la imagen si obs no está vario
                                                var inp =$("#"+dest);

                                                var irudia = $("#"+dest).parent().next().children().children();
                                                
                                                if (inp.val().length > 0) {
                                                    $( irudia ).attr('src', '/img/nota_rojo.png');
                                                } else {
                                                    $( irudia ).attr('src', '/img/nota_berde.gif');
                                                }

					}
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {
				allFields.val( "" ).removeClass( "ui-state-error" );
			}
		});

		$( ".create-user" )
			//.button()
			.click(function() {
                                var miirel = $(this).attr("rel");
                                $("#rela").val(miirel);
                                //console.log($("#" + miirel).val());
                                //console.log($("#rela").val(miirel));
                                //$("#obs").text($("#" + miirel).val());
                                $("#obs").val($("#" + miirel).val());
				$( "#dialog-form" ).dialog( "open" );
			}
                    );


	});
