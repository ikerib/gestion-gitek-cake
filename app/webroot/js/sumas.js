// UTILITY FUNCTIONS

function IsNumeric(n) {
    return !isNaN(n);
}

function calcTotalPallets1() {

    var totalPallets = 0;

    $(".orduak1").each(function() {

        var thisValue = $(this).val();

        if ( (IsNumeric(thisValue)) && (thisValue != '') && (thisValue > 0)) {
            totalPallets += parseFloat(thisValue);
        };

    });

    $("#total-orduak1").val(totalPallets);

}

function calcTotalPallets2() {

    var totalPallets = 0;

    $(".orduak2").each(function() {

        var thisValue = $(this).val();

        if ( (IsNumeric(thisValue)) && (thisValue != '') && (thisValue > 0)) {
            totalPallets += parseFloat(thisValue);
        };

    });

    $("#total-orduak2").val(totalPallets);

}
function calcTotalPallets3() {

    var totalPallets = 0;

    $(".orduak3").each(function() {

        var thisValue = $(this).val();

        if ( (IsNumeric(thisValue)) && (thisValue != '') && (thisValue > 0)) {
            totalPallets += parseFloat(thisValue);
        };

    });

    $("#total-orduak3").val(totalPallets);

}
function calcTotalPallets4() {

    var totalPallets = 0;

    $(".orduak4").each(function() {

        var thisValue = $(this).val();

        if ( (IsNumeric(thisValue)) && (thisValue != '') && (thisValue > 0)) {
            totalPallets += parseFloat(thisValue);
        };

    });

    $("#total-orduak4").val(totalPallets);

}
function calcTotalPallets5() {

    var totalPallets = 0;

    $(".orduak5").each(function() {

        var thisValue = $(this).val();

        if ( (IsNumeric(thisValue)) && (thisValue != '') && (thisValue > 0)) {
            totalPallets += parseFloat(thisValue);
        };

    });

    $("#total-orduak5").val(totalPallets);

}

// DOM READY
$(function() {

    // "The Math" is performed pretty much whenever anything happens in the quanity inputs
    $('.orduak1').bind("focus blur change keyup", function(){
        calcTotalPallets1();
    });

    $('.orduak2').bind("focus blur change keyup", function(){
        calcTotalPallets2();
    });

    $('.orduak3').bind("focus blur change keyup", function(){
        calcTotalPallets3();
    });

    $('.orduak4').bind("focus blur change keyup", function(){
        calcTotalPallets4();
    });

    $('.orduak5').bind("focus blur change keyup", function(){
        calcTotalPallets5();
    });
    
    // lanzamos todas
    calcTotalPallets1();
    calcTotalPallets2();
    calcTotalPallets3();
    calcTotalPallets4();
    calcTotalPallets5();

});