<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * This is a placeholder class.
 * Create the same file in app/app_controller.php
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       cake
 * @subpackage    cake.cake.libs.controller
 * @link http://book.cakephp.org/view/957/The-App-Controller
 */
if (Configure::read('debug') == 0){
                        @ob_start ('ob_gzhandler');
                        header('Content-type: text/html; charset: UTF-8');
                        header('Cache-Control: must-revalidate');
                        $offset = -1;
                        $ExpStr = "Expires: " .gmdate('D, d M Y H:i:s',time() + $offset) .' GMT';
                        header($ExpStr);
                } 
class AppController extends Controller {
    var $components = array('Session','DebugKit.Toolbar');
    var $helpers = array('Session','Javascript', 'Ajax');

    function checkSession()
        {
            // If the session info hasn't been set...
            if ((!$this->Session->check('Usuario_id')))
            {
                    // Force the user to login
                    $this->redirect('/login');
                    exit();
            }
    }

}
